(* elab_test.ml ---
 *
 *      Copyright (C) 2016-2023  Free Software Foundation, Inc.
 *
 *   Author: Vincent Bonnevalle <tiv.crb@gmail.com>
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- *)

open Typerlib

open Utest_lib

let add_elab_test elab test name ?(setup="") ?(expected="") input =
  let run_elab_test () =
    let _, ectx = Elab.lexp_decl_str setup Elab.default_ectx in
    let last_metavar = !Unification.global_last_metavar in
    let actual, _ = elab input ectx in
    if String.equal expected ""
    then success
    else
      (Unification.global_last_metavar := last_metavar;
       let expected, _ = elab expected ectx in
       test actual expected)
  in
  add_test "ELAB" name run_elab_test

(** Run setup, then elaborate [input]. Succeeds if there are no compilation
    errors and [expected] elaborates to the same lexp, if provided. *)
let add_elab_test_expr =
  add_elab_test (fun s ctx -> Elab.lexp_expr_str s ctx, ctx) expect_equal_lexps

(** Like {!add_elab_test_expr}, but elaborates [input] and [expected] as top
    level declarations instead of expressions. *)
let add_elab_test_decl =
  add_elab_test Elab.lexp_decl_str expect_equal_decls

let _ = add_elab_test_expr
  "Instanciate implicit arguments"
  ~setup:{|
%% FIXME: Unification not powerful enough to handle polymorphic ints
%%   here... It seems that we would need unif. of Case or Proj.
typer-immediate = ##typer-immediate;

f : (i : Integer) -> Eq i i -> Integer;
f = lambda i -> lambda eq -> i;
  |}
  ~expected:"f 4 (Eq_refl (x := 4));"
  "f 4 Eq_refl;"

let _ = add_elab_test_decl
  "Whnf of case"
  {|
Box = (typecons (Box (l : TypeLevel) (t : Type_ l)) (box t));
box = (datacons Box box);
unbox b = ##case_ (b | box inside => inside);
alwaysbool b = ##case_ (b | box _ => Bool);

example1 : unbox (box (lambda x -> Bool)) (1 : Int);
example1 = true;

example2 : alwaysbool (box Int);
example2 = true;
  |}

let _ = add_elab_test_decl
  "Whnf of proj"
  {|
Box = (typecons (Box (l ::: TypeLevel) (t : Type_ l)) (box (inside : t)));
box = (datacons Box box);

unbox : (Box ?t) -> ?t;
unbox b = b.inside;

example1 : unbox (box (lambda x -> Bool)) (1 : Int);
example1 = true;

example2 : Eq (__\.__ (box 1) inside) (1 : Int);
example2 = Eq_refl;
  |}

let _ = add_elab_test_decl
  "depelim with Nats"
  {|
type Nat
  | Z
  | S Nat;

Nat_induction :
  (P : Nat -> Type_ ?l) ≡>
  P Z ->
  ((n : Nat) -> P n -> P (S n)) ->
  ((n : Nat) -> P n);
Nat_induction base step n =
  ##case_ (n
  | Z =>    Eq_cast (p := ##DeBruijn 0) (f := P) base
  | S n' => Eq_cast (p := ##DeBruijn 0) (f := P) (step n' (Nat_induction (P := P) base step n')));

plus : Nat -> Nat -> Nat;
plus x y =
  case x
  | Z => y
  | S x' => S (plus x' y);

+0_identity : (n : Nat) -> Eq (plus n Z) n;
+0_identity =
  Nat_induction
      (P := (lambda n -> Eq (plus n Z) n))
      Eq_refl
      (lambda n-1 n-1+0=n-1 ->
       Eq_cast
           (p := n-1+0=n-1)
           (f := lambda n-1_n -> Eq (S (plus n-1 Z)) (S n-1_n))
           Eq_refl);
    |}

let _ = add_elab_test_decl
  "depelim macro"
  {|
type Nat
  | Z
  | S Nat;

Nat_induction :
  (P : Nat -> Type_ ?l) ≡>
  P Z ->
  ((n : Nat) -> P n -> P (S n)) ->
  ((n : Nat) -> P n);
Nat_induction base step n =
  case n return (P n)
  | Z => base
  | S n' => (step n' (Nat_induction (P := P) base step n'));
  |}

let _ = add_elab_test_decl
  "case conversion"
  {|
unify =
  macro (
      lambda sxps ->
      do {vname <- gensym ();
          IO_return
              (quote ((uquote vname) : (uquote (Sexp_node (Sexp_symbol "##Eq") sxps));
                      (uquote vname) = Eq_refl;
                     ));
         });

case_ = ##case_;

type Nat
  | S Nat
  | Z;

plus : ?;
plus x y =
  case x
  | Z => y
  | S n => S (plus n y);

f x y b =
  case b
  | true => plus x y
  | false => Z;

unify (f Z (S Z)) (f (S Z) Z);
   |}

let _ = add_elab_test_decl
  "WHNF of Eq.cast (applied to Eq.eq)"
  {|
x = (4 : Int);
y = x;

p : Eq x y;
p = Eq_refl;

test : Eq (Eq_cast (p := p) (f := lambda _ -> Unit) ()) ();
test = Eq_refl;
   |}

let _ = add_elab_test_decl
   "WHNF of Eq.cast (applied to Quotient.eq)"
   {|
totalRel : Unit -> Unit -> Type;
totalRel u1 u2 = Unit;

unitQ : Quotient Unit totalRel;
unitQ = Quotient_in unit;

unitQ' = unitQ;

unitQ=unitQ' : Eq (t := Quotient Unit totalRel) unitQ unitQ';
unitQ=unitQ' = Quotient_eq (R := totalRel)
                           (a := unit)
                           (a' := unit)
                           unit;

test : Eq (Eq_cast (p := unitQ=unitQ') (f := lambda _ -> Unit) ()) ();
test = Eq_refl;
    |}

let _ = add_elab_test_decl
  "Decidable at the type level"
  {|
Decidable =
  typecons (Decidable (ℓ ::: TypeLevel) (prop : Type_ ℓ))
           (yes (p :: prop)) (no (p :: Not prop));
yes = datacons Decidable yes;
no  = datacons Decidable  no;

true≠false (p : Eq true false) =
  Eq_cast (p := p)
          (f := lambda tf -> case tf
            | true => True
            | false => False)
          (); % This is unit, an inhabitant of `True`.

false≠true (p : Eq false true) = true≠false (Eq_comm p);

decideBoolEq : (a : Bool) => (b : Bool) => Decidable (Eq a b);
decideBoolEq =
  lambda (a : Bool) (b : Bool) =>
  case a return (Decidable (Eq a b))
  | true =>
      (case b return (Decidable (Eq true b))
       | true => yes (p := Eq_refl)
       | false => no (p := true≠false))
  | false =>
      (case b return (Decidable (Eq false b))
       | true  =>  no (p := false≠true)
       | false => yes (p := Eq_refl));

getProof :
  (l : TypeLevel) ≡>
  (prop : Type_ l) ≡>
  (d : Decidable prop) ->
  case d | yes => prop | no => Not prop;
getProof d =
  case d return (case d | yes => prop | no => Not prop)
  | yes (p := p) => p
  | no  (p := p) => p;

test =
  (getProof (decideBoolEq : Decidable (Eq true true)) : Eq true true);
   |}

let _ = add_elab_test_decl
  "Implicit type parameters"
  {|
type Not (ℓ ::: TypeLevel) (prop : Type_ ℓ) : Type_ ℓ
  | mkNot ((contra : prop) -> False);

type Decidable (ℓ ::: TypeLevel) (prop : Type_ ℓ) : Type_ ℓ
  | yes (p ::: prop)
  | no (p ::: Not prop);

type Lift (l ::: TypeLevel) (t :: Type_ l) (x : t)
  | mkLift;
   |}

let _ = add_elab_test_decl
  "Check and instantiate implicit args when a type is given"
  {|
test : Int -> ?a -> ?a;
test _ = id;
   |}

let _ = add_elab_test_decl
   "η expansion for Lambdas"
  {|
succ : Int -> Int;
succ i = i + 1;

p1 : Eq succ (lambda i -> succ i);
p1 = Eq_refl;

p2 : Eq (lambda i -> succ i) succ;
p2 = Eq_refl;
    |}

let _ = run_all ()
