(* instargs_test.ml ---
 *
 *      Copyright (C) 2016-2017  Free Software Foundation, Inc.
 *
 *   Author: Jean-Alexandre Barszcz <jean-alexandre.barszcz@umontreal.ca>
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- *)

open Typerlib
open Utest_lib

open Ir

module DB = Debruijn
module E = Elab
module I = Instargs
module L = Lexp
module O = Opslexp

let add_test = add_test "INSTARGS"

let default_ectx = E.default_ectx
let default_rctx = E.default_rctx
let dummy_vname = DB.dsinfo, None

let make_metavar t ctx =
  E.newInstanceMetavar ctx (DB.dsinfo, Some "inst") t

let lexp_from_str str ctx =
  List.hd (E.lexp_expr_str str ctx)

(* Test the type class context : `is_typeclass` *)

let _ =
  let vars_str =
    {|
a = 1;

eq : Eq a (1 : Int);
eq = Eq_refl;

typeclass Eq;
     |} in
  let ctx = snd (E.eval_decl_str vars_str default_ectx default_rctx) in
  (List.map (fun (tstr, b) ->
       let t = lexp_from_str tstr ctx in
       add_test ("is_typeclass `" ^ tstr ^ "`") (fun _ ->
           expect_equal_bool (I.is_typeclass ctx t) b
         )
     ) [
       ("Int", false);
       ("Eq", true);
       ("decltype a", false);
       ("decltype eq", true);
  ])

(* Test the type class context : which vars are instances *)

let _ =
  let vars_str =
    {|
% bind-instances; % This is the default.
a = 1;
b = 2;
not-instance b;
dont-bind-instances;
c = 3;
d = 4;
instance d;
     |} in
  let ctx = snd (E.eval_decl_str vars_str default_ectx default_rctx) in
  let tcctx = DB.ectx_to_tcctx ctx in
  let _,_,insts = tcctx in
  (List.map (fun (lstr, b) ->
       match L.lexp_lexp' (lexp_from_str lstr ctx) with
       | Var (_,idx) ->
           add_test ("is `" ^ lstr ^ "` an instance ?") (fun _ ->
               expect_equal_bool (DB.set_mem idx insts) b
             )
       | _ -> failwith "Impossible"
     ) [("a", true); ("b", false); ("c", false); ("d", true)])

(* Test : Make sure that we get the expected instance.

    Fill the context with instances of `Lift i` with `i` their de
   Bruijn index, and resolve for various values of `i`. *)

let lift_str =
  {|
type Lift (l ::: TypeLevel) (t :: Type_ l) (x : t)
  | mkLift;
typeclass Lift;
   |}

let lift_rctx, lift_ectx =
  E.eval_decl_str lift_str default_ectx default_rctx

let lift_n n ctx =
  let str = Format.sprintf "mkLift (t := Int) (x := %i)" n in
  lexp_from_str str ctx

let lift_n_t n ctx =
  let str = Format.sprintf "Lift (t := Int) (x := %i)" n in
  lexp_from_str str ctx

let rec extend_ectx_with_lifts n ectx =
  let lift_n = lift_n n ectx in
  let lift_n_t = O.get_type (DB.ectx_to_lctx ectx) lift_n in
  let nctx = E.ctx_define ectx dummy_vname lift_n lift_n_t in
  if n = 0 then nctx else
    extend_ectx_with_lifts (n - 1) nctx

let lifts_metavar i ctx =
  make_metavar (lift_n_t i ctx) ctx

let _ =
  add_test "Matching the right instance" (fun _ ->
      let depth = 100 in
      let ectx = extend_ectx_with_lifts depth lift_ectx in
      combine_results
        (List.map (fun i ->
             let lxp = lifts_metavar i ectx in
             E.resolve_instances lxp;
             expect_equal_lexp lxp (L.mkVar (dummy_vname, i))
           ) [0;1;2;4;13;17;58;99])
    )

(* Test : Recursive resolution builds the right calls.

    Use the singleton type for natural numbers to trigger recursive
   searches. The resolution finds the instance `Ss'` for `SNat (S n)`,
   and then recursively searches for `SNat n`, until it finds `Zs'`
   for `SNat Z`. *)

let snats_str =
  {|
type Nat
  | S Nat
  | Z;

type SNat (n : Nat)
  | Ss (n-1 ::: Nat) (p ::: Eq (S n-1) n) (sn-1 :: SNat n-1)
  | Zs (Z=n ::: Eq Z n);

Ss' : (n : Nat) => SNat n => SNat (S n);
Ss' = lambda (n : Nat) => Ss (n := S n) (n-1 := n) (p := Eq_refl);

Zs' : SNat Z;
Zs' = Zs (Z=n := Eq_refl);

typeclass SNat;
   |}

let snats_rctx, snats_ectx =
  Elab.eval_decl_str snats_str default_ectx default_rctx

let to_nat n ctx =
  let s = lexp_from_str "S" ctx in
  let z = lexp_from_str "Z" ctx in
  let rec loop i nat =
    if i = 0 then nat else loop (i - 1) (L.mkCall (DB.dsinfo, s,[Anormal, nat])) in
  loop n z

let to_snat n ctx =
  let s = lexp_from_str "S" ctx in
  let ss = lexp_from_str "Ss'" ctx in
  let zs = lexp_from_str "Zs'" ctx in
  let rec loop i nat snat =
    if i = 0 then snat else
      loop (i - 1)
        (L.mkCall (DB.dsinfo, s,[Anormal, nat]))
        (L.mkCall (DB.dsinfo, ss,[Aimplicit, nat; Aimplicit, snat])) in
  loop n (to_nat 0 ctx) zs

let snat_n_t n ctx =
  let snat = lexp_from_str "SNat" ctx in
  L.mkCall (DB.dsinfo, snat, [Anormal, to_nat n ctx])

let snat_metavar i ctx =
  make_metavar (snat_n_t i ctx) ctx

let _ =
  add_test "Recursive resolution" (fun _ ->
      let lctx = DB.ectx_to_lctx snats_ectx in
      combine_results
        (List.map (fun i ->
             let lxp = snat_metavar i snats_ectx in
             E.resolve_instances lxp;
             expect_conv_lexp lctx lxp (to_snat i snats_ectx)
           ) [0;1;2;4;13;17;58;99])
    )

let _ =
  add_test "Resolution recursion limit" (fun _ ->
      let limit = 20 in
      Instargs.recursion_limit := Some limit;
      let lxp = snat_metavar limit snats_ectx in
      expect_throw Fmt.string_of_lexp
        (fun _ -> E.resolve_instances lxp; Log.stop_on_error (); lxp))

let _ = run_all ()
