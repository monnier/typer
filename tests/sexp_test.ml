(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2016-2022  Free Software Foundation, Inc.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          utest library utility.  Tests register themselves
 *
 * --------------------------------------------------------------------------- *)

open Typerlib

open Sexp
open Lexer
open Utest_lib

let sexp_parse_str dcode =
  let source = Source.of_string dcode in
  sexp_parse_source source Grammar.default_stt Grammar.default_grammar (Some ";")

let test_sexp_add dcode testfun =
  add_test "SEXP" dcode
    (fun () -> testfun (sexp_parse_str dcode))

let _ = test_sexp_add "lambda x -> x + x" (fun ret ->
        match ret with
        | [Node(_,
                Symbol(_, "lambda_->_"),
                [Symbol(_, "x");
                 Node(_, Symbol(_, "_+_"), [Symbol(_, "x"); Symbol(_, "x")])])]
          -> success
        | _ -> failure
)

let _ = test_sexp_add "x * x * x" (fun ret ->
        match ret with
        | [Node(_,
                Symbol(_, "_*_"),
                [Node(_, Symbol(_, "_*_"), [Symbol(_, "x"); Symbol(_, "x")]);
                 Symbol(_, "x")])]
          -> success
        | _ -> failure
)

let test_sexp_eqv ?name actual_source expected_source =
  let test () =
    let actual = sexp_parse_str actual_source in
    let expected = sexp_parse_str expected_source in
    expect_equal_sexps ~actual ~expected
  in
  let name = Option.value ~default:actual_source name in
  add_test "SEXP" name test

let _ = test_sexp_eqv "((a) ((1.00)))" "a 1.0"
let _ = test_sexp_eqv "(x + y)" "_+_ x y"
let _ = test_sexp_eqv "(x := y)" "_:=_ x y"
let _ = test_sexp_eqv "A : B -> C" "A : (B -> C)"
let _ = test_sexp_eqv "f __\\; y" "(f (__\\;) y)"
let _ = test_sexp_eqv "case e | p1 => e1 | p2 => e2"
                      "case_ (_|_ e (_=>_ p1 e1) (_=>_ p2 e2))"
let _ = test_sexp_eqv "a\\b\\c" "abc"
let _ = test_sexp_eqv "(a;b)" "(_\\;_ a b)"
let _ = test_sexp_eqv "a.b.c . (.)" "(__\\.__ (__\\.__ a b) c) \\. \\."

let _ =
  test_sexp_eqv
    ~name:"Type ascription in declaration"
    "x = 4 : Int"
    "(_=_ x (_:_ 4 Int))"

let add_sym_printing_test name source expected =
  add_test
    "SEXP"
    name
    (fun ()
     -> if
          source
          |> Sym.intern ~location:Source.Location.dummy
          |> Fmt.string_of_symbol
          |> String.equal expected
        then success
        else failure)


let _ =
  add_sym_printing_test
    "Convert average symbol to string"
    "monoïde单子функтор"
    "monoïde单子функтор"

let _ =
  add_sym_printing_test
    "Convert symbol with dot to string"
    ".asdf"
    "\\.asdf"

let _ =
  add_sym_printing_test
    "Convert symbol with parens to string"
    "asdf)"
    "asdf\\)"

(* run all tests *)
let _ = run_all ()
