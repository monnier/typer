(* Copyright (C) 2021  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>. *)

open Typerlib

open Utest_lib

open Sexp

let test_lex name pretokens expected =
  let lex () =
    let actual = Lexer.lex Grammar.default_stt pretokens in
    if List.equal Sexp.same actual expected
    then success
    else
      begin
        (* Only print locations if the Sexp are otherwise identical so its
           easier to spot the problem. *)
        let open Format in
        let f = formatter_of_out_channel stderr in
        if List.equal Sexp.sexp_equal actual expected then
          Source.pp_enable_print_locations f;
        fprintf
          f
          "@{<red>Expected:@}@.%a@.@{<red>Actual:@}@.%a@."
          Fmt.pp_print_sexp_list
          expected
          Fmt.pp_print_sexp_list
          actual;
        failure
      end
  in
  add_test "LEXER" name lex

let l c os ls cs oe le ce : Source.Location.t =
  {
    container = c;
    start = {offset = os; line = ls; column = cs};
    end' = {offset = oe; line = le; column = ce};
  }

let () =
  let source = "a.b" in
  let l = l (Source.Container.of_string ~label:"test" source) in
  test_lex
    "Inner operator inside a presymbol"
    [Pretoken (l 0 1 0 3 1 3, source)]
    [Node (l 0 1 0 3 1 3,
           Symbol (l 1 1 1 2 1 2, "__.__"),
           [Symbol (l 0 1 0 1 1 1, "a"); Symbol (l 2 1 2 3 1 3, "b")])]

let () =
  let source = ".b" in
  let l = l (Source.Container.of_string ~label:"test" source) in
  test_lex
    "Inner operators at the beginning of a presymbol"
    [Pretoken (l 0 1 0 2 1 2, source)]
    [Node (l 0 1 0 2 1 2,
           Symbol (l 0 1 0 1 1 1, "__.__"),
           [epsilon (l 0 1 0 0 1 0); Symbol (l 1 1 1 2 1 2, "b")])]

let () =
  let source = "a." in
  let l = l (Source.Container.of_string ~label:"test" source) in
  test_lex
    "Inner operators at the end of a presymbol"
    [Pretoken (l 0 1 0 2 1 2, source)]
    [Node (l 0 1 0 2 1 2,
           Symbol (l 1 1 1 2 1 2, "__.__"),
           [Symbol (l 0 1 0 1 1 1, "a");
            epsilon (l 2 1 2 2 1 2)])]

let () =
  let source = "." in
  let l = l (Source.Container.of_string ~label:"test" source) in
  test_lex
    "An inner operator by itself is a simple symbol"
    [Pretoken (l 0 1 0 1 1 1, source)]
    [Symbol (l 0 1 0 1 1 1, ".")]

let () = run_all ()
