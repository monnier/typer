(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2022  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          utest library utility.  Tests register themselves
 *
 * --------------------------------------------------------------------------- *)

open Typerlib

open Fmt
open Util

type test_fun = unit -> int
type section = test_fun SMap.t * string list

let success = 0
let failure = -1

let combine_results = List.fold_left (+) 0

(*
 *  SECTION NAME - TEST NAME   - FUNCTION (() -> int)
 *      "Let"    - "Base Case" - (fun () -> success )
 *      "Let"    - "Recursive"
 *      "Lambda" - "Base Case"
 *      "Lambda" - "Nested"
 *)
let global_sections = ref SMap.empty
let insertion_order = ref []
let ret_code = ref 0
let number_test = ref 0

(*  Exception *)
exception Unexpected_result of string

(*  Argument Parsing                        *)
(* ---------------------------------------- *)

(*
 *  0: Nothing
 *  1: Print:            (x/N) file_test .................... pass
 *  2: Print Each composing Test
 *  3: use debug
 *)
let global_verbose_lvl = ref 2
let global_sample_dir = ref ""
let global_fsection = ref ""
let global_ftitle = ref ""

let set_verbose lvl =
    global_verbose_lvl := lvl;
    Log.set_typer_log_level (if lvl >= 3 then Log.Debug else Log.Nothing)

let arg_defs = [
    ("--verbose",
     Arg.Int set_verbose, " Set verbose level");
    ("--samples",
     Arg.String (fun g -> global_sample_dir := g), " Set sample directory");
    (* Allow users to select which test to run *)
    ("--fsection",
     Arg.String (fun g -> global_fsection := String.uppercase_ascii g),
     " Set test filter");
    ("--ftitle",
     Arg.String (fun g -> global_ftitle := String.uppercase_ascii g),
     " Set test filter");
  ]

let must_run_section str =
  !global_fsection = ""
  || String.uppercase_ascii str = !global_fsection

let must_run_title str =
  !global_ftitle = ""
  || String.uppercase_ascii str = !global_ftitle

let parse_args () = Arg.parse arg_defs (fun _ -> ()) ""


(* Utest printing function                  *)
(* ---------------------------------------- *)
let ut_string vb msg = if vb <= (!global_verbose_lvl) then print_string msg else ()
let ut_string2 = ut_string 2


(* Value Checking                                           *)
(*      USAGE: expect_something_type value expected_value   *)
(*          return success() if match else failure ()       *)
let unexpected_throw sk tk e =
    ut_string2 (red ^ "[   FAIL] " ^ sk ^ " - " ^ tk ^ "\n");
    ut_string2        "[       ]     UNEXPECTED THROW:\n";
    ut_string2        "[       ]     ----------------------------- Backtrace: ----------------------------------------------\n";
    ut_string2        ("[       ]     " ^ (Printexc.get_backtrace ()) ^ "\n");
    ut_string2        "[       ]     ---------------------------------------------------------------------------------------\n";
    ut_string2        "[       ]  "; ut_string2 ((Printexc.to_string e) ^ "\n" ^ reset)

let _expect_equal_t equality_test to_string value expect =
  if equality_test value expect then
    success
  else (
    ut_string2 (red ^ "EXPECTED: " ^ reset ^ "\n" ^ (to_string expect) ^ "\n");
    ut_string2 (red ^ "GOT: " ^ reset ^ "\n" ^ (to_string value) ^ "\n");
    failure)

let print_value_list values =
  List.fold_left (fun s v -> s ^ "\n" ^ Fmt.string_of_value v) "" values

let expect_equal_bool  = _expect_equal_t Bool.equal string_of_bool
let expect_equal_int   = _expect_equal_t Int.equal string_of_int
let expect_equal_float = _expect_equal_t Float.equal string_of_float
let expect_equal_str   = _expect_equal_t String.equal (fun g -> g)
let expect_equal_values = _expect_equal_t Env.value_eq_list print_value_list

let expect_equal_lexp = _expect_equal_t Lexp.eq Fmt.string_of_lexp
let expect_conv_lexp ctx = _expect_equal_t (Opslexp.conv_p ctx) Fmt.string_of_lexp

let expect_equal_sexps ~expected ~actual =
  let print_sexp_ok s =
    Printf.printf (green_f ^^ "%s" ^^ reset_f ^^ "\n") (Fmt.string_of_sexp s)
  in

  let print_sexp_err expected actual  =
    Printf.printf
      ("Expected:\n" ^^ red_f ^^ "%s\n" ^^ reset_f)
      (Fmt.string_of_sexp expected);
    Printf.printf
      ("Actual:\n" ^^ red_f ^^ "%s\n" ^^ reset_f)
      (Fmt.string_of_sexp actual)
  in

  let rec loop failed fine expected actual =
    match expected, actual with
    | l :: ll, r :: rr ->
       if Sexp.sexp_equal l r
       then loop failed (l :: fine) ll rr
       else
         (List.iter print_sexp_ok fine;
          print_sexp_err l r;
          loop true [] ll rr)
    | _ :: _, [] ->
       List.iter print_sexp_ok fine;
       Printf.printf ("Missing sexps:\n" ^^ red_f);
       List.iter (fun s -> s |> Fmt.string_of_sexp |> Printf.printf "%s\n") expected;
       Printf.printf reset_f;
       true
    | [], _ :: _ ->
       List.iter print_sexp_ok fine;
       Printf.printf ("Unexpected sexps:\n" ^^ red_f);
       List.iter (fun s -> s |> Fmt.string_of_sexp |> Printf.printf "%s\n") actual;
       Printf.printf reset_f;
       true
    | [], [] ->
       if failed
       then List.iter print_sexp_ok fine;
       failed
  in
  if loop false [] expected actual
  then failure
  else success

let expect_equal_lexps =
  let rec lexp_list_eq l r =
    match l, r with
    | [], [] -> true
    | l_head :: l_tail, r_head :: r_tail when Lexp.eq l_head r_head
      -> lexp_list_eq l_tail r_tail
    | _ -> false
  in
  let string_of_lexp_list lexps =
    List.fold_left (fun s lexp -> s ^ "\n" ^ Fmt.string_of_lexp lexp) "" lexps
  in
  _expect_equal_t lexp_list_eq string_of_lexp_list

let expect_equal_decls =
  let decl_eq l r =
    let (_, l_vname), l_lexp, l_ltype = l in
    let (_, r_vname), r_lexp, r_ltype = r in
    Option.equal String.equal l_vname r_vname
    && Lexp.eq l_lexp r_lexp
    && Lexp.eq l_ltype r_ltype
  in
  let rec mutual_decl_list_eq l r =
    match l, r with
    | [], [] -> true
    | l_head :: l_tail, r_head :: r_tail when decl_eq l_head r_head
      -> mutual_decl_list_eq l_tail r_tail
    | _ -> false
  in
  let rec decl_list_eq l r =
    match l, r with
    | [], [] -> true
    | l_head :: l_tail, r_head :: r_tail when mutual_decl_list_eq l_head r_head
      -> decl_list_eq l_tail r_tail
    | _ -> false
  in
  let string_of_decl_list ds =
    Format.asprintf "%a" Fmt.pp_print_lexp_decls ds
  in
  _expect_equal_t decl_list_eq string_of_decl_list

let expect_throw to_string test =
  try let value = test () in
      ut_string2 (red ^ "EXPECTED an exception" ^ reset ^ "\n");
      ut_string2 (red ^ "GOT: " ^ reset ^ "\n" ^ (to_string value) ^ "\n");
      failure
  with
  | _ -> success

(* USAGE
 *
 *   (add_test "LET" "Base Case" (fun () ->
 *      let r = eval_string "let a = 2; b = 3; in a + b;" in
 *      let v = (get_int r) in
 *          if v = 5 then success () else failure ()))
 *)
let add_test section_name test_name test_fun =
  let update_tests = function
    | Some _ as entry
      -> (ut_string2 (red ^ {|Test "|} ^ test_name ^ {|" alread exists.|} ^ reset);
          entry)
    | None
      -> (number_test := !number_test + 1;
          Some test_fun)
  in
  let update_sections section =
    let updated_entry = match section with
      | Some (tests, order)
        -> SMap.update test_name update_tests tests, test_name :: order
      | None
        -> (insertion_order := section_name :: !insertion_order;
            SMap.singleton test_name test_fun, [test_name])
    in
    Some updated_entry
  in
  global_sections := SMap.update section_name update_sections !global_sections

(*  sk  : Section Key
 *  tmap: test_name -> tmap
 *  tk  : Test Key                  *)
let for_all_tests sk tmap tk =
    if (must_run_title tk) then (
    let tv = SMap.find tk tmap in
    flush stdout;
    Log.clear_log ();
    try
        let r = tv () in
            if r = 0 then(
                ut_string2 (green ^ "[     OK] " ^ sk ^ " - " ^ tk ^ "\n" ^ reset))
            else(
                ut_string2 (red ^ "[   FAIL] " ^ sk ^ " - " ^ tk ^ "\n" ^ reset);
                ret_code := failure)
    with
    | Log.Stop_compilation message ->
      ret_code := failure;
      ut_string2 (red ^ "[   FAIL] " ^ sk ^ " - " ^ tk ^ "\n");
      ut_string2 ("[       ] " ^ message ^ "\n" ^ reset);
      Log.print_log ();
    | e ->
      ret_code := failure;
      unexpected_throw sk tk e) else ()

let for_all_sections sk =
  let tmap, tst = SMap.find sk (!global_sections) in
  let tst = List.rev tst in
  if must_run_section sk
  then (
    ut_string2 ("[RUN    ] " ^ sk ^ " \n");
    List.iter (for_all_tests sk tmap) tst)

(* Run all *)
let run_all () =
    Printexc.record_backtrace true;

    parse_args ();

    insertion_order := List.rev (!insertion_order);

    (* iter over all sections *)
    List.iter for_all_sections (!insertion_order);

    (* return success or failure *)
    exit !ret_code
