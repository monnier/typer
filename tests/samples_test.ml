(*   Copyright (C) 2022  Free Software Foundation, Inc.
 *
 *   Author: Jean-Alexandre Barszcz <jean-alexandre.barszcz@umontreal.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>. *)

open Typerlib
open Utest_lib
module SSet = Set.Make(String)

let ectx = Elab.default_ectx

let add_sample_test filename =
  let error_expected =
    Filename.check_suffix (Filename.remove_extension filename) "error" in
  let run_sample_test () =
    let backend = new Eval.ast_interpreter (Debruijn.ectx_to_lctx ectx) in
    let file = Filename.concat "samples" filename in
    ignore (Elab.process_file backend ectx file);
    Log.stop_on_error ();
    success in
  add_test "SAMPLES" filename (fun () ->
      if error_expected then
        expect_throw (fun _ -> "successful evaluation") run_sample_test
      else
        run_sample_test ()
    )

(* Some samples are currently not runnable, let's skip them. *)
(* FIXME: Fix these samples. *)
let excluded_samples =
  SSet.of_list [
      "autodiff.typer";
      "batch_test.typer";
      "bbst_test.typer";
      "case_test.typer";
      "defmacro.typer";
      "dependent.typer";
      "hurkens.typer";
      "io.typer";
      "list_n.typer";
      "myers_test.typer";
      "pervasive.typer";
      "polyfun_test.typer";
      "table_test.typer";
      "tuple_test.typer";
      "typer_proof.typer";
    ]

let _ =
  let samples =
    List.filter (fun fn -> Filename.check_suffix fn ".typer" &&
                             not (SSet.mem fn excluded_samples))
      (Array.to_list (Sys.readdir "samples")) in
  List.iter add_sample_test samples

let _ = run_all ()
