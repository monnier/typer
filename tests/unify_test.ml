(* unify_test.ml --- Test the unification algorithm
 *
 *      Copyright (C) 2016-2020  Free Software Foundation, Inc.
 *
 *   Author: Vincent Bonnevalle <tiv.crb@gmail.com>
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- *)

open Typerlib

open Ir
open Lexp
open Unification

open Fmt
open Utest_lib

module U = Util

(* default environment *)
let ectx = Elab.default_ectx
let rctx = Elab.default_rctx

let dsinfo = DB.dsinfo

type result =
  | Constraint
  | Unification
  | Equivalent
  | Nothing

let string_of_result r =
  match r with
  | Constraint  -> "Constraint"
  | Unification -> "Unification"
  | Equivalent  -> "Equivalent"
  | Nothing     -> "Nothing"

let unif_output ?matching (lxp1: lexp) (lxp2: lexp) ctx =
  let orig_subst = !metavar_table in
  let constraints = unify ?matching lxp1 lxp2 ctx in
  match constraints with
  | [] -> 
      if !current_residues == [] then
        let new_subst = !metavar_table in
        if orig_subst == new_subst
        then (Equivalent, constraints)
        else (Unification, constraints)
      else
        (Constraint, constraints)
  | _ -> (Nothing, constraints)

let add_unif_test name ?matching ?(ectx=ectx) lxp_a lxp_b expected =
  add_test "UNIFICATION" name (fun () ->
      clean_residues ();
      let (r, _) = unif_output ?matching lxp_a lxp_b (DB.ectx_to_lctx ectx) in

      if r = expected then
        success
      else (
        ut_string2 (red ^ "EXPECTED: " ^ reset ^ (string_of_result expected) ^ "\n");
        ut_string2 (red ^ "GOT:      " ^ reset ^ (string_of_result r       ) ^ "\n");
        ut_string2 ("During the unification of:\n\t" ^ (Fmt.string_of_lexp lxp_a)
                    ^ "\nand\n\t" ^ (Fmt.string_of_lexp lxp_b) ^ "\n");
        failure
      ))

let add_unif_test_s name ?(setup="") ?(ectx=ectx) input_a input_b expected =
  let _, ectx = Elab.lexp_decl_str setup ectx in

  let lxp_a = List.hd (Elab.lexp_expr_str input_a ectx) in
  let lxp_b = List.hd (Elab.lexp_expr_str input_b ectx) in

  add_unif_test name ~ectx lxp_a lxp_b expected

let _ =
  (* Let's have some variables in context to block the reduction of
     elimination forms. The variables are manually added to the
     context (and not given a value) to make sure that they cannot be
     reduced. *)
  let _, ectx = Elab.lexp_decl_str
                  {|
                   typer-immediate = ##typer-immediate;
                   type Nat
                     | Z
                     | S (Nat);
                   |} ectx in
  let dsinfo = DB.dsinfo in
  let nat = mkVar ((dsinfo, Some "Nat"), 2) in
  let shift l i = mkSusp l (S.shift i) in
  let ectx, _ =
    List.fold_left
      (fun (ectx, i) (name, lexp) ->
        Elab.ctx_extend ectx (dsinfo, Some name) Variable (shift lexp i), i + 1)
      (ectx, 0)
      [("f", (mkArrow (dsinfo, Anormal, (dsinfo, None), nat, shift nat 1)));
       ("g", (mkArrow (dsinfo, Anormal, (dsinfo, None), nat, shift nat 1)));
       ("h", (mkArrow (dsinfo, Anormal, (dsinfo, Some "x"), nat,
                       mkArrow (dsinfo, Anormal, (dsinfo, Some "y"), shift nat 1,
                                shift nat 2))));
       ("a", nat);
       ("b", nat)] in

  add_unif_test_s "same integer" ~ectx "4" "4" Equivalent;
  add_unif_test_s "diff. integers" ~ectx "3" "4" Nothing;
  add_unif_test_s "int and builtin" ~ectx "3" "##Int" Nothing;
  add_unif_test_s "same var" ~ectx "a" "a" Equivalent;
  add_unif_test_s "diff. var" ~ectx "a" "b" Constraint;
  add_unif_test_s "var and integer" ~ectx "a" "1" Constraint;
  add_unif_test_s "same call" ~ectx "f a"  "f a" Equivalent;
  add_unif_test_s "calls with inconvertible heads" ~ectx "f a"  "g a" Constraint;
  add_unif_test_s "calls with diff. num. of args" ~ectx "h a"  "h a b" Constraint;
  add_unif_test_s "calls with residue in args" ~ectx "f a"  "f b" Constraint;
  add_unif_test_s "same case" ~ectx
    "case a | Z => false | S n => true"
    "case a | Z => false | S n => true"
    Equivalent;
  add_unif_test_s "diff. case" ~ectx
    "case a | Z => false | S n => true"
    "case a | Z => true | S n => false"
    (* 'Nothing' would be a more accurate result here. This would
       require implementing unification for case exprs. *)
    Constraint;

  add_unif_test_s "datacons/inductive" ~ectx
    "Z"
    "(datacons (typecons Nat Z (S Nat)) Z)"
    (* Not recursive! Refers to the previous def of Nat. *)
    Equivalent;

  add_unif_test_s "calls to different constructors" ~ectx
    "S (S Z)" "S Z" Nothing;
  add_unif_test_s "calls to different constants" ~ectx
    "Nat" "Eq (t := Nat) Z a" Nothing;

  (* Metavariables *)
  add_unif_test_s "same metavar" "?m" "?m" Equivalent;
  add_unif_test_s "diff. metavar" "?m1" "?m2" Unification;
  add_unif_test_s "metavar and int" "?m" "5" Unification;

  ()

(* Tests for the "matching" mode of unification *)
let _ =
  let vars_str =
    {|
a = 1;

eq : Eq a (1 : Int);
eq = Eq_refl;

eq_any : (false : False) => (x : Int) => Eq a x;
eq_any = lambda false x => ##case_ false;
     |} in
  let ectx = snd (Elab.eval_decl_str vars_str ectx rctx) in
  let dummy_lexp = List.hd (Elab.lexp_expr_str "()" ectx) in
  List.iter (fun (inst_str, query_str, result) ->
      let name = "Matching `" ^ inst_str ^ "` against `" ^ query_str ^ "`" in
      let query = List.hd (Elab.lexp_expr_str query_str ectx) in
      let inst_t = List.hd (Elab.lexp_expr_str inst_str ectx) in
      (* Instantiate implicit vars of instance in the new scope
         level. We are only interested in the type, so se use a dummy
         lexp and ignore the resulting expression. *)
      let ectx_sl = DB.ectx_new_scope ectx in
      let sl = DB.ectx_to_scope_level ectx_sl in
      let _lxp, lxp_t = Elab.instantiate_implicit dummy_lexp inst_t ectx_sl in
      add_unif_test name ~matching:sl ~ectx:ectx_sl lxp_t query result
    ) [
      ("decltype eq","decltype eq",Equivalent);
      ("decltype eq_any","decltype eq",Unification);
      ("decltype Eq_refl","decltype eq",Unification);
      (* Metavar treated as free because not at the right scope level : *)
      ("Eq a ?a","decltype eq",Constraint);
    ]

let _ = run_all ()
