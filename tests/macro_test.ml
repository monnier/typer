(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2018  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * --------------------------------------------------------------------------- *)

open Typerlib

open Ir
open Utest_lib


(* default environment *)
let ectx = Elab.default_ectx
let rctx = Elab.default_rctx

let _ = (add_test "MACROS" "macros base" (fun () ->

    (* define 'lambda x -> x * x' using macros *)
    let dcode = "
    my_fun = lambda (x : List Sexp) ->
        let hd = (case x
          | cons hd tl => hd
          | nil => Sexp_symbol \"x\") : Sexp
        in IO_return (Sexp_node (Sexp_symbol \"_*_\")
                                (cons hd (cons hd nil)));

    sqr = macro my_fun;
    " in

    let rctx, ectx = Elab.eval_decl_str dcode ectx rctx in

    let ecode = "(lambda (x : Int) -> sqr 3) 5;" in

    let ret = Elab.eval_expr_str ecode ectx rctx in
    expect_equal_values ret [Vint(3 * 3)]))

let _ = (add_test "MACROS" "macros decls" (fun () ->
    let dcode = "
      decls-impl = lambda (x : List Sexp) ->
        let chain-decl : Sexp -> Sexp -> Sexp;
            chain-decl a b = Sexp_node (Sexp_symbol \"_;_\") (cons a (cons b nil)) in

        let make-decl : String -> Sexp -> Sexp;
            make-decl name val =
          (Sexp_node (Sexp_symbol \"_=_\") (cons (Sexp_symbol name) (cons val nil))) in

        let d1 = make-decl \"a\" (quote (1 : Int)) in
        let d2 = make-decl \"b\" (quote (2 : Int)) in
          IO_return (chain-decl d1 d2);

      my-decls = macro decls-impl;

      my-decls Nat;" in

    let rctx, ectx = Elab.eval_decl_str dcode ectx rctx in

    let ecode = "a; b;" in

    let ret = Elab.eval_expr_str ecode ectx rctx in
    expect_equal_values ret [Vint(1); Vint(2)]))

(* run all tests *)
let _ = run_all ()
