{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  name = "typer";
  buildInputs =
    with pkgs.ocaml-ng.ocamlPackages_4_11; [
      pkgs.gnumake ocaml dune_2 findlib utop # tooling
      zarith # ocaml libraries
      merlin # for emacs
    ];
}
