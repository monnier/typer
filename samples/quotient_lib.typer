%%%%% Prelude %%%%%%

%% FIXME : Loading hott.typer doesn't work for some reason
%% due to "[X] Fatal    :(internal) lub of two SLsucc"
%% Some definitions will be duplicated for now

Eq_funext : (f : ? -> ?) => (g : ? -> ?) => ((x : ?) -> Eq (f x) (g x))
            -> Eq f g;
Eq_funext p = Eq_eq (f := lambda i ≡> lambda x -> Eq_uneq (p := p x) (i := i));

HoTT_isProp P = (x : P) -> (y : P) -> Eq x y;

HoTT_isSet A = (x : A) -> (y : A) -> HoTT_isProp (Eq x y);

HoTT_isContr = typecons (HoTT_isContr (l ::: TypeLevel)
                                      (A : Type_ l))
                         (isContr (a : A) ((a' : A) -> Eq a a'));
isContr = datacons HoTT_isContr isContr;

%%%%% Prelude END %%%%%%

%% TODO: Prove dependent version of this after we introduce
%% dependent elim, which will be more interesting and more
%% worthwhile
recProp : (A : Type_ ?) ≡> (B : Type_ ?)
          ≡> (R : A -> A -> Type_ ?) ≡> (p : HoTT_isProp B)
          -> (f : A -> B) -> Quotient A R -> B;
recProp = lambda _ _ _ _ _ R ≡>
           lambda p f x ->
             Quotient_elim (R := R) f (p := lambda a a' r -> p (f a) (f a')) x;

%% Again, this is not very interesting, unlike its dependent
%% counterpart.
recContr : (A : Type_ ?) ≡> (B : Type_ ?)
           ≡> (R : A -> A -> Type_ ?)
           ≡> (p : HoTT_isContr B)
           -> Quotient A R -> B;
recContr = lambda _ _ _ _ _ R ≡>
            lambda p x -> case p
                | isContr a f => Quotient_elim (R := R)
                                               (lambda _ -> a)
                                               (p := lambda a a' r -> Eq_refl)
                                               x;

%% FIXME: Quotient_elim should be named Quotient_rec?
%%  This definition requires dependent elimination of `Quotient`
%%
%% rec2 : (A : Type_ ?) ≡> (B : Type_ ?) ≡> (C : Type_ ?)
%%        ≡> (R : A -> A -> Type_ ?) ≡> (S : B -> B -> Type_ ?)
%%        ≡> (C_isSet : HoTT_isSet C)
%%        -> (f : A -> B -> C)
%%        -> ((a : A) -> (b : A) -> (c : B) -> R a b -> Eq (f a c) (f b c))
%%        -> ((a : A) -> (b : B) -> (c : B) -> S b c -> Eq (f a b) (f a c))
%%        -> Quotient A R -> Quotient B S -> C;
%% rec2 = lambda _ _ _ _ _ A B C R S ≡>
%%        lambda C_isSet f feql feqr ->
%%           Quotient_elim (R := R)
%%                         (lambda a ->
%%                           lambda b -> Quotient_elim (R := S) (f a)
%%                                                     (p := feqr a) b)
%%                         (p := lambda a a' r ->
%%                           let
%%                             eqf : (b : B) -> Eq (f a b) (f a' b);
%%                             eqf b = feql a a' b r;
%%                             eqf' : Eq (f a) (f a');
%%                             eqf' = Eq_funext (f := f a) (g := f a') eqf;
%%                             p : (x : Quotient B S) ->
%%                                 HoTT_isProp (Eq (Quotient_elim (R := S) (f a)
%%                                                                (p := feqr a) x)
%%                                                 (Quotient_elim (R := S) (f a')
%%                                                                (p := feqr a') x));
%%                             p x = C_isSet (Quotient_elim (R := S) (f a)
%%                                                          (p := feqr a) x)
%%                                           (Quotient_elim (R := S) (f a')
%%                                                          (p := feqr a') x);
%%                             res : (x : Quotient B S) ->
%%                                   (Eq (Quotient_elim (R := S) (f a)
%%                                                      (p := feqr a) x)
%%                                       (Quotient_elim (R := S) (f a')
%%                                                      (p := feqr a') x));
%%                             res x = Quotient_elim (A := B)
%%                                                            %% FIXME: We need depelim here
%%                                                   (B := Eq (f a b) (f a' b))
%%                                                   (R := S)
%%                                                   eqf
%%                                                   (p := lambda u v s ->
%%                                                          Eq_eq (f := lambda i ≡>
%%                                                                    p (Eq_uneq (p := Quotient_eq (R := S) s) (i := i))
%%                                                                      (eqf u) (eqf v)))
%%                                                   x;
%%                           in
%%                             Eq_funext (f := Quotient_elim (R := S) (f a)
%%                                                           (p := feqr a))
%%                                       (g := Quotient_elim (R := S) (f a')
%%                                                           (p := feqr a'))
%%                                       res);

%% Lemma 6.10.2 in HoTT book, to prove this we need to
%% apply propositional truncation on SurjectiveQuotientProof.
%% type SurjectiveQuotientProof (A : ?) (x : Quotient A ?R)
%%    | surjectiveQuotientProof (a : A) (Eq (Quotient_in a) x);
%% Quotient_in_surjective : (x : Quotient ?A ?R) -> ||SurjectiveQuotientProof ?A ?R x||₁;

%% Given a proof that a unary operation preserves the underlying
%% relation, we can apply the operation to the quotiented type.
quotUnaryOp : (A : Type_ ?)
              ≡> (R : A -> A -> Type_ ?)
              ≡> (op : A -> A)
              -> ((a : A) -> (a' : A) -> R a a' -> R (op a) (op a'))
              -> Quotient A R -> Quotient A R;
quotUnaryOp = lambda _ _ A R ≡>
              lambda op h x ->
                 let
                   opPreservesQuotient : (a : A) -> (a' : A) -> R a a' ->
                                         Eq (t := Quotient A R)
                                            (Quotient_in (op a))
                                            (Quotient_in (op a'));
                   opPreservesQuotient a a' r = Quotient_eq (R := R) (h a a' r);
                 in
                   Quotient_elim (R := R)
                                 (lambda a -> Quotient_in (op a))
                                 (p := opPreservesQuotient)
                                 x;
