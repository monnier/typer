%%% decltype.typer --- Extract type of expression

%% Get type of expression:
%%
%%     decl-type E
%%
%% returns the type of `E`.
decl-type = lambda (t : Type) => lambda (_ : t) -> t;

%%
%% It mostly work as exemples below work fine
%%   but I can't do somethings like:
%%       y = decl-type x;
%%

%%
%% Some tests and exemples
%%

int-var : Int;
int-var = 100;

%%
%% Error "Metavar in erase_type":
%%     int = decl-type int-var;
%%

flt-var : Float;
flt-var = 0.1;

str-var : String;
str-var = "abc";

bool-var : Bool;
bool-var = true;

take-int : (decl-type int-var) -> (decl-type (1 : Int));
take-int n = n + n;

take-flt : (decl-type flt-var) -> (decl-type 1.0);
take-flt x = Float_+ x x;

take-str : (decl-type str-var) -> (decl-type "123");
take-str s = String_concat s s;

%%
%% Next are exemple where we really need
%%   type annotation
%%

take-bool : (decl-type bool-var) -> (decl-type true);
take-bool b = case b
  | true  => false
  | false => true;

rec-int : (decl-type int-var) -> (decl-type int-var);
rec-int n = if (Int_> n 0) then
              (n + rec-int (n - 1))
            else
              (0);
