(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2023  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          print out each compilation' steps
 *
 * --------------------------------------------------------------------------- *)

open Typerlib

(* Utilities *)
open Fmt

(* ASTs *)
open Lexp

(* AST reader *)
open Prelexer
open Lexer
open Eval

(* definitions *)
open Grammar

(* environments *)
open Debruijn
open Env

(*          Argument parsing        *)
let arg_print_options = ref SMap.empty
let arg_files = ref []
let debug_arg = ref 0

let add_p_option name () =
    debug_arg := (!debug_arg) + 1;
    arg_print_options := SMap.add name true (!arg_print_options)

let get_p_option name =
    try let _ = SMap.find name (!arg_print_options) in
        true
    with
        Not_found -> false

let arg_defs = [
    ("-typecheck",
        Arg.Unit (add_p_option "typecheck"), " Enable type checking");

    (*  Debug *)
    ("-lctx",
        Arg.Unit (add_p_option "lctx"), " Print lexp context");
    ("-rctx",
        Arg.Unit (add_p_option "rctx"), " Print runtime context");
    ("-all",
        Arg.Unit (fun () ->
            add_p_option "lctx" ();
            add_p_option "rctx" ();),
        " Print all debug info");
]

let parse_args () =
  Arg.parse arg_defs (fun s -> arg_files:= s::!arg_files) ""

let make_default () =
    arg_print_options := SMap.empty;
    add_p_option "lctx" ()

let main () =
    parse_args ();

    let arg_n = Array.length Sys.argv in

    let usage =
        "\n  Usage:   " ^ Sys.argv.(0) ^ " <file_name> [options]\n" in

    (*  Print Usage *)
    if arg_n == 1 then
        (Arg.usage (Arg.align arg_defs) usage)

    else(
        (if (!debug_arg) = 0 then make_default ());

        let filename = List.hd (!arg_files) in

        (* get pretokens*)
        print_string yellow;
        let source = Source.of_path filename in
        let pretoks = prelex source in
        print_string reset;

        (* get sexp/tokens *)
        print_string yellow;
        let toks = lex default_stt pretoks in
        print_string reset;

        (* get lexp *)
        let octx = Elab.default_ectx in

        (* debug lexp parsing once merged *)
        print_string yellow;
        let lexps, nctx = try Elab.lexp_p_decls [] toks octx
          with e ->
            print_string reset;
            raise e in
        print_string reset;

        (* use the new way of parsing expr *)
        let ctx = nctx in
        let flexps = List.flatten lexps in

        (* get typecheck context *)
        (if (get_p_option "typecheck") then(
            print_string (make_title " TYPECHECK ");

            let cctx = ectx_to_lctx ctx in
            (* run type check *)
            List.iter (fun (_, lxp, _)
                       -> let _ = OL.check cctx lxp in ())
                      flexps;

            print_string ("    " ^ (make_line '-' 76));
            print_string "\n";));

        (if (get_p_option "lctx") then(
           print_lexp_ctx (ectx_to_lctx nctx); print_string "\n"));

        (* Type erasure *)
        let _, clean_lxp =
          List.fold_left_map OL.clean_decls (ectx_to_lctx nctx) lexps
        in

        (* Eval declaration *)
        let rctx = Elab.default_rctx in
        print_string yellow;
        let rctx = (try eval_decls_toplevel clean_lxp rctx;
            with e ->
                print_string reset;
                print_rte_ctx (!global_eval_ctx);
                print_eval_trace None;
                raise e) in
        print_string reset;

        (if (get_p_option "rctx") then(
            print_rte_ctx rctx; print_string "\n"));

        (* Eval Each Expression *)
        print_string (make_title " Eval Print ");

        (try
            (* Check if main is present *)
            let main = (senv_lookup "main" nctx) in

            (* get main body *)
            let body = (get_rte_variable (dsinfo, Some "main") main rctx) in

            (* eval main *)
                print_eval_result 1 body None

        with
            Not_found -> ()
        )
    )

let _ = main ()
