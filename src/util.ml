(* util.ml --- Misc definitions for Typer.  -*- coding: utf-8 -*-

Copyright (C) 2011-2021  Free Software Foundation, Inc.

Author: Stefan Monnier <monnier@iro.umontreal.ca>
Keywords: languages, lisp, dependent types.

This file is part of Typer.

Typer is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

Typer is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.  *)

module SMap = Map.Make(String)
module IMap = Map.Make(Int)

type location = Source.Location.t
let dummy_location = Source.Location.dummy

(*************** DeBruijn indices for variables *********************)

(* Occurrence of a variable's symbol: we use DeBruijn index, and for
 * debugging purposes, we remember the name that was used in the source
 * code.  *)

type bottom = | B_o_t_t_o_m_ of bottom

let get_vname_name_option vname =
  let (_loc, name) = vname in name

let get_vname_name vname =
  let (_loc, name) = vname in
  match name with
  | Some n -> n
  | _ -> "" (* FIXME: Replace with dummy_name ? *)

let maybename n =
  match n with
  | None -> "<anon>"
  | Some v -> v

let string_implode chars = String.concat "" (List.map (String.make 1) chars)
let string_sub str b e = String.sub str b (e - b)

let str_split str sep =
    let str = String.trim str in
    let n = String.length str in

    if n = 0 then []
    else (

        let ret = ref [] in
        let buffer = Buffer.create 10 in
            Buffer.add_char buffer (str.[0]);

        for i = 1 to n - 1 do
            if str.[i] = sep then (
                ret := (Buffer.contents buffer)::(!ret);
                Buffer.reset buffer)
            else
                Buffer.add_char buffer (str.[i]);
        done;
        (if (Buffer.length buffer) > 0 then
            ret := (Buffer.contents buffer)::(!ret));

        List.rev (!ret))

let utf8_head_p (c : char) : bool
  = Char.code c < 128 || Char.code c >= 192

(* It seemed good to use the prime number 31.
 * FIXME: Pick another one ? *)
let combine_hash e1 e2 = (e1 * 31) lxor e2

let rec combine_hashes li =
 match li with
 | [] -> 31
 | e :: l -> combine_hash (e * 31) (combine_hashes l)

let get_stats_hashtbl stats =
  let (tl, ne, sumb, smallb, medianb, bigb) = stats in
    Printf.printf "\n\ttable length: %i\n
      number of entries: %i\n
      sum of bucket lengths: %i\n
      smallest bucket length: %i\n
      median bucket length: %i\n
      biggest bucket length: %i\n"
      tl ne sumb smallb medianb bigb
