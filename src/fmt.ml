(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2020  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          Various printing functions, including pretty-printing for
 *          types in Ir module.
 *
 * ---------------------------------------------------------------------------*)

open Ir
open Util

type Format.stag += Indexed of int

(* print n char 'c' *)
let make_line c n = String.make n c

(* Table Printing helper *)
let make_title title =
    let title_n = String.length title in
    let p = title_n mod 2 in
    let sep_n = (80 - title_n - 4) / 2 in
    let lsep = (make_line '=' sep_n) in
    let rsep = (make_line '=' (sep_n + p)) in
        ("    " ^ lsep ^ title ^ rsep ^ "\n")

let make_rheader (head: ((char * int) option * string) list) =
  print_string "    | ";
  let print_header (o, name) =
    (match o with
     | Some ('r', size) -> Printf.printf "%*s" size name
     | Some ('l', size) -> Printf.printf "%-*s" size name
     | _ -> print_string name);
    print_string " | "
  in
  List.iter print_header head;
  print_string "\n"

let make_sep c = "    " ^ (make_line c 76) ^ "\n"


(* used to help visualize the call trace *)
let print_ct_tree i =
    let rec loop j =
        if j = i then () else
        match j with
            | _ when (j mod 2) = 0 -> print_char '|'; loop (j + 1)
            | _ -> print_char ':'; loop (j + 1) in
    loop 0

(* Colors *)
let red_f : _ format6 = "\x1b[31m"
let green_f : _ format6 = "\x1b[32m"
let yellow_f : _ format6 = "\x1b[33m"
let blue_f : _ format6 = "\x1b[34m"
let magenta_f : _ format6 = "\x1b[35m"
let cyan_f : _ format6 = "\x1b[36m"
let reset_f : _ format6 = "\x1b[0m"

let red = string_of_format red_f
let green = string_of_format green_f
let blue = string_of_format blue_f
let yellow = string_of_format yellow_f
let magenta = string_of_format magenta_f
let cyan = string_of_format cyan_f
let reset = string_of_format reset_f

let color_string color str =
  color ^ str ^ reset

let formatter_of_out_channel (chan : out_channel) : Format.formatter =
  let open Format in
  let f = formatter_of_out_channel chan in
  let isatty = chan |> Unix.descr_of_out_channel |> Unix.isatty in
  let stag_fns : formatter_stag_functions =
    {
      mark_open_stag =
        if isatty
        then
          function
          | String_tag ("red") -> red
          | String_tag ("green") -> green
          | String_tag ("yellow") -> yellow
          | String_tag ("blue") -> blue
          | String_tag ("magenta") -> magenta
          | String_tag ("cyan") -> cyan
          | _ -> ""
        else
          Fun.const "";
      mark_close_stag =
        if isatty
        then
          function
          | String_tag ("red" | "green" | "yellow" | "blue" | "magenta" | "cyan")
            -> reset
          | _ -> ""
        else
          Fun.const "";
      print_open_stag = Fun.const ();
      print_close_stag = Fun.const ();
    }
  in
  Format.pp_set_formatter_stag_functions f stag_fns;
  Format.pp_set_tags f true;
  f


(*** Prelexer pretty-printing functions ***)

let rec pp_print_pretoken (f : Format.formatter) (pretoken : pretoken) : unit =
  Format.pp_open_stag f (Source.Located (pretoken_location pretoken));
  begin match pretoken with
  | Preblock (_, pretokens)
    -> Format.fprintf f "@[<hov 1>{%a}@]" pp_print_pretoken_list pretokens
  | Pretoken (_, text) -> Format.pp_print_string f text
  | Prestring (_, text) -> Format.fprintf f {|"%s"|} text
  end;
  Format.pp_close_stag f ()

and pp_print_pretoken_list (f : Format.formatter) (pretokens : pretoken list) : unit =
  Format.pp_print_list ~pp_sep:Format.pp_print_space pp_print_pretoken f pretokens


(*** S-exp pretty-printing functions ***)

and escape_sym_name (name : string) : string =
  let is_normal_char c =
    c > ' '
    && c <> '\\'
    && c <> '%'
    && c <> '"'
    && c <> '{'
    && c <> '}'
    && Grammar.default_stt.(Char.code c) = Grammar.CKnormal in
  let escape_char c =
    let cc = Seq.return c in
    if is_normal_char c then cc else Seq.cons '\\' cc in
  if String.for_all is_normal_char name
  then name
  else name |> String.to_seq |> Seq.flat_map escape_char |> String.of_seq

and pp_print_sexp (f : Format.formatter) (sexp : sexp) : unit =
  Format.pp_open_stag f (Source.Located (sexp_location sexp));
  begin match sexp with
  | Block (_, pretokens)
    -> Format.fprintf f "@[<hov 1>{%a}@]" pp_print_pretoken_list pretokens
  | Symbol (_, "")
    -> Format.pp_print_string f "ε"
  | Symbol (_, name)
    -> Format.pp_print_string f name
  | String (_, value)
    -> Format.fprintf f {|"%s"|} (String.escaped value)
  | Integer (_, value)
    -> Z.pp_print f value
  | Float (_, value)
    -> Format.pp_print_float f value
  | Node (_, head, tail)
    -> Format.fprintf f "@[<hov 1>(%a)@]" pp_print_sexp_list (head :: tail)
  end;
  Format.pp_close_stag f ()

and pp_print_sexp_list (f : Format.formatter) (sexps : sexp list) : unit =
  Format.pp_print_list ~pp_sep:Format.pp_print_space pp_print_sexp f sexps

and string_of_sexp (sexp : sexp) : string =
  Format.asprintf "%a" pp_print_sexp sexp

and print_sexp (sexp : sexp) : unit =
  Format.printf "%a" pp_print_sexp sexp

(** Prints the symbol. Unlike simply printing the name of the symbol, this
    function will escape special characters so it can be read back.
    Note that we do not track the lexical environment whence it came, so we
    assume the default configuration. *)
and pp_print_symbol (f : Format.formatter) (_, name : symbol) : unit =
  name |> escape_sym_name |> Format.pp_print_string f

and string_of_symbol (sym : symbol) : string =
  Format.asprintf "%a" pp_print_symbol sym

and string_of_vname ?(default : string = "<anon>") : vname -> string = function
  | (_, None) -> default
  | (_, Some (name)) -> escape_sym_name name

and pp_print_vname
      ?(default : string option) (f : Format.formatter) (name : vname)
    : unit =
  Format.pp_print_string f (string_of_vname ?default name)


(*** Lexp pretty-printing functions ***)

and arg_kind_to_arrow : arg_kind -> string = function
  | Anormal -> "->" | Aimplicit -> "=>" | Aerasable -> "≡>"

and arg_kind_to_colon : arg_kind -> string = function
  | Anormal -> ":" | Aimplicit -> "::" | Aerasable -> ":::"

and pp_enable_print_indices (f : Format.formatter) : unit =
  let fns = Format.pp_get_formatter_stag_functions f () in
  let fns' =
    {
      fns with
      print_open_stag =
        begin function
          | Indexed _ -> ()
          | stag -> fns.print_open_stag stag
        end;
      print_close_stag =
        begin function
          | Indexed (index) -> Format.fprintf f "@{<green>[%d]@}" index
          | stag -> fns.print_close_stag stag
        end;
    }
  in
  Format.pp_set_formatter_stag_functions f fns';
  Format.pp_set_tags f true

and pp_print_var (f : Format.formatter) (name : vname) (index : int) : unit =
  let open Format in
  pp_open_stag f (Indexed (index));
  pp_print_string f (string_of_vname name);
  pp_close_stag f ()

and pp_print_proj :
      'expr 'field.
      Format.formatter
      -> (Format.formatter -> 'expr -> unit)
      -> 'expr
      -> (Format.formatter -> 'field -> unit)
      -> 'field
      -> unit =
  fun f pp_print_expr e pp_print_field field_name ->
    Format.fprintf
      f
      "@[<hov 1>(__.__@ (%a)@ %a)@]"
      pp_print_expr
      e
      pp_print_field
      field_name

and pp_print_builtin (f : Format.formatter) (name : symbol) : unit =
  Format.fprintf f "##%a" pp_print_symbol name

and pp_print_let :
      'decls 'expr.
      Format.formatter
      -> (Format.formatter -> 'decls -> unit)
      -> 'decls
      -> (Format.formatter -> 'expr -> unit)
      -> 'expr
      -> unit =
  fun f pp_print_decl_block decls pp_print_body body ->
    Format.fprintf
      f
      "@[<v>@[<v 2>@{<magenta>let@}@;%a@]@;@[<v 2>@{<magenta>in@}@;%a;@]@]"
      pp_print_decl_block
      decls
      pp_print_body
      body

and pp_print_call :
      'expr 'arg.
      Format.formatter
      -> (Format.formatter -> 'expr -> unit)
      -> 'expr
      -> (Format.formatter -> 'arg -> unit)
      -> 'arg list
      -> unit =
  fun f pp_print_callee callee pp_print_arg args ->
    let open Format in
    fprintf f "@[<hov 1>(%a" pp_print_callee callee;
    List.iter (fun arg -> fprintf f "@ %a" pp_print_arg arg) args;
    fprintf f ")@]"

and pp_print_lambda :
      'expr 'param.
      Format.formatter
      -> (Format.formatter -> 'param -> unit)
      -> 'param
      -> arg_kind
      -> (Format.formatter -> 'expr -> unit)
      -> 'expr
      -> unit =
  fun f pp_print_param param param_kind pp_print_body body ->
    Format.fprintf
      f
      "@[<v 1>(@{<magenta>lambda@} %a %s@;%a)@]"
      pp_print_param
      param
      (arg_kind_to_arrow param_kind)
      pp_print_body
      body

and pp_print_case :
      'meta 'expr 'binding.
      Format.formatter
      -> (Format.formatter -> 'expr -> unit)
      -> 'expr
      -> (Format.formatter -> 'binding -> unit)
      -> ('meta * 'binding list * 'expr) SMap.t
      -> (vname * 'expr) option
      -> unit =
  fun f pp_print_expr e pp_print_binding branches default ->
    let open Format in
    let pp_print_branch f cons_name (_, args, body) =
      fprintf
        f
        "@ @[<hov 2>| %s@ @[<hov 1>%a@] ->@ %a@]"
        cons_name
        (pp_print_list ~pp_sep:pp_print_space pp_print_binding)
        args
        pp_print_expr
        body
    in
    let pp_print_branches f branches =
      SMap.iter (pp_print_branch f) branches
    in
    let pp_print_default f branch =
      match branch with
      | None -> ()
      | Some ((binding, body))
        -> fprintf
            f
            "@ @[<hov 2>| %s ->@ %a@]"
            (string_of_vname binding)
            pp_print_expr
            body
    in
    fprintf
      f
      "@[<v 1>(@[<hov 2>@{<magenta>case@}@ %a@]%a%a)@]"
      pp_print_expr
      e
      pp_print_branches
      branches
      pp_print_default
      default

and pp_print_lexp (f : Format.formatter) (e, _ : lexp) : unit =
  let open Format in
  match e with
  | Imm (_, v)
    -> fprintf f "%s" (string_of_value v)

  | Susp (e, s)
    -> fprintf f "%a@ %a" pp_print_lexp e pp_print_subst s

  | Builtin (name, _)
    -> pp_print_builtin f name

  | Var ((name, index))
    -> pp_print_var f name index

  | Metavar (index, subst, name)
    -> pp_open_stag f (Indexed (index));
       fprintf f "?%s%a" (string_of_vname name) pp_print_subst subst;
       pp_close_stag f ()

  | Proj (_, e, field_name)
    -> pp_print_proj f pp_print_lexp e pp_print_symbol field_name

  | Cons (e, (_, cons_name))
    -> fprintf
         f
         "@[<hov 1>(@{<magenta>datacons@}@ %a@ %s)@]"
         pp_print_lexp
         e
         cons_name

  | Lambda (kind, param_name, param_ty, body)
    -> pp_print_lambda
         f
         (fun f (param_name, param_ty)
          -> fprintf
               f
               "@[<hov 2>(%s@ : %a)@]"
               (string_of_vname param_name)
               pp_print_lexp
               param_ty)
         (param_name, param_ty)
         kind
         pp_print_lexp
         body

  | Arrow (_, kind, (_, None), arg_ty, body_ty)
    -> fprintf
         f
         "@[<hov 2>(%a@ %s %a)@]"
         pp_print_lexp
         arg_ty
         (arg_kind_to_arrow kind)
         pp_print_lexp
         body_ty

  | Arrow (_, kind, (_, Some (name)), arg_ty, body_ty)
    -> fprintf
         f
         "@[<hov 2>(%s@ : %a@ %s %a)@]"
         name
         pp_print_lexp
         arg_ty
         (arg_kind_to_arrow kind)
         pp_print_lexp
         body_ty

  | Let (_, decls, body)
    -> pp_print_let f pp_print_lexp_decl_block decls pp_print_lexp body

  | Call (_, callee, args)
    -> pp_print_call f pp_print_lexp callee (fun f (_, arg) -> pp_print_lexp f arg) args

  | Inductive (_, name, params, constructors)
    -> let pp_print_param f (kind, name, ty) =
         match kind, name with
         | Anormal, (_, None) -> fprintf f "@ @[<hov 1>(%a)@]" pp_print_lexp ty
         | _, _
           -> fprintf
                f
                "@ @[<hov 1>(%s %s@ %a)@]"
                (string_of_vname name)
                (arg_kind_to_colon kind)
                pp_print_lexp
                ty
       in
       let pp_print_params f params =
         List.iter (pp_print_param f) params
       in
       let pp_print_cons name params =
         fprintf f "@ @[<hov 1>(%s%a)@]" name pp_print_params params
       in

       fprintf f "@[<hov 1>(@{<magenta>typecons@} ";
       begin match params with
       | [] -> pp_print_symbol f name
       | _ :: _
         -> fprintf
              f
              "@[<hov 1>(%a%a)@]"
              pp_print_symbol
              name
              pp_print_params
              params
       end;
       SMap.iter pp_print_cons constructors;
       fprintf f ")@]";

  | Case (_, e, _, branches, default)
    -> let pp_print_binding f (_, name) =
         pp_print_string f (string_of_vname ~default:"_" name)
       in
       pp_print_case f pp_print_lexp e pp_print_binding branches default

  | Sort (_, sort)
    -> pp_print_sort f sort

  | SortLevel ( sort_level)
    -> pp_print_sort_level f sort_level

and pp_print_subst (f : Format.formatter) (subst : subst) : unit =
  let open Format in
  match subst with
  | Subst.Identity (o)
    -> fprintf f "↑%d" o
  | Subst.Cons (l, s, 0)
    -> fprintf f "(%a) · %a" pp_print_lexp l pp_print_subst s
  | Subst.Cons (l, s, o)
    -> fprintf f "(↑%d %a)" o pp_print_subst (Subst.cons l s)

and pp_print_sort (f : Format.formatter) (sort : sort) : unit =
  let open Format in
  match sort with
  | StypeLevel -> pp_print_string f "##TypeLevel.Sort"
  | StypeOmega -> pp_print_string f "##Type_ω"
  | Stype ((SortLevel SLz, _)) -> pp_print_string f "##Type"
  | Stype ((SortLevel (SLsucc (SortLevel SLz, _)), _))
    -> pp_print_string f "##Type1"
  | Stype (e)
    -> fprintf f "@[<hov 1>(##Type_@ %a)@]" pp_print_lexp e

and pp_print_sort_level (f : Format.formatter) (level : sort_level) : unit =
  let open Format in
  match level with
  | SLz -> pp_print_string f "##TypeLevel.z"
  | SLsucc (e) -> fprintf f "@[<hov 1>(##TypeLevel.succ@ %a)@]" pp_print_lexp e
  | SLlub (l, r)
    -> fprintf f "@[<hov 1>(##TypeLevel.∪@ %a@ %a)@]" pp_print_lexp l pp_print_lexp r

and pp_print_lexp_decl_block (f : Format.formatter) (ldecls : ldecls) : unit =
  let open Format in
  pp_open_vbox f 0;
  pp_print_list
    ~pp_sep:pp_print_space
    (fun f (name, _, ty)
     -> fprintf f "@[<hov 2>%s :@ %a;@]" (string_of_vname name) pp_print_lexp ty)
    f
    ldecls;
  pp_print_cut f ();
  pp_print_list
    ~pp_sep:pp_print_space
    (fun f (name, body, _)
     -> fprintf f "@[<hov 2>%s =@ %a;@]" (string_of_vname name) pp_print_lexp body)
    f
    ldecls;
  pp_close_box f ()

and pp_print_lexp_decls (f : Format.formatter) (ldecls : ldecls list) : unit =
  let open Format in
  fprintf
    f
    "@[<v 0>%a@]"
    (pp_print_list ~pp_sep:pp_print_cut pp_print_lexp_decl_block)
    ldecls

and print_lexp (l : lexp) : unit =
  Format.printf "%a" pp_print_lexp l;
  Format.print_flush ()

and string_of_lexp (l : lexp) : string =
  Format.asprintf "%a" pp_print_lexp l


(*** E-Lexp pretty-printing functions ***)

and pp_print_elexp (f : Format.formatter) (e : elexp) : unit =
  let open Format in
  match e with
  | Eimm (_, value)
    -> fprintf f "%s" (string_of_value value)

  | Evar (name, index)
    -> pp_print_var f name index

  | Eproj (_, e, field_name)
    -> pp_print_proj f pp_print_elexp e pp_print_int field_name

  | Ebuiltin (name)
    -> pp_print_builtin f name

  | Elet (_, decls, body)
    -> pp_print_let f pp_print_elexp_decl_block decls pp_print_elexp body

  | Elambda (param_name, body)
    -> pp_print_lambda
         f
         (fun f param_name
          -> fprintf f "(%a)" (pp_print_vname ~default:"_") param_name)
         param_name
         Anormal
         pp_print_elexp
         body

  | Ecall (callee, args)
    -> pp_print_call f pp_print_elexp callee pp_print_elexp args

  | Econs (_, name)
    -> fprintf f "@{<magenta>datacons@}@ %a" pp_print_symbol name

  | Ecase (_, e, branches, default)
    -> pp_print_case
         f pp_print_elexp e (pp_print_vname ~default:"_") branches default

  | Etype (e)
    -> fprintf f "Type(%a)" pp_print_lexp e

and pp_print_elexp_decl_block (f : Format.formatter) (decls : eldecls) : unit =
  let open Format in
  let pp_print_decl (f : Format.formatter) (name, body : eldecl) : unit =
    fprintf f "@[<hov 2>%s =@ %a;@]" (string_of_vname name) pp_print_elexp body
  in
  fprintf
    f
    "@[<v 0>%a@]"
    (pp_print_list ~pp_sep:pp_print_space pp_print_decl)
    decls

and pp_print_elexp_decls (f : Format.formatter) (eldecls : eldecls list) : unit =
  let open Format in
  fprintf
    f
    "@[<v 0>%a@]"
    (pp_print_list ~pp_sep:pp_print_cut pp_print_elexp_decl_block)
    eldecls

and string_of_elexp (elexp : elexp) : string =
  Format.asprintf "%a" pp_print_elexp elexp


(*** Value pretty-printing functions ***)

and string_of_value v =
  match v with
    | Vin   _ -> "in_channel"
    | Vout  _ -> "out_channel"
    | Vundefined -> "<undefined!>"
    | Vcommand _ -> "command"
    | Vstring  s -> "\"" ^ s ^ "\""
    | Vbuiltin s -> s
    | Vint     i -> string_of_int i
    | Vinteger i -> Z.to_string i
    | Vfloat   f -> string_of_float f
    | Vsexp    s -> string_of_sexp s
    | Vtype    e -> string_of_lexp e
    | Closure  ((_, s), elexp, _)
      -> "(lambda " ^ maybename s ^ " -> " ^ string_of_elexp elexp ^ ")"
    | Vcons    ((_, s), lst)
      -> let args = List.fold_left
                      (fun str v -> str ^ " " ^ string_of_value v)
                      "" lst in
         "(" ^ s ^ args ^ ")"
    | Vref     (v) -> ("Ref of "^(string_of_value (!v)))
    | Velabctx _ -> ("Velabctx")
    | Varray a -> if ( (Array.length a) = 0 )
      then "[]"
      else ( let
        str = (Array.fold_left
          (fun str v -> (str^(string_of_value v)^";")) "" a)
      in ("["^(String.sub str 0 ((String.length str) - 1))^"]") )
