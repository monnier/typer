(* lexer.ml --- Second half of lexical analysis of Typer.

Copyright (C) 2011-2021  Free Software Foundation, Inc.

Author: Stefan Monnier <monnier@iro.umontreal.ca>
Keywords: languages, lisp, dependent types.

This file is part of Typer.

Typer is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

Typer is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.  *)

open Util
open Prelexer
open Ir
open Sexp
open Grammar

(*************** The Lexer phase *********************)

let unescape str =
  let rec split b =
    if b >= String.length str then []
    else let e = try String.index_from str b '\\'
                 with Not_found -> String.length str in
         string_sub str b e :: split (e + 1)
  in String.concat "" (split 0)

let lex_number
      (source : #Source.t)
      (start_point : Source.Point.t)
    : sexp =

  let rec lex_exponent () =
    match source#peek with
    | Some ('0' .. '9')
      -> source#advance;
         lex_exponent ()

    | _
      -> let source, location = source#slice start_point in
         Float (location, float_of_string source)
  in

  let lex_exponent_sign () =
    match source#peek with
    | Some ('0' .. '9' | '+' | '-')
      -> source#advance;
         lex_exponent ()

    (* TODO: error if there are no signs or digits after the 'e'? *)
    | _
      -> let source, location = source#slice start_point in
         Float (location, float_of_string source)
  in

  let rec lex_fractional () =
    match source#peek with
    | Some ('0' .. '9')
      -> source#advance;
         lex_fractional ()

    | Some ('e' | 'E')
      -> source#advance;
         lex_exponent_sign ()

    | _
      -> let source, location = source#slice start_point in
         Float (location, float_of_string source)
  in

  let rec lex_integer () =
    match source#peek with
    | Some ('0' .. '9')
      -> source#advance;
         lex_integer ()

    | Some ('.')
      -> source#advance;
         lex_fractional ()

    | Some ('e' | 'E')
      -> source#advance;
         lex_exponent_sign ()

    | _
      -> let source, location = source#slice start_point in
         Integer (location, Z.of_string source)
  in

  lex_integer ()

(* Splits one symbol from the beginning of the source. *)
let lex_symbol
      (token_env : token_env)
      (source : #Source.t)
      (start_point : Source.Point.t)
    : sexp =

  let mksym start_point escaped =
    if Source.Point.equal start_point source#point
    then Sexp.epsilon (Source.Location.of_point source#container start_point)
    else
      let raw_name, location = source#slice start_point in
      let name = if escaped then unescape raw_name else raw_name in
      Sexp.symbol ~location name
  in

  let rec loop start_point prec lf (escaped : bool) =
    match source#peek with
    | None
      -> lf (mksym start_point escaped)

    | Some c
      -> (match token_env.(Char.code c) with
          | CKseparate
            -> lf (mksym start_point escaped)

          | CKinner prec'
            -> let left = mksym start_point escaped in
               let op_point = source#point in

               (* To be considered `inner`, an operator char needs to have
                  something on its LHS or its RHS, otherwise, treat it as a
                  normal char. `.` can be the normal function composition
                  operator as well. *)
               let lhs_p = not (Source.Point.equal start_point source#point) in
               source#advance;
               let rhs_p =
                 (match source#peek with
                  | None -> false
                  | Some c' -> CKseparate != token_env.(Char.code c'))
               in
               if lhs_p || rhs_p || lf dummy_epsilon <> dummy_epsilon
               then
                 let op_text, op_location = source#slice op_point in
                 let op =
                   Sexp.symbol
                     ~location:op_location (Printf.sprintf "__%s__" op_text)
                 in
                 let location s =
                   s
                   |> sexp_location
                   |> Source.Location.extend op_location
                   |> Source.Location.extend (sexp_location left)
                 in
                 let lf' =
                   if prec' > prec
                   then fun s -> lf (Node (location s, op, [left; s]))
                   else fun s -> Node (location s, op, [lf left; s])
                 in
                 loop source#point prec' lf' false
               else
                 loop start_point prec lf escaped

          | CKnormal
            -> (source#advance;
                let is_last = Option.is_none source#peek in
                match c with
                (* Skip next char, in case it's a special token. For utf-8,
                   simply advancing is risky but actually works: '\' counts as 1
                   and if the input is valid utf-8 the next byte has to be a
                   leading byte, so it has to count as 1 as well ;-) *)
                | '\\' when not is_last
                  -> source#advance;
                     loop start_point prec lf true

                | _ -> loop start_point prec lf escaped))
  in
  loop start_point 0 (fun s -> s) false

let split_presymbol
      (token_env : token_env)
      (source : #Source.t)
    : sexp list =

  let rec loop (acc : sexp list) : sexp list =
    match source#peek with
    | None -> List.rev acc
    | Some (c)
      -> let start_point = source#point in
         let token =
           match c with
           (* A negative number literal, or a symbol starting with '-'. *)
           | '-'
             -> (source#advance;
                 match source#peek with
                 | Some ('0' .. '9')
                   -> lex_number source start_point
                 | _
                   -> lex_symbol token_env source start_point)

           | '0' .. '9'
             -> source#advance;
                lex_number source start_point

           | _ when token_env.(Char.code c) = CKseparate
             -> source#advance;
                let name, location = source#slice start_point in
                Sexp.symbol ~location name

           | _
             -> lex_symbol token_env source start_point
         in
         loop (token :: acc)
  in
  loop []

let lex (token_env : token_env) (pretokens : pretoken list) : sexp list =
  let rec loop pretokens acc =
    match pretokens with
    | [] -> List.rev acc

    | Preblock (location, block_pretokens) :: pretokens'
      -> loop pretokens' (Block (location, block_pretokens) :: acc)

    | Prestring (location, text) :: pretokens'
      -> loop pretokens' (String (location, text) :: acc)

    | Pretoken (location, _) :: pretokens'
      -> let source = Source.of_location location in
         let tokens = split_presymbol token_env source in
         loop pretokens' (List.rev_append tokens acc)
  in
  loop pretokens []

let lex_source (source : #Source.t) tenv =
  let pretoks = prelex source in
  lex tenv pretoks

let sexp_parse_source (source : #Source.t) tenv grm limit =
  let toks = lex_source source tenv in
  sexp_parse_all_to_list grm toks limit
