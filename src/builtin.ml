(* builtin.ml --- Infrastructure to define built-in primitives
 *
 *      Copyright (C) 2016-2024  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 * There are several different issues with how to make the compiler's code
 * interact with code defined in Typer:
 *
 * ** Exporting primitives
 *
 * I.e. how to give a name and a type to a primitive implemented in OCaml
 *
 * There are several conflicting desires, here: we'd generally want the name,
 * the type (and the association) to be close to the primitive's definition, so
 * that adding a new primitive doesn't require changes in many files.
 *
 * But it's also good to have the type written in some Typer file, both for
 * the convenience of writing the code in Typer syntax with typer-mode support,
 * and also because error messages can easily refer to that file, so it can be
 * used for user-documentation.
 *
 * ** Importing definitions
 *
 * Sometimes we want some part of the core to be defined in Typer and used
 * from OCaml.  Examples are the type `Macro` along with the `expand-macro`
 * function or the type Bool/true/false used with various primitives.
 *
 * ** Intertwined dependencies
 *
 * The various importing/exporting might need to be interlaced.  Some exported
 * functions's types will want to refer to imported types, while some imported
 * definitions may want to refer to exported definitions.  So we'd like to be
 * able to do them in "any" order.
 *
 * ---------------------------------------------------------------------------*)

(* open Pexp   *) (* arg_kind *)

open Ir
open Lexp
module OL = Opslexp
module DB = Debruijn
module E = Env

let log_raise_error ?print_action ?loc fmt =
  Log.log_msg
    (fun s -> raise (Log.Internal_error s))
    Log.Error
    ?kind:None
    ~section:"BUILT-IN"
    ?print_action
    ?loc
    fmt

let predef_names = [
    "cons";                     (* FIXME: Should be used but isn't!  *)
    "nil";                      (* FIXME: Should be used but isn't!  *)
    "true";
    "false";
    "Macro";
    "Macro_expand";
]

(* FIXME: Actually, we should map the predefs to *values* since that's
 * where they're really needed!  *)
let predef_map : lexp SMap.t ref
  (* Pre-fill "Macro" with a dummy value, to avoid errors while reading
   * the builtins.typer file.  *)
  = ref (SMap.add "Macro" impossible SMap.empty)

let get_predef (name: string) (ctx: elab_context) : lexp =
  try let r = (DB.get_size ctx) - !builtin_size - 0 in
      let p = SMap.find name (!predef_map) in
      mkSusp p (S.shift r)
  with Not_found -> log_raise_error {|"%s" was not predefined|} name

let set_predef name lexp
  = predef_map := SMap.add name lexp (!predef_map)

(*                Builtin types               *)
let dloc    = DB.dloc
let dsinfo = DB.dsinfo

let op_binary t = mkArrow (dsinfo, Anormal, (dsinfo, None), t,
                           mkArrow (dsinfo, Anormal, (dsinfo, None), t, t))

let o2l_bool ctx b = get_predef (if b then "true" else "false") ctx

(* Typer list as seen during runtime.  *)
let o2v_list lst =
  (* FIXME: We're not using predef here.  This will break if we change
   * the definition of `List` in builtins.typer.  *)
  List.fold_left (fun tail elem
                  -> Vcons ((dloc, "cons"), [Vsexp (elem); tail]))
                 (Vcons ((dloc, "nil"), []))
                 (List.rev lst)


(* Typer list to Ocaml list *)
let v2o_list v =
  (* FIXME: We're not using predef here.  This will break if we change
   * the definition of `List` in builtins.typer.  *)
  let rec v2o_list acc v =
    match v with
    | Vcons ((_, "cons"), [hd; tl]) -> v2o_list (hd::acc) tl
    | Vcons ((_, "nil"), []) -> List.rev acc
    | _ ->
       log_raise_error
         ~loc:(E.value_location v)
         "Failed to convert the %s with value:\n%s\n to a list."
         (E.value_name v) (Fmt.string_of_value v)
  in
  v2o_list [] v

let new_builtin_type name =
  let t = mkBuiltin ((dloc, name), DB.type0) in
  (* Although the current context may not be empty when adding this builtins,
   * we still provide 0 as the type's DB offset, since `DB.type0` doesn't
   * reference any var/metavar, so the offset doesn't matter. *)
  OL.add_builtin_cst name 0 t;
  t

let register_builtin_csts () =
  (* Although the current context may not be empty when adding these builtins,
   * we still provide 0 as the types' DB offset, since none of these types
   * reference any var/metavar, so the offset doesn't matter in these cases. *)
  OL.add_builtin_cst "TypeLevel" 0 DB.type_level;
  OL.add_builtin_cst "TypeLevel_z" 0 DB.level0;
  OL.add_builtin_cst "Type" 0 DB.type0;
  OL.add_builtin_cst "Type0" 0 DB.type0;
  OL.add_builtin_cst "Type1" 0 DB.type1;
  OL.add_builtin_cst "Integer" 0 DB.type_integer;
  OL.add_builtin_cst "Float" 0 DB.type_float;
  OL.add_builtin_cst "String" 0 DB.type_string;
  OL.add_builtin_cst "Eq" 0 DB.type_eq;
  OL.add_builtin_cst "I" 0 DB.type_interval

let _ = register_builtin_csts ()
