(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2022  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          Type Erased Lexp expression
 *
 * --------------------------------------------------------------------------- *)

open Ir

module SMap = Util.SMap

let rec location : elexp -> Source.Location.t = function
  | Eimm (l, _) -> l
  | Evar ((l, _), _) -> sexp_location l
  | Eproj (l, _, _) -> l
  | Ebuiltin ((l, _)) -> l
  | Elet (l, _, _) -> l
  | Elambda ((l, _), _) -> sexp_location l
  | Ecall (f, _) -> location f
  | Econs (_, (l, _)) -> l
  | Ecase (l, _, _, _) -> l
  | Etype (e) -> Lexp.location e
