(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2023  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          Implements runtime environment
 *
 * --------------------------------------------------------------------------- *)

open Ir
open Lexp
open Fmt
open Printf
open Sexp

module M = Myers
module DB = Debruijn
module OL = Opslexp

let fatal ?print_action ?loc fmt =
  Log.log_fatal ~section:"ENV" ?print_action ?loc fmt
let warning ?print_action ?loc fmt =
  Log.log_warning ~section:"ENV" ?print_action ?loc fmt

let str_idx idx = "[" ^ (string_of_int idx) ^ "]"

let rec value_equal a b =
  match a, b with
    | Vint (i1), Vint (i2)          -> i1 = i2
    | Vinteger (i1), Vinteger (i2)  -> i1 = i2
    | Vstring (s1), Vstring (s2)    -> s1 = s2
    | Vbuiltin (s1), Vbuiltin (s2)  -> s1 = s2
    | Vfloat (f1), Vfloat (f2)      -> f1 = f2
    | Vsexp (a), Vsexp (b)          -> sexp_equal a b
    | Vin (c1), Vin (c2)            -> c1 = c2
    | Vout (_c1), Vout (c2)         -> c2 = c2
    | Vcommand (f1), Vcommand (f2)  -> f1 = f2
    | Vundefined, _ | _, Vundefined
      -> warning "Vundefined";
         false
    | Vtype _e1, Vtype _e2
      -> warning "Vtype";
         false

    | Closure (s1, _b1, _ctx1), Closure (s2, _b2, _ctx2)
      -> warning "Closure";
         if s1 != s2 then false else true

    | Vcons ((_, ct1), a1), Vcons ((_, ct2), a2)
      -> if not (ct1 = ct2) then false else
           not (List.exists2
                  (fun a b -> not (value_equal a b))
                  a1 a2)

    | Vref (v1), Vref (v2) -> value_equal (!v1) (!v2) (* FIXME: Really?  *)

    | Velabctx (e1), Velabctx (e2) -> (e1 = e2)

    | Varray (a1), Varray (a2) -> (a1 = a2)

    | _ -> false

let rec value_eq_list a b =
  match a, b with
    | [], [] -> true
    | v1::vv1, v2::vv2 -> value_equal v1 v2 && value_eq_list vv1 vv2
    | _ -> false

let value_location : value -> Source.Location.t = function
  | Vcons ((loc, _), _) -> loc
  | Closure (_, lxp, _) -> Elexp.location lxp
  (* location info was lost or never existed *)
  | _ -> Source.Location.dummy

let rec value_name v =
  match v with
    | Vin   _ -> "Vin"
    | Vout  _ -> "Vout"
    | Vint  _ -> "Vint"
    | Vinteger _ -> "Vinteger"
    | Vsexp  _ -> "Vsexp"
    | Vtype  _ -> "Vtype"
    | Vcons  _ -> "Vcons"
    | Vfloat _ -> "Vfloat"
    | Vundefined -> "Vundefined"
    | Vstring  _ -> "Vstring"
    | Closure  _ -> "Closure"
    | Vbuiltin _ -> "Vbuiltin"
    | Vcommand _ -> "Vcommand"
    | Vref v -> ("Vref of "^(value_name (!v)))
    | Velabctx _ -> ("Velabctx")
    | Varray _ -> ("Varray")

let value_string_with_type v ltype ctx =
  (* Handle some special cases, and use the default
     value string otherwise. *)
  let rec get_string ltype ctx =
    match lexp_lexp' (OL.lexp_whnf ltype ctx) with
      | Arrow (_, Aerasable, _, _, ret_ltype)
          (* Recurse on return type to handle chain of erasable arguments *)
          -> let shifted_ret_ltype = push_susp ret_ltype (S.shift (-1)) in
              sprintf "(lambda _ ≡> %s)" (get_string shifted_ret_ltype ctx)
      | Call (_, e, args) ->
          let e' = OL.lexp_whnf e ctx in
            (match args with
             (* Pretty print identity types *)
             | [_l; (_, t); (_, left); (_, right)]
                  when OL.conv_builtin_p ctx e' "Eq"
               -> Format.asprintf "%a = %a [ %a ]"
                         pp_print_clean_lexp left
                         pp_print_clean_lexp right
                         pp_print_clean_lexp t
             | [_; _; (_, t); (_, r)]
                 when OL.conv_builtin_p ctx e' "Quotient"
                 -> Format.asprintf "(Quotient.in %a %a)"
                            pp_print_clean_lexp t
                            pp_print_clean_lexp r
             | _ -> string_of_value v)
      | _ -> string_of_value v
  in get_string ltype ctx

(* Caller may optionally provide additional type information
   to allow the printer to produce a more informative output. *)
let value_print (vtp: value) (lexp_ctx: (lexp * lexp_context) option) =
  print_string (Option.fold
                  ~none:(string_of_value vtp)
                  ~some:(fun (ltype, ctx) -> value_string_with_type vtp ltype ctx)
                  lexp_ctx)

let make_runtime_ctx = M.nil

let get_rte_size (ctx: runtime_env): int = M.length ctx

let print_myers_list l print_fun start =
    let n = (M.length l) - 1 in
    print_string (make_title " ENVIRONMENT ");
    make_rheader [(None, "INDEX");
        (None, "VARIABLE NAME"); (Some ('l', 48), "VALUE")];
    print_string (make_sep '-');

    for i = start to n do
        printf "    | %5d | " (n -i);
        print_fun (M.nth (n - i) l);
    done;
    print_string (make_sep '=')

let print_rte_ctx_n (ctx: runtime_env) start =
  print_myers_list
    ctx
    (fun (n, vref) ->
      let g = !vref in
      let _ =
        match n with
        | (_, Some m) -> Printf.printf "%-12s  |  " m
        | _ -> print_string (make_line ' ' 12); print_string "  |  " in

      value_print g None; print_string "\n") start

(* Only print user defined variables *)
let print_rte_ctx ctx =
  print_rte_ctx_n ctx (!Lexp.builtin_size)

(* Dump the whole context *)
let dump_rte_ctx ctx =
  print_rte_ctx_n ctx 0

let get_rte_variable (name: vname) (idx: int)
                     (ctx: runtime_env): value =
  try
    let (defname, ref_cell) = (M.nth idx ctx) in
    let x = !ref_cell in
    match (defname, name) with
    | ((_, Some n1), (_, Some n2))
      -> if n1 = n2
         then x
         else
           fatal
             ~print_action:(fun () -> dump_rte_ctx ctx)
             {|Variable lookup failure. Expected "%s[%d]" got "%s"|} n2 idx n1
    | _ -> x
  with
  | Not_found
    -> let n = match name with (_, Some n) -> n | _ -> "" in
       fatal {|Variable lookup failure. Var: "%s" idx: %d|} n idx

let add_rte_variable (name:vname) (x: value) (ctx: runtime_env)
    : runtime_env =
  let valcell = ref x in
  M.cons (name, valcell) ctx

let set_rte_variable idx name (v: value) (ctx : runtime_env) =
  let (n, ref_cell) = (M.nth idx ctx) in

  (match (n, name) with
   | ((_, Some n1), (_, Some n2)) when n1 != n2
     -> fatal {|Variable names must match: "%s" vs "%s"|} n1 n2
   | _ -> ());

  ref_cell := v

(* Select the n first variable present in the env *)
let nfirst_rte_var n ctx =
    let rec loop i acc =
        if i < n then
            loop (i + 1) ((get_rte_variable Lexp.vdummy i ctx)::acc)
        else
            List.rev acc in
    loop 0 []
