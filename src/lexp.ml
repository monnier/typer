(* lexp.ml --- Lambda-expressions: the core language.

Copyright (C) 2011-2023  Free Software Foundation, Inc.

Author: Stefan Monnier <monnier@iro.umontreal.ca>
Keywords: languages, lisp, dependent types.

This file is part of Typer.

Typer is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

Typer is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.  *)

open Ir

module L = List
module S = Subst
module SMap = Util.SMap
module U = Util

(*************** Elaboration to Lexp *********************)

(* For metavariables, we give each metavar a (hopefully) unique integer
 * and then we store its corresponding info into the `metavar_table`
 * global map.
 *
 * Instead of this single ref-cell holding an IntMap, we could use many
 * ref-cells, and do away with the unique integer.  The reasons why we
 * do it this way are:
 * - for printing purposes, we want to have a printable unique identifier
 *   for each metavar.  OCaml does not offer any way to turn a ref-cell
 *   into some kind of printable identifier (can't get a hash of the address,
 *   no `eq` hash-tables, ...).
 * - Hashtbl.hash as well as `compare` happily follow ref-cell indirections:
 *   `compare (ref 0) (ref 0)` tells us they're equal!  So we need the unique
 *   integer in order to produce a hash anyway (and we'd have to write the hash
 *   function by hand, tho that might be a good idea anyway).
 *)

type metavar_info =
  | MVal of lexp            (* Exp to which the var is instantiated.  *)
  | MVar of scope_level     (* Outermost scope in which the var appears.  *)
            * ltype         (* Expected type.  *)
            (* The ctx_length keeps track of the length of the lctx in which the
             * metavar is meant to be defined.
             * We'd like to keep the lexp_context in which the type is to be
             * understood, but lexp_context is not yet defined here,
             * so we just keep the length of the lexp_context.  *)
            * ctx_length
type meta_subst = metavar_info U.IMap.t

let dummy_scope_level = 0

let builtin_size = ref 0

let dummy_sinfo = Sexp.dummy_sinfo

let metavar_table = ref (U.IMap.empty : meta_subst)
let metavar_lookup (id : meta_id) : metavar_info
  = try U.IMap.find id (!metavar_table)
    with Not_found
         -> Log.log_fatal ~section:"LEXP" "metavar lookup failure!"

(********************** Hash-consing **********************)

(** Hash-consing test **
* with: Hashtbl.hash / lexp'_hash
* median bucket length: 7 / 7
* biggest bucket length: 205 / 36
* found/new lexp entries: - / 2 *)

let lexp_lexp' (e, _h) = e
let lexp_hash (_e, h) = h

(* Hash `Lexp` using combine_hash (lxor) with hash of "sub-lexp". *)
let lexp'_hash (lp : lexp') =
  match lp with
   | Imm (l, v)
    -> U.combine_hash 1 (U.combine_hash (Hashtbl.hash l) (Hashtbl.hash v))
   | SortLevel l
    -> U.combine_hash 2
        (match l with
          | SLz -> Hashtbl.hash l
          | SLsucc lp -> lexp_hash lp
          | SLlub (lp1, lp2)
            -> U.combine_hash (lexp_hash lp1) (lexp_hash lp2))
   | Sort (l, s)
    -> U.combine_hash 3 (U.combine_hash (Hashtbl.hash l)
        (match s with
          | Stype lp -> lexp_hash lp
          | StypeOmega -> Hashtbl.hash s
          | StypeLevel -> Hashtbl.hash s))
   | Builtin (v, t)
    -> U.combine_hash 4 (U.combine_hash (Hashtbl.hash v) (lexp_hash t))
   | Var v -> U.combine_hash 5 (Hashtbl.hash v)
   | Let (l, ds, e)
     -> U.combine_hash 6 (U.combine_hash (Hashtbl.hash l)
          (U.combine_hash (U.combine_hashes
            (List.map (fun e -> let (n, lp, lt) = e in
              (U.combine_hash (Hashtbl.hash n)
                (U.combine_hash (lexp_hash lp) (lexp_hash lt))))
              ds))
            (lexp_hash e)))
   | Arrow (l, k, v, t1, t2)
     -> U.combine_hash 7 (U.combine_hash
          (U.combine_hash (Hashtbl.hash l) (Hashtbl.hash k))
          (U.combine_hash (Hashtbl.hash v)
            (U.combine_hash (lexp_hash t1) (lexp_hash t2))))
   | Lambda (k, v, t, e)
     -> U.combine_hash 8 (U.combine_hash
          (U.combine_hash (Hashtbl.hash k) (Hashtbl.hash v))
          (U.combine_hash (lexp_hash t) (lexp_hash e)))
   | Inductive (l, n, a, cs)
     -> U.combine_hash 9 (U.combine_hash
          (U.combine_hash (Hashtbl.hash l) (Hashtbl.hash n))
          (U.combine_hash (U.combine_hashes
            (List.map (fun e -> let (ak, n, lt) = e in
              (U.combine_hash (Hashtbl.hash ak)
                (U.combine_hash (Hashtbl.hash n) (lexp_hash lt))))
            a))
          (Hashtbl.hash cs)))
   | Cons (t, n) -> U.combine_hash 10 (U.combine_hash (lexp_hash t) (Hashtbl.hash n))
   | Case (l, e, rt, bs, d)
     -> U.combine_hash 11 (U.combine_hash
          (U.combine_hash (Hashtbl.hash l) (lexp_hash e))
          (U.combine_hash (lexp_hash rt) (U.combine_hash
            (Hashtbl.hash bs)
            (match d with
             | Some (n, lp) -> U.combine_hash (Hashtbl.hash n) (lexp_hash lp)
             | _ -> 0))))
   | Metavar (id, s, v)
     -> U.combine_hash 12 (U.combine_hash id
          (U.combine_hash (Hashtbl.hash s) (Hashtbl.hash v)))
   | Call (_, e, args)
      -> U.combine_hash 13 (U.combine_hash (lexp_hash e)
          (U.combine_hashes (List.map (fun e -> let (ak, lp) = e in
              (U.combine_hash (Hashtbl.hash ak) (lexp_hash lp)))
            args)))
   | Susp (lp, subst)
      -> U.combine_hash 14 (U.combine_hash (lexp_hash lp) (Hashtbl.hash subst))
   | Proj (l,lxp,lbl)
      -> U.combine_hash 15 (U.combine_hash (Hashtbl.hash l)
                             (U.combine_hash (Hashtbl.hash lxp)
                                (Hashtbl.hash lbl)))

(* Equality function for hash table
 * using physical equality for "sub-lexp" and compare for `subst`. *)
let hc_eq e1 e2 =
  try match (lexp_lexp' e1, lexp_lexp' e2) with
      | (Imm (_, v1), Imm (_, v2)) -> v1 = v2
      | (SortLevel SLz, SortLevel SLz) -> true
      | (SortLevel (SLsucc e1), SortLevel (SLsucc e2)) -> e1 == e2
      | (SortLevel (SLlub (e11, e21)), SortLevel (SLlub (e12, e22)))
        -> e11 == e12 && e21 == e22
      | (Sort (_, StypeOmega), Sort (_, StypeOmega)) -> true
      | (Sort (_, StypeLevel), Sort (_, StypeLevel)) -> true
      | (Sort (_, Stype e1), Sort (_, Stype e2)) -> e1 == e2
      | (Builtin ((_, name1), _), Builtin ((_, name2), _)) -> name1 = name2
      | (Var (_, i1), Var (_, i2)) -> i1 = i2
      | (Susp (e1, s1), Susp (e2, s2)) -> e1 == e2 && compare s1 s2 = 0
      | (Let (_, defs1, e1), Let (_, defs2, e2))
        -> e1 == e2 && List.for_all2
        (fun (_, e1, t1) (_, e2, t2) -> t1 == t2 && e1 == e2) defs1 defs2
      | (Arrow (_, ak1, _, t11, t21), Arrow (_, ak2, _, t12, t22))
        -> ak1 = ak2 && t11 == t12 && t21 == t22
      | (Lambda (ak1, _, t1, e1), Lambda (ak2, _, t2, e2))
        -> ak1 = ak2 && t1 == t2 && e1 == e2
      | (Call (_, e1, as1), Call (_, e2, as2))
        -> e1 == e2 && List.for_all2
        (fun (ak1, e1) (ak2, e2) -> ak1 = ak2 && e1 == e2) as1 as2
      | (Inductive (_, l1, as1, ctor1), Inductive (_, l2, as2, ctor2))
        -> l1 = l2 && List.for_all2
        (fun (ak1, _, e1) (ak2, _, e2) -> ak1 = ak2 && e1 == e2) as1 as2
          && SMap.equal (List.for_all2
            (fun (ak1, _, e1) (ak2, _, e2) -> ak1 = ak2 && e1 == e2)) ctor1 ctor2
      | (Cons (t1, (_, l1)), Cons (t2, (_, l2))) -> t1 == t2 && l1 = l2
      | (Case (_, e1, r1, ctor1, def1), Case (_, e2, r2, ctor2, def2))
        -> e1 == e2 && r1 == r2 && SMap.equal
          (fun (_, fields1, e1) (_, fields2, e2)
            -> e1 == e2 && List.for_all2
            (fun (ak1, _) (ak2, _) -> ak1 = ak2) fields1 fields2) ctor1 ctor2
          && (match (def1, def2) with
              | (Some (_, e1), Some (_, e2)) -> e1 == e2
              | _ -> def1 = def2)
      | (Metavar (i1, s1, _), Metavar (i2, s2, _))
        -> i1 = i2 && compare s1 s2 = 0
      | _ -> false
  with
  | Invalid_argument _ -> false (* Different lengths in List.for_all2. *)

module WHC = Weak.Make (struct type t = lexp
                          let equal x y = hc_eq x y
                          let hash = lexp_hash
                        end)

let hc_table : WHC.t = WHC.create 1000

let hc (l : lexp') : lexp =
    WHC.merge hc_table (l, lexp'_hash l)

let mkImm (l, v)               = hc (Imm (l, v))
let mkSortLevel l              = hc (SortLevel l)
let mkSort (l, s)              = hc (Sort (l, s))
let mkBuiltin (v, t)           = hc (Builtin (v, t))
let mkVar v                    = hc (Var v)
let mkProj (l,lxp,lbl)         = hc (Proj (l,lxp,lbl))
let mkLet (l, ds, e)           = hc (Let (l, ds, e))
let mkArrow (l, k, v, t1, t2)  = hc (Arrow (l, k, v, t1, t2))
let mkLambda (k, v, t, e)      = hc (Lambda (k, v, t, e))
let mkInductive (l, n, a, cs)  = hc (Inductive (l, n, a, cs))
let mkCons (t, n)              = hc (Cons (t, n))
let mkCase (l, e, rt, bs, d)   = hc (Case (l, e, rt, bs, d))
let mkMetavar (n, s, v)        = hc (Metavar (n, s, v))
let mkCall (l, f, es) =
  match lexp_lexp' f, es with
    | Call (l, f', es'), _ -> hc (Call (l, f', es' @ es))
    | _, [] -> f
    | _ -> hc (Call (l, f, es))

let impossible = mkImm (dummy_sinfo, Vundefined)

let lexp_head e =
  match lexp_lexp' e with
    | Imm       (_, _v) ->
       if e = impossible then "impossible" else "Imm"
    | Var       _ -> "Var"
    | Proj _ -> "Proj"
    | Let       _ -> "let"
    | Arrow     _ -> "Arrow"
    | Lambda    _ -> "lambda"
    | Call      _ -> "Call"
    | Cons      _ -> "datacons"
    | Case      _ -> "case"
    | Inductive _ -> "typecons"
    | Susp      _ -> "Susp"
    | Builtin   _ -> "Builtin"
    | Metavar   _ -> "Metavar"
    | Sort      _ -> "Sort"
    | SortLevel _ -> "SortLevel"

let mkSLlub' (e1, e2) =
  match (lexp_lexp' e1, lexp_lexp' e2) with
  (* FIXME: This first case should be handled by calling `mkSLlub` instead!  *)
  | (SortLevel SLz, SortLevel l) | (SortLevel l, SortLevel SLz) -> l
  | (SortLevel SLz, e) | (e, SortLevel SLz)
    -> Log.log_fatal ~section:"internal" "lub of SLz with %S" (lexp_head (hc e))
  | (SortLevel (SLsucc _), SortLevel (SLsucc _))
    -> Log.log_fatal ~section:"internal" "lub of two SLsucc"
  | ((SortLevel _ | Var _ | Metavar _ | Susp _),
     (SortLevel _ | Var _ | Metavar _ | Susp _))
    -> SLlub (e1, e2)
  | _
    -> Log.log_fatal
         ~section:"internal"
         "SLlub of non-level: %s ∪ %s"
         (lexp_head e1)
         (lexp_head e2)

let mkSLsucc e =
  match lexp_lexp' e with
    | SortLevel _ | Var _ | Metavar _ | Susp _
      -> SLsucc e
    | _ -> Log.log_fatal ~section:"internal" "SLsucc of non-level: %s"
            (lexp_head e)

(********************** Lexp tests ************************)

let pred_var l pred =
  match lexp_lexp' l with
    | Var v -> pred v
    | _ -> false

let is_var l = pred_var l (fun _ -> true)

let get_var l =
  match lexp_lexp' l with
    | Var v -> v
    | _ -> Log.log_fatal ~section:"internal" "Lexp is not Var "

let get_var_db_index v =
  let (_n, idx) = v in idx

let get_var_vname v =
  let (n, _idx) = v in n

let pred_inductive l pred =
  match lexp_lexp' l with
    | Inductive (l, n, a, cs) -> pred (l, n, a, cs)
    | _ -> false

let get_inductive l =
match lexp_lexp' l with
  | Inductive (l, n, a, cs) -> (l, n, a, cs)
  | _ -> Log.log_fatal ~section:"internal" "Lexp is not Inductive "

let get_inductive_ctor i =
  let (_l, _n, _a, cs) = i in cs

let is_inductive l = pred_inductive l (fun _ -> true)

(********* Helper functions to use the Subst operations  *********)
(* This basically "ties the knot" between Subst and Lexp.
 * Maybe it would be cleaner to just move subst.ml into lexp.ml
 * and be done with it.  *)

(* FIXME: We'd like to make this table "weak" to avoid memory leaks,
 * but Weak.Make doesn't cut it because we need to index with a pair
 * that is transient and hence immediately GC'd.  *)
let hcs_table : ((lexp * subst), lexp) Hashtbl.t = Hashtbl.create 1000

(* When computing the type of "load"ed modules
 * we end up building substitutions of the form
 *
 *     ((Susp e3 (((Susp e2 (e1 · id)) · e1 · id)
 *                · e1 · id))
 *      · ((Susp e2 (e1 · id)) · e1 · id)
 *      · e1 · id)
 *
 * with 2^n size (but lots of sharing).  So it's indispensible
 * to memoize the computation to avoid the exponential waste of time.
 *
 * This shows up because of the dependent type of `let`:
 *
 *    let z = e₃ in e                :   τ[e₃/z]
 *    let y = e₂ in let z = e₃ in e  :   (τ[e₃/z])[e₂/y] = τ[(e₃[e₂/y])/z,e₂/y]
 *    let x = e₁ in let y = e₂ in let z = e₃ in e
 *      :   (τ[(e₃[e₂/y])/z,e₂/y])[e₁/x]
 *        = τ[(e₃[(e₂[e₁/x])/y,e₁/x])/z,(e₂[e₁/x])/y,e₁/x]
 *      ...
 *)

let rec mkSusp e s =
  if S.identity_p s then e else
    (* We apply the substitution eagerly to some terms.
     * There's no deep technical reason for that:
     * it just seemed like a good idea to do it eagerly when it's easy.  *)
    match lexp_lexp' e with
    | Imm _ -> e
    | Builtin (sym, t) -> mkBuiltin (sym, mkSusp t s)
    | Susp (e, s') -> mkSusp_memo e (scompose s' s)
    | Var (l,v) -> slookup s l v
    | Metavar (vn, s', vd) -> mkMetavar (vn, scompose s' s, vd)
    | _ -> hc (Susp (e, s))
and mkSusp_memo e s
  = if Hashtbl.mem hcs_table (e, s)
    then Hashtbl.find hcs_table (e, s)
    else let res = mkSusp e s in
         Hashtbl.add hcs_table (e, s) res;
         res
and scompose s1 s2 = S.compose mkSusp_memo s1 s2
and slookup s l v = S.lookup (fun l i -> mkVar (l, i))
                             (fun e o -> mkSusp e (S.shift o))
                             s l v
let ssink = S.sink (fun l i -> mkVar (l, i))

(* Apply a "dummy" substitution which replace #0 with #0
 * in order to account for changes to a variable's name.
 * This should probably be made into a no-op, but only after we get rid
 * of the check in DB.lookup that a `Var` has the same name as the
 * one stored in the lctx!
 * Using DeBruijn *should* make α-renaming unnecessary
 * so this is a real PITA!  :-(  *)
let srename name le = mkSusp le (S.cons (mkVar (name, 0)) (S.shift 1))

(* Shift by a negative amount!  *)
let rec sunshift n =
  if n = 0 then S.identity
  else (assert (n >= 0);
        S.cons impossible (sunshift (n - 1)))

let _ = assert (S.identity_p (scompose (S.shift 5) (sunshift 5)))

(* The quick test below seemed to indicate that about 50% of the "sink"s
 * are applied on top of another "sink" and hence could be combined into
 * a single "Lift^n" constructor.  Doesn't seem high enough to justify
 * the complexity of adding a `Lift` to `subst`.
 *)
(* let sink_count_total = ref 0
 * let sink_count_optimizable = ref 0
 *
 * let ssink l s =
 *   sink_count_total := 1 + !sink_count_total;
 *   (match s with
 *    | S.Cons (Var (_, 0), (S.Identity o | S.Cons (_, _, o)), 0) when o > 0
 *      -> sink_count_optimizable := 1 + !sink_count_optimizable
 *    | _ -> ());
 *   if ((!sink_count_total) mod 10000) = 0 then
 *     (print_string ("Optimizable = "
 *                    ^ (string_of_int ((100 * !sink_count_optimizable)
 *                                      / !sink_count_total))
 *                    ^ "%\n");
 *      if !sink_count_total > 100000 then
 *        (sink_count_total := !sink_count_total / 2;
 *         sink_count_optimizable := !sink_count_optimizable / 2));
 *   S.sink (fun l i -> mkVar (l, i)) l s *)


let rec lexp_sinfo s =
  match lexp_lexp' s with
  | Sort (l,_) -> l
  | SortLevel (SLsucc s) -> lexp_sinfo s
  | SortLevel (SLlub (s, _)) -> lexp_sinfo s
  | SortLevel SLz -> dummy_sinfo
  | Imm (l,_) -> l
  | Var ((l,_),_) -> l
  | Builtin ((l, _), _) -> Symbol (l, "")
  | Let (l,_,_) -> l
  | Arrow (l,_,_,_,_) -> l
  | Lambda (_,(l,_),_,_) -> l
  | Call (_,f,_) -> lexp_sinfo f
  | Inductive (l,_,_,_) -> l
  | Cons (_,(l,_)) -> Symbol (l, "")
  | Case (l,_,_,_,_) -> l
  | Susp (s, _) -> lexp_sinfo s
  (* | Susp (_, e) -> lexp_location e *)
  | Metavar (_,_,(l,_)) -> l
  | Proj (l,_,_) -> l

let location (e : lexp) : Source.Location.t =
  sexp_location (lexp_sinfo e)

(********* Normalizing a term *********)

let vdummy = (dummy_sinfo, None)
let sname (sinfo, n : vname) : symbol =
  (sexp_location sinfo, U.maybename n)

let rec push_susp e s =            (* Push a suspension one level down.  *)
  match lexp_lexp' e with
  | Imm _ -> e
  | SortLevel (SLz) -> e
  | SortLevel (SLsucc e'') -> mkSortLevel (mkSLsucc (mkSusp e'' s))
  | SortLevel (SLlub (e1, e2))
    -> mkSortLevel (mkSLlub' (push_susp e1 s, push_susp e2 s))
  | Sort (l, Stype e) -> mkSort (l, Stype (mkSusp e s))
  | Sort (_, _) -> e
  | Builtin (sym, t) -> mkBuiltin (sym, mkSusp t s)
  | Let (l, defs, e)
    -> let s' = L.fold_left (fun s (v, _, _) -> ssink v s) s defs in
      let rec loop s defs = match defs with
        | [] -> []
        | (v, def, ty) :: defs
          -> (v, mkSusp def s', mkSusp ty s) :: loop (ssink v s) defs in
      mkLet (l, loop s defs, mkSusp e s')
  | Arrow (l, ak, v, t1, t2)
    -> mkArrow (l, ak, v, mkSusp t1 s, mkSusp t2 (ssink v s))
  | Lambda (ak, v, t, e) -> mkLambda (ak, v, mkSusp t s, mkSusp e (ssink v s))
  | Call (l, f, args) -> mkCall (l,mkSusp f s,
                             L.map (fun (ak, arg) -> (ak, mkSusp arg s)) args)
  | Inductive (l, label, args, cases)
    -> let (s, nargs) = L.fold_left (fun (s, nargs) (ak, v, t)
                                    -> (ssink v s, (ak, v, mkSusp t s) :: nargs))
                                   (s, []) args in
      let nargs = List.rev nargs in
      let ncases = SMap.map (fun args
                             -> let (_, ncase)
                                 = L.fold_left (fun (s, nargs) (ak, v, t)
                                                -> (ssink v s,
                                                   (ak, v, mkSusp t s)
                                                   :: nargs))
                                               (s, []) args in
                               L.rev ncase)
                            cases in
      mkInductive (l, label, nargs, ncases)
  | Cons (it, name) -> mkCons (mkSusp it s, name)
  | Case (l, e, ret, cases, default)
    -> mkCase (l, mkSusp e s, mkSusp ret s,
              SMap.map (fun (l, cargs, e)
                        -> let s' = L.fold_left
                                     (fun s (_,ov) -> ssink ov s)
                                     s cargs in
                          (l, cargs, mkSusp e (ssink (l, None) s')))
                       cases,
              match default with
              | None -> default
              | Some (v,e) -> Some (v, mkSusp e (ssink (l, None) (ssink v s))))
  (* Susp should never appear around Var/Susp/Metavar because mkSusp
   * pushes the subst into them eagerly.  IOW if there's a Susp(Var..)
   * or Susp(Metavar..) it's because some chunk of code should use mkSusp
   * rather than Susp.
   * But we still have to handle them here, since push_susp is called
   * in many other cases than just when we bump into a Susp.  *)
  | Susp (e,s') -> push_susp e (scompose s' s)
  | (Var _ | Metavar _) -> nosusp (mkSusp e s)
  | Proj (l,lxp,lbl) -> mkProj (l, mkSusp lxp s, lbl)

and nosusp e =                  (* Return `e` with no outermost `Susp`.  *)
  match lexp_lexp' e with
    | Susp(e, s) -> push_susp e s
    | _ -> e


(* Get rid of `Susp`ensions and instantiated `Metavar`s.  *)
let clean e =
  let rec clean s e =
  match lexp_lexp' e with
    | Imm _ -> e
    | SortLevel (SLz) -> e
    | SortLevel (SLsucc e) -> mkSortLevel (mkSLsucc (clean s e))
    | SortLevel (SLlub (e1, e2))
      (* FIXME: The new SLlub could have `succ` on both sides!  *)
      -> mkSortLevel (mkSLlub' (clean s e1, clean s e2))
    | Sort (l, Stype e) -> mkSort (l, Stype (clean s e))
    | Sort (_, _) -> e
    | Builtin (sym, t) -> mkBuiltin (sym, clean s t)
    | Let (l, defs, e)
      -> let s' = L.fold_left (fun s (v, _, _) -> ssink v s) s defs in
        let (_,ndefs) = L.fold_left (fun (s,ndefs) (v, def, ty)
                                     -> (ssink v s,
                                        (v, clean s' def, clean s ty) :: ndefs))
                                  (s, []) defs in
        mkLet (l, ndefs, clean s' e)
    | Arrow (l, ak, v, t1, t2)
      -> mkArrow (l, ak, v, clean s t1, clean (ssink v s) t2)
    | Lambda (ak, v, t, e) -> mkLambda (ak, v, clean s t, clean (ssink v s) e)
    | Call (l, f, args) -> mkCall (l, clean s f,
                               L.map (fun (ak, arg) -> (ak, clean s arg)) args)
    | Inductive (l, label, args, cases)
      -> let (s, nargs) = L.fold_left (fun (s, nargs) (ak, v, t)
                                    -> (ssink v s, (ak, v, clean s t) :: nargs))
                                   (s, []) args in
      let nargs = List.rev nargs in
      let ncases = SMap.map (fun args
                             -> let (_, ncase)
                                 = L.fold_left (fun (s, nargs) (ak, v, t)
                                                -> (ssink v s,
                                                   (ak, v, clean s t)
                                                   :: nargs))
                                               (s, []) args in
                               L.rev ncase)
                            cases in
      mkInductive (l, label, nargs, ncases)
    | Cons (it, name) -> mkCons (clean s it, name)
    | Case (l, e, ret, cases, default)
      -> mkCase (l, clean s e, clean s ret,
                SMap.map (fun (l, cargs, e)
                          -> let s' = L.fold_left
                                       (fun s (_,ov) -> ssink ov s)
                                       s cargs in
                             let s'' = ssink (l, None) s' in
                             (l, cargs, clean s'' e))
                         cases,
                match default with
                | None -> default
                | Some (v,e) -> Some (v, clean (ssink (l, None) (ssink v s)) e))
    | Susp (e, s') -> clean (scompose s' s) e
    | Var _ -> if S.identity_p s then e
              else clean S.identity (mkSusp e s)
    | Proj (l, lxp, lbl) -> mkProj (l, clean s lxp, lbl)
    | Metavar (idx, s', name)
      -> let s = scompose s' s in
        match metavar_lookup idx with
        | MVal e -> clean s e
        | _ -> mkMetavar (idx, s, name)
  in clean S.identity e

(** Syntactic equality (i.e. without β).  *******)

let rec eq e1 e2 =
  try e1 == e2 ||
    match (lexp_lexp' e1, lexp_lexp' e2) with
    | (Imm (_, v1), Imm (_, v2)) -> v1 = v2
    | (SortLevel SLz, SortLevel SLz) -> true
    | (SortLevel (SLsucc e1), SortLevel (SLsucc e2)) -> eq e1 e2
    | (SortLevel (SLlub (e11, e21)), SortLevel (SLlub (e12, e22)))
      -> eq e11 e12 && eq e21 e22
    | (Sort (_, StypeOmega), Sort (_, StypeOmega)) -> true
    | (Sort (_, StypeLevel), Sort (_, StypeLevel)) -> true
    | (Sort (_, Stype e1), Sort (_, Stype e2)) -> eq e1 e2
    | (Builtin ((_, name1), _), Builtin ((_, name2), _)) -> name1 = name2
    | (Var (_, i1), Var (_, i2)) -> i1 = i2
    | (Susp (e1, s1), _) -> eq (push_susp e1 s1) e2
    | (_, Susp (e2, s2)) -> eq e1 (push_susp e2 s2)
    | (Let (_, defs1, e1), Let (_, defs2, e2))
      -> eq e1 e2 && List.for_all2
      (fun (_, e1, t1) (_, e2, t2) -> eq t1 t2 && eq e1 e2) defs1 defs2
    | (Arrow (_, ak1, _, t11, t21), Arrow (_, ak2, _, t12, t22))
      -> ak1 = ak2 && eq t11 t12 && eq t21 t22
    | (Lambda (ak1, _, t1, e1), Lambda (ak2, _, t2, e2))
      -> ak1 = ak2 && eq t1 t2 && eq e1 e2
    | (Call (_, e1, as1), Call (_, e2, as2))
      -> eq e1 e2 && List.for_all2
      (fun (ak1, e1) (ak2, e2) -> ak1 = ak2 && eq e1 e2) as1 as2
    | (Inductive (_, l1, as1, ctor1), Inductive (_, l2, as2, ctor2))
      -> l1 = l2 && List.for_all2
      (fun (ak1, _, e1) (ak2, _, e2) -> ak1 = ak2 && eq e1 e2) as1 as2
        && SMap.equal (List.for_all2
          (fun (ak1, _, e1) (ak2, _, e2) -> ak1 = ak2 && eq e1 e2)) ctor1 ctor2
    | (Cons (t1, (_, l1)), Cons (t2, (_, l2))) -> eq t1 t2 && l1 = l2
    | (Case (_, e1, r1, ctor1, def1), Case (_, e2, r2, ctor2, def2))
      -> eq e1 e2 && eq r1 r2 && SMap.equal
       (fun (_, fields1, e1) (_, fields2, e2) -> eq e1 e2 && List.for_all2
        (fun (ak1, _) (ak2, _) -> ak1 = ak2) fields1 fields2) ctor1 ctor2
        && (match (def1, def2) with
              | (Some (_, e1), Some (_, e2)) -> eq e1 e2
              | _ -> def1 = def2)
    | (Metavar (i1, s1, _), Metavar (i2, s2, _))
      -> if i1 == i2 then subst_eq s1 s2 else
           (match (metavar_lookup i1, metavar_lookup i2) with
            | (MVal l, _) -> eq (push_susp l s1) e2
            | (_, MVal l) -> eq e1 (push_susp l s2)
            | _ -> false)
    | (Metavar (i1, s1, _), _)
      -> (match metavar_lookup i1 with
          | MVal l ->  eq (push_susp l s1) e2
          | _ -> false)
    | (_, Metavar (i2, s2, _))
      -> (match metavar_lookup i2 with
          | MVal l ->  eq e1 (push_susp l s2)
          | _ -> false)
    | _ -> false
  with
  | Invalid_argument _ -> false (* Different lengths in List.for_all2. *)

and subst_eq s1 s2 =
  s1 == s2 ||
    match (s1, s2) with
    | (S.Identity o1, S.Identity o2) -> o1 = o2
    | (S.Cons (e1, s1, o1), S.Cons (e2, s2, o2))
      -> if o1 = o2 then
          eq e1 e2 && subst_eq s1 s2
        else if o1 > o2 then
          let o = o1 - o2 in
          eq (mkSusp e1 (S.shift o)) e2
          && subst_eq (S.mkShift s1 o) s2
        else
          let o = o2 - o1 in
          eq e1 (mkSusp e2 (S.shift o))
          && subst_eq s1 (S.mkShift s2 o)
    | ((S.Identity n, S.Cons (e, s, o))
       | (S.Cons (e, s, o), S.Identity n))
      -> (* ↑n ≃ Var n · ↑(n+1), so unfold Identity when needed.  *)
       match lexp_lexp' e with
       | Var (_, n') when n' == n + o
         -> subst_eq (S.Identity (n + 1)) (S.mkShift s o)
    | _ -> false

(* Print a clean version of an lexp. Should be preferred to `pp_print_lexp`
   since the latter may use an exponential amount of memory for certain
   expressions. *)
let pp_print_clean_lexp (f : Format.formatter) l : unit =
  Fmt.pp_print_lexp f (clean l)

let string_of_clean_lexp l =
  Fmt.string_of_lexp (clean l)
