(* Copyright (C) 2022  Free Software Foundation, Inc.
 *
 * Author: Jean-Alexandre Barszcz <jean-alexandre.barszcz@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>. *)

module DB = Debruijn
module L = Lexp
module OL = Opslexp
module S = Subst
module U = Util
module Unif = Unification

open Ir

(** If true, log all the matching instances when resolving. *)
let debug_list_all_candidates = ref false

(** Optionnally limit resolution recursion depth. *)
let recursion_limit = ref (Some 100)

(** Some variables may be skipped during instance resolution if
   unification returns residual constraints (i.e. at this point during
   inference, we do not have enough info to confirm or rule out a
   match). If set to `some lvl`, log a message at level `lvl` when
   that happens. *)
let log_skipped_uncertain_matches = ref (Some Log.Warning)

let metavar_resolution_contexts =
  ref (U.IMap.empty : (elab_context * sinfo) U.IMap.t)

let lookup_metavar_resolution_ctxt (id : meta_id)
    : (elab_context * sinfo) option
  = U.IMap.find_opt id (!metavar_resolution_contexts)

let save_metavar_resolution_ctxt (id : meta_id) ctx loc : unit
  = (* First, check that there is only one possible resolution context
       for any metavar. *)
  (match U.IMap.find_opt id !metavar_resolution_contexts with
   | Some (other_ctx, _) -> assert ((DB.get_size ctx) = (DB.get_size other_ctx))
   | _ -> ());
  metavar_resolution_contexts :=
    U.IMap.add id (ctx, loc) !metavar_resolution_contexts

(** Check if a type is in the set of type class types. *)
let in_typeclass_set (ectx : elab_context) (t : ltype) : bool =
  (* We could use unification to be more general, but conversion
     on the heads of the calls seems simpler, faster, and good
     enough. Also, we would need to undo instanciations after
     unification. *)
  let (tcs, _, _) = DB.ectx_to_tcctx ectx in
  let cl = DB.get_size ectx in
  (* FIXME: We use go through a list element by element to check for
     conversion, but syntactic equality of the heads (already in WHNF)
     should be enough, probably? This means that we could use a real
     set instead of a list! *)
  List.exists (fun (t', cl') ->
      let i = cl - cl' in
      let t' = L.mkSusp t' (S.shift i) in
      (* L.eq (OL.lexp_whnf t (DB.ectx_to_lctx ectx)) t' *)
      OL.conv_p (DB.ectx_to_lctx ectx) t t'
    ) tcs

(** Get the head of a call. For instance, the call of `(Monoid α)` is
   `Monoid`. *)
let get_head (lctx : lexp_context) (t : ltype) : ltype =
  let whnft = OL.lexp_whnf t lctx in
  match L.lexp_lexp' whnft with
  | Call (_, head, _) -> head
  | _ -> whnft

(** A type is a type class if its head is in the set of type class
   types of the elaboration context. *)
let is_typeclass (ctx : elab_context) (t : ltype) =
  let head = get_head (DB.ectx_to_lctx ctx) t in
  in_typeclass_set ctx head

(** Add the type at the head of a call to the set of type class types
   in the elaboration context. *)
let add_typeclass (ctx : elab_context) (t : ltype) : elab_context =
  let head = get_head (DB.ectx_to_lctx ctx) t in
  DB.ectx_add_typeclass ctx head

(** The result type for matching.*)
type matching
  = Impossible (* Unification (matching) failed with impossible constraints *)
  | Possible (* Unification returned residual constraints: possible
                match under the right metavariable associations. *)
  | Match (* Perfect match: no constraints. *)

let try_match t1 t2 lctx sl =
  let has_impossible constraints =
    List.exists (function | (Unif.CKimpossible,_,_,_) -> true
                          | _ -> false)
      constraints in
  match Unif.check_unifiable ~matching:sl t1 t2 lctx with
  | [] -> Match
  | constraints when has_impossible constraints -> Impossible
  | _ -> Possible

let search_instance
      (instantiate_implicit)
      (ctx : elab_context)
      (sinfo : sinfo)
      (t : ltype)
    : lexp option =

  Log.log_debug
    ~loc:(sexp_location sinfo)
    "@[<v>Resolving type:@,  @[<hov 2>%a@]@]"
    L.pp_print_clean_lexp t;
  let lctx = DB.ectx_to_lctx ctx in
  let (_, _, insts) = DB.ectx_to_tcctx ctx in

  (* Increment the scope level and save it, as it is used to select
     the instantiatable metavariables during matching. *)
  let ctx = DB.ectx_new_scope ctx in
  let sl = (DB.ectx_to_scope_level ctx) in

  (* For any env_elem in the context, check if its (implicitly
     applied) type is matching the type that we are looking for, and
     return the applied expression (ex: `(nil (t := Int))` for a
     `(List Int)`). The expression is tupled with the index and
     env_elem, for convenience. *)
  let env_elem_match (i, elem : db_index * env_elem)
      : (int * env_elem * lexp) option =
    let ((_, namopt), _, t') = elem in
    let var = L.mkVar ((sinfo, namopt), i) in
    let t' = L.mkSusp t' (S.shift (i + 1)) in
    let (e, t') = instantiate_implicit var t' ctx in
    Log.log_debug ~loc:(sexp_location sinfo)
      ("@[<v>Considering potential instance:"
       ^^ "@,  @[<hov 2>%a@]"
       ^^ "@,of type:"
       ^^ "@,  @[<hov 2>%a@]"
       ^^ "@,while resolving for:"
       ^^ "@,  @[<hov 2>%a@]@]")
      L.pp_print_clean_lexp var
      L.pp_print_clean_lexp t'
      L.pp_print_clean_lexp t;
    (* All candidates should have a type that is a typeclass. *)
    if not (is_typeclass ctx t') then None else
      (* `try_match` uses the matching mode of unification where only
         the instance's implicit argument metavariables can be
         instantiated (those generated by the call to
         `instantiate_implicit` above). This means that there is no
         need to undo any instantiations in case of failure, as the
         new metavars will simply be forgotten. *)
      match try_match t t' lctx sl with
      | Impossible -> None
      | Possible ->
         (match !log_skipped_uncertain_matches with
          | Some level ->
             Log.log_msg ignore level ~loc:(sexp_location sinfo)
               ("@[<v>Skipping potential instance:"
                ^^ "@,  @[<hov 2>%a@]"
                ^^ "@,of type:@,  @[<hov 2>%a@]"
                ^^ "@,while resolving for:@,  @[<hov 2>%a@]@]")
               L.pp_print_clean_lexp var
               L.pp_print_clean_lexp t'
               L.pp_print_clean_lexp t
          | None -> ());
         None
      | Match -> Some (i, elem, e) in

  let candidates = DB.env_lookup_set insts lctx
                   |> Seq.filter_map env_elem_match in

  if !debug_list_all_candidates then
    Log.log_debug "@[<v>Candidates for instance of type:@,  @[<hov 2>%a@]@,are:"
      L.pp_print_clean_lexp t
      ~print_action:(fun () ->
        Seq.iter
          (fun (i, ((_, so),_,t'), _) ->
            Format.printf "@,%-4i %-10s @[<hov 2>%a@]"
              i (* De Bruijn index *)
              (match so with | Some s -> s | None -> "<none>") (* Var name *)
              L.pp_print_clean_lexp t' (* Variable type *))
          candidates;
        Format.printf "@]@.");

  let inst = match candidates () with
    | Seq.Nil -> None
    | Seq.Cons (c, _) -> Some c in

  match inst with
  | None  -> None
  | Some (i, (vname, _, t'), e) ->
     let t' = L.mkSusp t' (S.shift (i + 1)) in
     Log.log_debug ~loc:(sexp_location sinfo)
      ("@[<v>Found instance for:"
       ^^ "@,  @[<hov 2>%a@]"
       ^^ "@,at index %i:"
       ^^ "@,  @[<hov 2>%a@]"
       ^^ "@,of type:"
       ^^ "@,  @[<hov 2>%a@]@]")
      L.pp_print_clean_lexp t
      i
      L.pp_print_clean_lexp (L.mkVar (vname, i))
      L.pp_print_clean_lexp t';
     Some e

let resolve_instances instantiate_implicit e =
  let rec resolve_instances e limit =
    let (_, (fv_map, _)) = OL.fv e in
    let changed =
      (U.IMap.fold (fun i (_sl, t, _cl, _vn) changed ->
           (match lookup_metavar_resolution_ctxt i with
            | Some (ctx, sinfo) ->
               (* Start by the instance metavars in the type: no need
                  to decrement the recursion limit here. *)
               resolve_instances t limit;
               let uninstantiated =
                 match L.metavar_lookup i with
                 |  MVar _ -> true
                 | _      -> false in
               if uninstantiated && is_typeclass ctx t
               then
                 (match search_instance instantiate_implicit ctx sinfo t with
                  | Some e -> Unif.associate i e; true

                  (* The metavar will be generalized, unified, or will remain
                     and cause an error. *)
                  | None
                    -> Log.log_info
                         ~loc:(sexp_location sinfo)
                         "@[<v>No instance found for type:@,  @[<hov 2>%a@]@]"
                         L.pp_print_clean_lexp t;
                       false)
               else false
            | None -> false
           ) || changed) fv_map false) in
    if changed then
      match limit with
      | Some l when l > 0 ->
         resolve_instances e (Some (l - 1))
      | None -> resolve_instances e None
      | _ ->
         Log.log_error ~loc:(Lexp.location e)
           "@[<v>Instance resolution recursion limit reached in expression:@,  @[<hov 2>%a@]@]"
           L.pp_print_clean_lexp e in
  resolve_instances e !recursion_limit
