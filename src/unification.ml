(* unification.ml --- Unification of Lexp terms

Copyright (C) 2016-2023  Free Software Foundation, Inc.

Author: Vincent Bonnevalle <tiv.crb@gmail.com>

This file is part of Typer.

Typer is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

Typer is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.  *)

open Ir
open Lexp
open Util
module OL = Opslexp
module DB = Debruijn

(** Provide unify function for unifying two Lambda-Expression *)

let log_info ?loc fmt = Log.log_info ~section:"UNIF" ?loc fmt

(* :-( *)
let global_last_metavar = ref (-1) (*The first metavar is 0*)

let create_metavar_1 (sl : scope_level) (t : ltype) (clen : int)
  = let idx = !global_last_metavar + 1 in
    global_last_metavar := idx;
    metavar_table := U.IMap.add idx (MVar (sl, t, clen))
                               (!metavar_table);
    idx

let create_metavar (ctx : lexp_context) (sl : scope_level) (t : ltype)
  = create_metavar_1 sl t (Myers.length ctx)

let dloc = DB.dloc
let dsinfo = DB.dsinfo

(* For every definition, keep track of all residues during elaboration. There
   is a chance they can be unified later (when some metavariable becomes
   instanciated, for example). *)
let current_residues = ref ([] : (lexp_context * lexp * lexp) list)

let add_residue (ctx : lexp_context) (e1 : lexp) (e2 : lexp) =
  current_residues := (ctx, e1, e2) :: !current_residues

let signal_residue (loc : location) (e1 : lexp) (e2 : lexp) =
  Log.log_error
    ~section:"UNIF"
    ~loc:loc
    ("@[<v>Remaining residue. Can't unify:"
    ^^ "@,  @[<hov 2>%a@]"
    ^^ "@,with:"
    ^^ "@,  @[<hov 2>%a@]@]")
    pp_print_clean_lexp e1
    pp_print_clean_lexp e2

let raise_errors_on_out_of_scope_residues
  (location : location)
  (scope_level : scope_level) =
  (* For each residue that caused an error to be logged, remove it from
   * `current_residues`. *)
  let new_residue_list = List.filter
    (fun (_ctx, e1, e2) ->
      let (_, e1_free_mv) = OL.fv e1 in
      let (_, e2_free_mv) = OL.fv e2 in
      let (free_mv_map, _) = OL.mv_set_union e1_free_mv e2_free_mv in
      (* Only raise an error if no metavariables appear outside of
        * `scope_level`. *)
      let to_flag = IMap.for_all
            (fun _ (mv_scope_level, _, _, _) ->
                mv_scope_level >= scope_level)
            free_mv_map in
      if to_flag then
        signal_residue location e1 e2;
      not to_flag)
    !current_residues in
  current_residues := new_residue_list

let raise_error_on_all_residues () : unit =
  List.iter
    (fun (_ctx, e1, e2) ->
      signal_residue dummy_location e1 e2)
    !current_residues

let clean_residues () : unit =
  current_residues := []

(* For convenience *)
type constraint_kind =
  | CKimpossible                (* Unification is simply impossible.  *)
  | CKresidual                  (* We failed to find a unifier.  *)
(* FIXME: Each constraint should additionally come with a description of how
   it relates to its "top-level" or some other info which might let us
   fix the problem (e.g. by introducing coercions).  *)
type constraints  = (constraint_kind * lexp_context * lexp * lexp) list

type return_type = constraints

let occurs_in (id: meta_id) (e : lexp) : bool = match metavar_lookup id with
  | MVal _ -> Log.internal_error
               "Checking occurrence of an instantiated metavar!!"
  | MVar (sl, _, _)
    -> let rec oi e =
      match lexp_lexp' e with
        | Imm _ -> false
        | SortLevel SLz -> false
        | SortLevel (SLsucc e) -> oi e
        | SortLevel (SLlub (e1, e2)) -> oi e1 || oi e2
        | Sort (_, Stype e) -> oi e
        | Sort (_, (StypeOmega | StypeLevel)) -> false
        | Builtin (_, t) -> oi t
        | Var (_, _i) -> false
        | Proj (_,lxp, _) -> oi lxp
        | Susp (_e, _s) -> Log.internal_error "`e` should be \"clean\" here!?"
                      (* ; oi (push_susp e s) *)
        | Let (_, defs, e)
          -> List.fold_left (fun o (_, e, t) -> o || oi e || oi t) (oi e) defs
        | Arrow (_, _, _, t1, t2) -> oi t1 || oi t2
        | Lambda (_, _, t, e) -> oi t || oi e
        | Call (_, f, args)
          -> List.fold_left (fun o (_, arg) -> o || oi arg) (oi f) args
        | Inductive (_, _, args, cases)
          -> SMap.fold
              (fun _ fields o
               -> List.fold_left (fun o (_, _, t) -> o || oi t)
                   o fields)
              cases
              (List.fold_left (fun o (_, _, t) -> o || oi t) false args)
        | Cons (t, _) -> oi t
        | Case (_, e, t, cases, def)
          -> let o = oi e || oi t in
            let o = match def with | None -> o | Some (_, e) -> o || oi e in
            SMap.fold (fun _ (_, _, e) o -> o || oi e)
              cases o
        | Metavar (id', _, _name) when id' = id -> true
        | Metavar (id', _, _)
          -> (match metavar_lookup id' with
             | MVal _e -> Log.internal_error "`e` should be \"clean\" here!?"
                      (* ; oi e *)
             | MVar (sl', t, cl)
               -> if sl' > sl then
                   (* id' will now appear in id's scope, so if id's scope is
                    * higher than id', we need to make sure id' won't be
                    * generalized in sl' but only in sl!
                    * This is the trick mentioned in
                    * http://okmij.org/ftp/ML/generalization.html
                    * to avoid computing `fv lctx` when generalizing!  *)
                   metavar_table := U.IMap.add id' (MVar (sl, t, cl))
                                     (!metavar_table);
                 false) in
      let old_mvt = !metavar_table in
      let old_residue_list = !current_residues in
      if oi e then
        (* Undo the side-effects since we're not going to instantiate the
           var after all!  *)
        (metavar_table := old_mvt;
         current_residues := old_residue_list;
         true)
      else false

(* When unifying a metavar with itself, if the two metavars don't
 * have the same substitution applied, then we should take the "common subset"
 * of those two substitutions: e.g.
 *
 *     ?a[0 => x, 1 => y, 2 => z, ...]   =?=   ?a[0 => x, 1 => b, 2 => z, ...]
 *
 * then we know `?a` can't use variable #1 since if it did then the two
 * substitutions would result in different terms.
 * We can do that by instantiating `?a` with a new metavar:
 *
 *     ?a = ?b[0 => 0, 1 => 2, ...]
 * aka
 *     ?a = ?b[0 => 0 · ↑1]
 *)
let common_subset ctx s1 s2 =
  let rec loop s1 s2 o1 o2 o =
    match (s1, s2) with
    | (S.Cons (le1, s1', o1'), S.Cons (le2, s2', o2'))
      -> let o1 = o1 + o1' in
        let o2 = o2 + o2' in
        (* FIXME: We should check if le1 and le2 are *unifiable* instead!  *)
        if not (le1 = impossible || le1 = impossible)
           && OL.conv_p ctx (mkSusp le1 (S.shift o1)) (mkSusp le2 (S.shift o2))
        then match loop s1' s2' o1 o2 1 with
             | S.Identity 1 -> S.Identity o (* Optimization!  *)
             | s' -> S.Cons (mkVar ((lexp_sinfo le1, None), 0),
                            s', o)
        else loop s1' s2' o1 o2 (o + 1)
    (* If one of them reached `Identity`, unroll it, knowing that
     *
     *     Identity 0 = #0 · #1 · #2 ... = #0 · (Identity 1)
     *)
    | (S.Cons _, S.Identity o2')
      -> loop s1 (S.Cons (mkVar ((dsinfo, None), 0),
                         S.Identity 1, o2'))
          o1 o2 o
    | (S.Identity o1', S.Cons _)
      -> loop (S.Cons (mkVar ((dsinfo, None), 0),
                      S.Identity 1, o1'))
          s2 o1 o2 o
    | (S.Identity o1', S.Identity o2')
      -> assert (o1 + o1' = o2 + o2');
        S.Identity o
  in loop s1 s2 0 0 0

(* Return the number of vars difference between input and output context.  *
 * Could be returned directly by `common_subset`, but it's pretty easy to
 * compute it here instead.  *)
let rec s_offset s = match s with
  | S.Identity o -> o
  | S.Cons (_, s', o) -> o - 1 + s_offset s'

(* Return true if metavariables of this scope level can be
   instantiated: in the matching mode of unification, only some
   metavariables can be instantiated (those of scope level at least as
   high as a given limit). *)
let matching_instantiation_check
      (matching : scope_level option)
      (sl : scope_level)
  = match matching with
  | Some limit -> limit <= sl
  | None -> true

(************************** Top level unify **********************************)

(** Dispatch to the right unifier.

     Unification has the side-effect of associating the metavariables
   in a way that makes the Lexp arguments convertible, if possible. In
   case of success, it returns the empty list. Otherwise, it returns
   the cause of failure as a list of constraints of the following
   kinds:

     1. `CKimpossible` constraints: constraints that cannot be
   satisfied, even if other metavars are associated, redexes are
   reduced, and subtitutions are done. For example, an arrow could
   never be unified with a lambda.

     2. `CKresidual` constraints: pairs of subterms that cannot be
   unified in the current context, but might eventually be sucessfully
   unifiable. For instance, a pair of different variables cannot be
   unified, unless another reduction eventually substitutes one for
   the other. As another example, a metavariable could be used in the
   head position of a call, blocking its reduction, but a later
   instanciation might turn this metavariable into a lambda, thus
   yielding a redex, possibly reducing the call to a term unifiable
   with the other argument.

     Additionnaly, the `unify` function provides a `matching` mode,
   where the metavariables of scope level lower than a given threshold
   are treated as free variables.
 *)
let rec unify ?(matching : scope_level option)
              (e1: lexp) (e2: lexp)
              (ctx : lexp_context)
        : return_type =
  let constraints = unify' e1 e2 ctx OL.set_empty matching in
  List.filter (fun (kind, _, _, _) -> kind <> CKresidual) constraints

(* The association of a metavariable must change the scope levels of
   the metavariables that are introduced to a wider scope. Here we
   assume that this has already been done. This happens in `occurs_in`
   during unification. *)
and associate (id: meta_id) (lxp: lexp) : unit =
  (* FIXME: Check that the types are convertible? *)
  metavar_table := U.IMap.add id (MVal lxp) (!metavar_table);
  (* Go through residues, see if, having instanciated this metavar,
     we can unify any residue. *)
  let old_residue_list = !current_residues in
  (* Reason for purging the list: any lexp pair that is still non-unifiable
     will be re-added to the list by `unify`. *)
  clean_residues ();
  let new_constraints = List.concat_map
    (fun (ctx, lxp1, lxp2) -> unify lxp1 lxp2 ctx)
    old_residue_list in
  (* In case we find out some expressions are impossible to unify,
     raise an error. *)
  List.iter
    (fun (_, _, lxp1, lxp2) ->
        Log.log_error
          ~section:"UNIF"
          ~loc:(location lxp1)
          ("@[<v>Unresolvable constraint. Expression:"
          ^^ "@,  @[<hov 2>%a@]"
          ^^ "@,cannot be unified with:"
          ^^ "@,  @[<hov 2>%a@]@]")
          Fmt.pp_print_lexp (clean lxp1)
          Fmt.pp_print_lexp (clean lxp2))
    new_constraints

and unify' (e1: lexp) (e2: lexp)
           (ctx : lexp_context) (vs : OL.set_plexp)
           (msl : scope_level option) (* matching mode optional scope level *)
    : return_type =
  if e1 == e2 then [] else
  let e1' = OL.lexp_whnf e1 ctx in
  let e2' = OL.lexp_whnf e2 ctx in
  if e1' == e2' then [] else
  let changed = true (* not (e1 == e1' && e2 == e2') *) in
  if changed && OL.set_member_p vs e1' e2' then [] else
  let vs' = if changed then OL.set_add vs e1' e2' else vs in
  match (lexp_lexp' e1', lexp_lexp' e2') with
    (* Both expressions are in WHNF: we don't have `Let`s nor `Susp`s. *)

    (* First, we handle the simple cases with no substructure. *)
    | ((Imm _ | Builtin _), (Imm _ | Builtin _))
      -> if OL.conv_p ctx e1' e2' then [] else [(CKimpossible, ctx, e1, e2)]

    (* Then, we handle metavariables (aka. flexible-flexible and
       Flexible-Rigid Equations). Reminder: WHNF implies that the
       metavariables are not already instanciated. *)
    | (_, Metavar (idx, s, _)) -> unify_metavar msl ctx idx s e2' e1' vs'
    | (Metavar (idx, s, _), _) -> unify_metavar msl ctx idx s e1' e2' vs'

    (* Otherwise, the equation is rigid-rigid. Let's start with the
       cases that can leave residuals: 1. Variables, since they could
       be substituted with further reduction; 2. Calls, because they
       might become redexes; 3. Case expressions, for the same
       reason. *)

    (* FIXME: Does the order of these branches matter? If we can place
       the Lambda branches above the Var branches, we can avoid
       having to explicitly name the Lambda * Var pair for the
       η expansion of Lambdas. *)
    | (Lambda _, Var _)        -> unify_lambda   msl e1' e2' ctx vs'
    | (_, Var _)               -> unify_var      e2' e1' ctx
    | (Var _, Lambda _)        -> unify_lambda   msl e2' e1' ctx vs'
    | (Var _, _)               -> unify_var      e1' e2' ctx
    | (_, Call _)              -> unify_call msl e2' e1' ctx vs'
    | (Call _, _)              -> unify_call msl e1' e2' ctx vs'
    (* FIXME: Handle `Case` expressions. *)

    (* We are left with the cases that are not affected by
       substitutions, thus should not immediately lead to residual
       constraints. *)
    | (_, Arrow _)             -> unify_arrow     msl e2' e1' ctx vs'
    | (Arrow _, _)             -> unify_arrow     msl e1' e2' ctx vs'
    | (_, Lambda _)            -> unify_lambda    msl e2' e1' ctx vs'
    | (Lambda _, _)            -> unify_lambda    msl e1' e2' ctx vs'
    | (_, Sort _)              -> unify_sort      msl e2' e1' ctx vs'
    | (Sort _, _)              -> unify_sort      msl e1' e2' ctx vs'
    | (_, SortLevel _)         -> unify_sortlvl   msl e2' e1' ctx vs'
    | (SortLevel _, _)         -> unify_sortlvl   msl e1' e2' ctx vs'
    | (_, Inductive _)         -> unify_inductive msl e2' e1' ctx vs'
    | (Inductive _, _)         -> unify_inductive msl e1' e2' ctx vs'
    | (_, Cons _)              -> unify_cons      msl e2' e1' ctx vs'
    | (Cons _, _)              -> unify_cons      msl e1' e2' ctx vs'

    | _ -> (if OL.conv_p ctx e1' e2' then []
           else (add_residue ctx e1' e2'; [(CKresidual, ctx, e1, e2)]))

(************************* Type specific unify *******************************)

(** Unify a Arrow and a lexp if possible
 - (Arrow, Arrow) -> if var_kind = var_kind
                     then unify ltype & lexp (Arrow (var_kind, _, ltype, lexp))
                     else None
 - (_, _) -> None
*)
and unify_arrow (matching : scope_level option) (arrow: lexp) (lxp: lexp) ctx vs
  : return_type =
  match (lexp_lexp' arrow, lexp_lexp' lxp) with
  | (Arrow (_, var_kind1, v1, ltype1, lexp1),
     Arrow (_, var_kind2, _, ltype2, lexp2))
    -> if var_kind1 = var_kind2
      then (unify' ltype1 ltype2  ctx vs matching)
           @(unify' lexp1 (srename v1 lexp2)
               (DB.lexp_ctx_cons ctx v1 Variable ltype1)
               (OL.set_shift vs) matching)
       else [(CKimpossible, ctx, arrow, lxp)]
  | (_, _) -> [(CKimpossible, ctx, arrow, lxp)]

(** Unify a Lambda and a lexp if possible
 - Lambda    , Lambda             -> if var_kind = var_kind
                                     then UNIFY ltype & lxp else ERROR
 - Lambda    , e                  -> UNIFY lambda with η expansion of e
 - else                           -> Impossible
*)
and unify_lambda (matching : scope_level option)
                 (lambda: lexp) (lxp: lexp) ctx vs =
  match (lexp_lexp' lambda, lexp_lexp' lxp) with
  | (Lambda (var_kind1, v1, ltype1, lexp1),
     Lambda (var_kind2, _, ltype2, lexp2))
    -> if var_kind1 = var_kind2
      then (unify' ltype1 ltype2  ctx vs matching)
           @(unify' lexp1 lexp2
               (DB.lexp_ctx_cons ctx v1 Variable ltype1)
               (OL.set_shift vs) matching)
      else [(CKimpossible, ctx, lambda, lxp)]
  | (Lambda (arg_kind, arg, ltype, _), e) ->
      (* η expansion of Lambda *)
      let expansion = mkLambda (arg_kind, arg, ltype,
                                mkCall (dsinfo, push_susp (hc e) (S.shift 1),
                                        [(arg_kind, mkVar ((dsinfo, None), 0))])) in
      unify_lambda matching lambda expansion ctx vs
  | (_, _) -> [(CKimpossible, ctx, lambda, lxp)]

(** Unify a Metavar and a lexp if possible
 - metavar   , metavar            -> if Metavar = Metavar then intersect
 - metavar   , metavar            -> inverse subst (try both sides)
 - metavar   , lexp               -> inverse subst
*)
and unify_metavar (matching : scope_level option)
                  ctx idx s1 (lxp1: lexp) (lxp2: lexp) vs' : return_type =
  let unif idx s lxp =
    let t, sl = match metavar_lookup idx with
      | MVal _ -> Log.internal_error
                   "`lexp_whnf` returned an instantiated metavar!!"
      | MVar (sl, t, _) -> push_susp t s, sl in
    if not (matching_instantiation_check matching sl)
    then (add_residue ctx lxp1 lxp2; [(CKresidual, ctx, lxp1, lxp2)]) else
    match Inverse_subst.apply_inv_subst lxp s with
    | exception Inverse_subst.Not_invertible
      -> log_info
           ("@[<v>Unification of metavar failed:"
            ^^ "@,  ?[@[<hov 2>%a@]]"
            ^^ "@,against:"
            ^^ "@,  @[<hov 2>%a@]@]")
           Fmt.pp_print_subst s
           pp_print_clean_lexp lxp;
        (add_residue ctx lxp1 lxp2; [(CKresidual, ctx, lxp1, lxp2)])
    | lxp' when occurs_in idx lxp' -> [(CKimpossible, ctx, lxp1, lxp2)]
    | lxp'
      -> associate idx lxp';
         let type_unif_residue =
           match unify' t (OL.get_type ctx lxp) ctx vs' matching with
           | [] as r -> r
           (* FIXME: Let's ignore the error for now.  *)
           | _
             -> log_info
                  ("@[<v>Unificaton of metavar type failed. Type:"
                   ^^ "@,  @[<hov 2>%a@]"
                   ^^ "@,is not equal to:"
                   ^^ "@,  @[<hov 2>%a@]"
                   ^^ "@,for:"
                   ^^ "@,  @[<hov 2>%a@]@]")
                  pp_print_clean_lexp t
                  pp_print_clean_lexp (OL.get_type ctx lxp)
                  pp_print_clean_lexp lxp;
                (add_residue ctx lxp1 lxp2; [(CKresidual, ctx, lxp1, lxp2)]) in
         (* FIXME Here, we unify lxp1 with lxp2 again, because that
            the metavariables occuring in the associated term might
            have different substitutions from the corresponding
            metavariables on the other side (due to subst inversion
            not being a perfect inverse of subtitution application).

            For example, when unifying `?τ↑1[114]` with `(List[56]
            ?ℓ↑0[117] ?a↑0[118])`, we associate the metavar ?τ[114]
            with the lexp `(List[55] ?ℓ() · ↑0[117] ?a() · ↑0[118])`
            (after applying the inverse substitution of ↑1), and the
            lexp (?τ↑1[114]) becomes `(List[56] ?ℓ(↑1 () · ↑0)[117]
            ?a(↑1 () · ↑0)[118])`, which is not exactly the same as
            the right-hand side. My solution/kludge to this problem is
            to do a second unification to fix the metavar
            substitutions on the right-hand side, but there is
            probably a cleaner way to solve this. *)
         let second_unif_residue = unify' lxp1 lxp2 ctx vs' matching in
         List.append type_unif_residue second_unif_residue in
  match lexp_lexp' lxp2 with
  | Metavar (idx2, s2, name)
    -> if idx = idx2 then
        match common_subset ctx s1 s2 with
        | S.Identity 0 -> []     (* Optimization!  *)
        (* ¡ s1 != s2 !
         * Create a new metavar that can only refer to those vars
         * which are mapped identically by `s1` and `s2`
         * (as found by `common_subset`).
         * This metavar doesn't necessarily live exactly in `ctx`
         * nor even a proper prefix of it, tho :-( !!
         *)
        | s ->
           (* print_string "Metavar idx-idx s1!=s2\n"; *)
           assert (not (OL.conv_p ctx lxp1 lxp2));
           match (Inverse_subst.inverse s,
                     metavar_lookup idx) with
              | (None, _)
                -> Log.internal_error
                    "Can't inverse subst returned by common_subset!"
              | (_, MVal _)
                -> Log.internal_error
                    "`lexp_whnf` returned an instantiated metavar!!"
              | (Some s_inv, MVar (sl, t, clen))
                -> if not (matching_instantiation_check matching sl) then [] else
                  let clen' = clen - s_offset s in
                  let t' = mkSusp t s_inv in
                  let newmv = create_metavar_1 sl t' clen' in
                  let lexp = mkMetavar (newmv, s, name) in
                  assert (sl <= clen);
                  assert (sl <= clen');
                  assert (OL.conv_p ctx (mkSusp t' s) t);
                  (* if (OL.conv_p ctx (mkSusp lexp s1) (mkSusp lexp s2)) then
                   *   print_string ("common_subset successful:\n    "
                   *                 ^ subst_string s
                   *                 ^ "\n  =\n    "
                   *                 ^ subst_string s1
                   *                 ^ "\n  ∩\n    "
                   *                 ^ subst_string s2
                   *                 ^ "\n")
                   * else
                   *   print_string ("common_subset failed:\n    "
                   *                 ^ subst_string s
                   *                 ^ "\n  ∘\n    "
                   *                 ^ subst_string s1
                   *                 ^ "\n  =\n    "
                   *                 ^ subst_string (scompose s s1)
                   *                 ^ "\n!=\n    "
                   *                 ^ subst_string s
                   *                 ^ "\n  ∘\n    "
                   *                 ^ subst_string s2
                   *                 ^ "\n  =\n    "
                   *                 ^ subst_string (scompose s s2)
                   *                 ^ "\n"); *)
                  associate idx lexp;
                  assert (OL.conv_p ctx lxp1 lxp2);
                  []
      else
        (* If one of the two subst can't be inverted, try the other.
         * FIXME: There's probably a more general solution.  *)
        (match unif idx s1 lxp2 with
         | [] -> []
         | _ -> unif idx2 s2 lxp1)
  | _ -> unif idx s1 lxp2

(** Unify a Var (var) and a lexp (lxp)
 - Var      , Var                -> IF same var THEN ok ELSE constraint
 - Var      , lexp               -> Constraint
*)
and unify_var (var: lexp) (lxp: lexp) ctx
    : return_type =
  match (lexp_lexp' var, lexp_lexp' lxp) with
  | (Var _, Var _) when OL.conv_p ctx var lxp -> []
  | (_, _) -> (add_residue ctx var lxp; [(CKresidual, ctx, var, lxp)])

(** Unify a Call (call) and a lexp (lxp)
 - Call      , Call               -> UNIFY
 - Call      , lexp               -> CONSTRAINT
*)
and unify_call (matching : scope_level option) (call: lexp) (lxp: lexp) ctx vs
    : return_type =
  match (lexp_lexp' call, lexp_lexp' lxp) with
  | (Call (_, lxp_left, lxp_list1), Call (_, lxp_right, lxp_list2))
       when OL.conv_p ctx lxp_left lxp_right
    -> (try List.fold_left (fun op ((ak1, e1), (ak2, e2))
                      -> if ak1 == ak2 then
                          (unify' e1 e2 ctx vs matching)@op
                        else [(CKimpossible, ctx, call, lxp)])
                     []
                     (List.combine lxp_list1 lxp_list2)
        with Invalid_argument _ (* Lists of diff. length in combine. *)
             -> (add_residue ctx call lxp; [(CKresidual, ctx, call, lxp)]))
  | (call', lxp') ->
     let head_left = match call' with
       | Call (_, head_left, _) -> head_left
       | _ -> call in
     let head_right = match lxp' with
       | Call (_, head_right, _) -> head_right
       | _ -> lxp in
     let for_sure_irreducible_call_head ctx head =
       match OL.lexp'_whnf head ctx with
       | (Imm _ | Cons _ | Inductive _) -> true
       | Builtin ((_, name), _)
         -> not (SMap.mem name !Opslexp.reducible_builtins)
       | _ -> false in
     if for_sure_irreducible_call_head ctx head_left &&
          for_sure_irreducible_call_head ctx head_right &&
            not (OL.conv_p ctx head_left head_right)
     then (* Whichever argument or substitution is applied, the
             inconvertible heads will remain. *)
       [(CKimpossible, ctx, head_left, head_right)]
     else
       (add_residue ctx call lxp; [(CKresidual, ctx, call, lxp)])

(** Unify a Case with a lexp
 - Case, Case -> try to unify
 - Case, _ -> Constraint
*)
(* and unify_case (case: lexp) (lxp: lexp) (subst: meta_subst) : return_type =
 *   let merge (_, const) subst_res = match subst_res with
 *     | None -> None
 *     | Some (s', c') -> Some (s', const@c')
 *   in
 *   let match_unify_inner lst smap1 smap2 subst =
 *     match unify_inner lst subst with
 *     | None -> None
 *     | Some (s, c) -> merge (s, c) (unify_inner_case (zip (SMap.bindings smap1) (SMap.bindings smap2)) s)
 *   in
 *   let match_lxp_opt lxp_opt1 lxp_opt2 tail smap1 smap2 subst =
 *     match lxp_opt1, lxp_opt2 with
 *     | Some (_, lxp1), Some (_, lxp2)
 *       -> match_unify_inner ((lxp1, lxp2)::tail) smap1 smap2 subst
 *     | _, _ -> None
 *   in
 *   match (case, lxp) with
 *     | (Case (_, lxp, lt12, smap, lxpopt), Case (_, lxp2, lt22, smap2, lxopt2))
 *       -> match_lxp_opt lxpopt lxopt2 ((lt12, lt22)::[]) smap smap2 subst
 *     | (Case _, _)     -> Some (subst, [(case, lxp)])
 *     | (_, _) -> None *)

(** Unify a Inductive and a lexp
 - Inductive, Inductive -> try unify
 - Inductive, Var -> constraint
 - Inductive, Call/Metavar/Case/Let -> constraint
 - Inductive, _ -> None
*)
(* and unify_induct (induct: lexp) (lxp: lexp) (subst: meta_subst) : return_type =
 *   let transform (a, b, c) (d, e, f) = ((a, Some b, c), (d, Some e, f))
 *   and merge map1 map2 (subst, const) : return_type =
 *     match (unify_induct_sub_list (SMap.bindings map1) (SMap.bindings map2) subst) with
 *     | Some (s', c') -> Some (s', const@c')
 *     | None -> None
 *   in
 *   let zip_unify lst subst map1 map2 : return_type =
 *     match unify_inner_induct lst subst with
 *     | None        -> None
 *     | Some (s, c) -> merge map1 map2 (s, c)
 *   in
 *   match (induct, lxp) with
 *   | (Inductive (_, lbl1, farg1, m1), Inductive (_, lbl2, farg2, m2)) when lbl1 = lbl2 ->
 *     (match zip_map farg1 farg2 transform with
 *      | Some [] -> Some (subst, [])
 *      | Some lst -> zip_unify lst subst m1 m2
 *      | None -> None)
 *   | (Inductive _, Var _) -> Some (subst, [(induct, lxp)])
 *   | (_, _) -> None *)

(** Unify a SortLevel with a lexp
 - SortLevel, SortLevel -> if SortLevel ~= SortLevel then OK else ERROR
 - SortLevel, _         -> ERROR
*)
and unify_sortlvl (matching : scope_level option)
                  (sortlvl: lexp) (lxp: lexp) ctx vs : return_type =
  match lexp_lexp' sortlvl, lexp_lexp' lxp with
  | (SortLevel s, SortLevel s2) -> (match s, s2 with
      | SLz, SLz -> []
      | SLsucc l1, SLsucc l2 -> unify' l1 l2 ctx vs matching
      | SLlub (l11, l12), SLlub (l21, l22)
        -> (* FIXME: This SLlub representation needs to be
           * more "canonicalized" otherwise it's too restrictive!  *)
         (unify' l11 l21 ctx vs matching)@(unify' l12 l22 ctx vs matching)
      | SLlub (l1, l2), other | other, SLlub (l1, l2)
        when OL.conv_p ctx l1 l2
        (* Arbitrarily selected `l1` over `l2` *)
        -> unify' l1 (mkSortLevel other) ctx vs matching
      | SLlub (l1, l2), SLz | SLz, SLlub (l1, l2)
        (* In this case, both `l1` and `l2` must be `SLz` *)
        -> (unify' l1 (mkSortLevel SLz) ctx vs matching)
           @(unify' l2 (mkSortLevel SLz) ctx vs matching)
      | _, _ -> (add_residue ctx sortlvl lxp;
                 [(CKresidual, ctx, sortlvl, lxp)]))
  | _, _ -> [(CKimpossible, ctx, sortlvl, lxp)]

(** Unify a Sort and a lexp
 - Sort, Sort -> if Sort ~= Sort then OK else ERROR
 - Sort, lexp -> ERROR
*)
and unify_sort (matching : scope_level option)
               (sort_: lexp) (lxp: lexp) ctx vs : return_type =
  match lexp_lexp' sort_, lexp_lexp' lxp with
  | (Sort (_, srt), Sort (_, srt2)) -> (match srt, srt2 with
      | Stype lxp1, Stype lxp2 -> unify' lxp1 lxp2 ctx vs matching
      | StypeOmega, StypeOmega -> []
      | StypeLevel, StypeLevel -> []
      | _, _ -> [(CKimpossible, ctx, sort_, lxp)])
  | _, _ -> [(CKimpossible, ctx, sort_, lxp)]

(************************ Helper function ************************************)

(***** for Case *****)
(** Check arg_king in <code>(arg_kind * vdef option) list </code> in Case *)
and is_same arglist arglist2 =
  match arglist, arglist2 with
  | (akind, _)::t1, (akind2, _)::t2 when akind = akind2 -> is_same t1 t2
  | [], []                                              -> true
  | _, _                                                -> false

(** try to unify the SMap part of the case *)
(* and unify_inner_case lst subst =
 *   let merge (_, c) res =
 *     match res with
 *     | Some (s', c') -> Some (s', c@c')
 *     | None          -> None
 *   in
 *   let rec unify_inner_case list_ subst =
 *     match list_ with
 *     | ((key, (_, arglist, lxp)), (key2, (_, arglist2, lxp2)))::tail when key = key2 ->
 *       (if is_same arglist arglist2
 *        then ( match unify lxp lxp2 subst with
 *            | Some (s', c) -> merge (s', c) (unify_inner_case tail s')
 *            | None         -> None)
 *        else None)
 *     | [] -> Some (subst, [])
 *     | _ -> None
 *   in (match lst with
 *       | Some [] -> Some (subst, [])
 *       | None -> None
 *       | Some l -> unify_inner_case l subst) *)

(***** for Inductive *****)
(** for unify_induct : unify the formal arg*)
(* and unify_inner_induct lst subst : return_type =
 *   let test ((a1, _, l1), (a2, _, l2)) subst : return_type =
 *     if a1 = a2 then unify l1 l2 subst
 *     else None
 *   in
 *   List.fold_left (fun a e ->
 *       (match a with
 *        | Some (s, c) ->
 *          (match test e s with
 *           | Some (s1, c1) -> Some (s1, c1@c)
 *           | None -> Some (s, c))
 *        | None -> test e subst)
 *     ) None lst *)

and unify_inductive (matching : scope_level option) lxp1 lxp2 ctx vs =
  match lexp_lexp' lxp1, lexp_lexp' lxp2 with
  | (Inductive (_loc1, _label1, args1, consts1),
     Inductive (_loc2, _label2, args2, consts2))
    -> unify_inductive' matching ctx vs args1 args2 consts1 consts2 lxp1 lxp2
  | _, _ -> [(CKimpossible, ctx, lxp1, lxp2)]

and unify_inductive' (matching : scope_level option) ctx vs
                     args1 args2 consts1 consts2 e1 e2 =
  let unif_formals ctx vs args1 args2
    = if not (List.length args1 == List.length args2) then
        (ctx, vs, [(CKimpossible, ctx, e1, e2)])
      else
        List.fold_left (fun (ctx, vs, residue) ((ak1, v1, t1), (ak2, _v2, t2))
                        -> (DB.lexp_ctx_cons ctx v1 Variable t1,
                           OL.set_shift vs,
                           if not (ak1 == ak2) then [(CKimpossible, ctx, e1, e2)]
                           else (unify' t1 t2 ctx vs matching) @ residue))
          (ctx, vs, [])
          (List.combine args1 args2) in
  let (ctx, vs, residue) = unif_formals ctx vs args1 args2 in
  if not (SMap.cardinal consts1 == SMap.cardinal consts2) then
    [(CKimpossible, ctx, e1, e2)]
  else
    SMap.fold (fun cname args1 residue
               -> match SMap.find cname consts2 with
                 | args2 -> let (_ctx, _vs, residue')
                             = unif_formals ctx vs args1 args2 in
                           residue' @ residue
                 | exception Not_found -> [(CKimpossible, ctx, e1, e2)])
      consts1 residue

(** unify the SMap of list in Inductive *)
(* and unify_induct_sub_list l1 l2 subst =
 *   let test l1 l2 subst =
 *     let merge l1 l2 subst (s, c) =
 *       match (unify_induct_sub_list l1 l2 subst) with
 *       | Some (s1, c1) -> Some (s1, c1@c)
 *       | None -> Some (s, c)
 *     in
 *     let unify_zip lst t1 t2 = match unify_inner_induct lst subst with
 *       | Some (s, c) -> merge l1 l2 subst (s, c)
 *       | None -> (unify_induct_sub_list t1 t2 subst)
 *     in
 *     match l1, l2 with
 *     | (k1, v1)::t1, (k2, v2)::t2 when k1 = k2 ->
 *       (match zip v1 v2 with
 *        | Some [] -> Some (subst, [])
 *        | Some lst -> unify_zip lst t1 t2
 *        | None -> None)
 *     | _, _ -> None
 *   in test l1 l2 subst *)

and unify_cons (matching : scope_level option) lxp1 lxp2 ctx vs =
  match lexp_lexp' lxp1, lexp_lexp' lxp2 with
  | (Cons (it1, (_, l1)), Cons (it2, (_, l2))) when l1 = l2
    -> unify' it1 it2 ctx vs matching
  | _, _ -> [(CKimpossible, ctx, lxp1, lxp2)]

(* This function attempts to unify `e1` with `e2`, but discards all
   side-effects, except if no impossible constraints nor any residues have
   been found.
   In the latter case, any instanciated metavariables are kept instanciated. *)
let check_unifiable ?(matching : scope_level option)
                    (e1: lexp) (e2: lexp)
                    (ctx : lexp_context) =
  let old_residue_list = !current_residues in
  let old_metavars = !metavar_table in
  let unify_result = unify' e1 e2 ctx OL.set_empty matching in
  current_residues := old_residue_list;
  match unify_result with
  | [] -> []
  | _ -> (metavar_table := old_metavars;
          unify_result)
