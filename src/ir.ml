(* Copyright (C) 2020-2023  Free Software Foundation, Inc.
 *
 * Author: Maxim Bernard <maxim.bernard@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>. *)

(* Typer Internal Representations
 * 
 * This file contains type definitions for various internal representations of
 * user code : s-expressions, lambda-expressions, type-erased lexps, values. *)

open Util


(*** Context representation ***)

type ctx_length = int

(* Scope level is used to detect "out of scope" metavars.
 * See http://okmij.org/ftp/ML/generalization.html *)
type scope_level = int

(* DeBruijn reverse index (i.e. counting from the root).  *)
type db_ridx = int
(* DeBruijn index. *)
type db_index = int
(* DeBruijn index offset. *)
type db_offset = int
(* DeBruijn index counting from the root. *)
type db_revindex = int

(* Sets of DeBruijn indices *)
type db_set = db_offset * unit IMap.t

(*  Map variable name to its distance in the context *)
type senv_length = int  (* it is not the map true length *)
type senv_type = senv_length * (db_ridx SMap.t)

type meta_id = int
type meta_scope =
  scope_level (* Integer identifying a level.  *)
  * ctx_length (* Length of ctx when the scope is added.  *)
  * ((meta_id * scope_level) SMap.t ref) (* Metavars already known in this scope.  *)


(*** Code representation ***)

type pretoken =
  | Pretoken of location * string
  | Prestring of location * string
  | Preblock of location * pretoken list

type symbol = location * string
type label = symbol

type sexp =
  | Block of location * pretoken list
  | Symbol of symbol
  | String of location * string
  | Integer of location * Z.t
  | Float of location * float
  | Node of location * sexp * sexp list
type token = sexp
type sinfo = sexp

type vname = sinfo * string option
type vref = vname * db_index

type arg_kind =
  | Anormal
  | Aimplicit
  | Aerasable (** eraseable ⇒ implicit.  *)


(*** Context representation - Mutually recursive definitions ***)

type varbind =
  | Variable
  | ForwardRef
  | LetDef of db_offset * lexp

(* easier to debug with type annotations *)
and env_elem = (vname * varbind * ltype)
and lexp_context = env_elem Myers.myers

and typeclass_ctx =
  (ltype * ctx_length) list (* the list of type classes *)
  * bool (* true if new bindings are instances
            (used to implement (dont-)bind-instances) *)
  * db_set (* The set of bindings that are instances *)

(* This is the *elaboration context* (i.e. a context that holds
 * a lexp context plus some side info.  *)
and elab_context =
  Grammar.t * senv_type * lexp_context * meta_scope * typeclass_ctx

(* Runtime Environment *)
and runtime_env = (vname * (value ref)) Myers.myers


(*** Code representation - Mutually recursive definitions ***)

and ltype = lexp
and subst = lexp Subst.subst
(* Here we want a pair of `Lexp` and its hash value to avoid re-hashing "sub-Lexp". *)
and lexp = lexp' * int
and lexp' =
  | Imm of sinfo * value                        (* Used for strings, ...  *)
  | Sort of sinfo * sort
  | SortLevel of sort_level
  | Builtin of symbol * ltype
  | Var of vref
  | Proj of sinfo * lexp * label
  | Susp of lexp * subst  (* Lazy explicit substitution: e[σ].  *)
  (* The scoping of `Let` is tricky:
   *
   * Since it's a recursive let, the definition part of each binding is
   * valid in the "final" scope which includes all the new bindings.
   *
   * But the type of each binding is not defined in that same scope.  Instead
   * it's defined in the scope of all the previous bindings.
   *
   * For exemple the type of the second binding of such a Let is defined in
   * the scope of the surrounded context extended with the first binding.
   * And the type of the 3rd binding is defined in the scope of the
   * surrounded context extended with the first and the second bindings.  *)
  | Let of sinfo * ldecls * lexp
  | Arrow of sinfo * arg_kind * vname * ltype * ltype
  | Lambda of arg_kind * vname * ltype * lexp
  | Call of sinfo * lexp * (arg_kind * lexp) list (* Curried call.  *)
  | Inductive of sinfo * label
                 * ((arg_kind * vname * ltype) list) (* formal Args *)
                 * ((arg_kind * vname * ltype) list) SMap.t
  | Cons of lexp * symbol (* = Type info * ctor_name  *)
  | Case of sinfo * lexp
            * ltype (* The type of the return value of all branches *)
            * (sinfo * (arg_kind * vname) list * lexp) SMap.t
            * (vname * lexp) option               (* Default.  *)
  (* The `subst` will be applied to the the metavar's value when it
   * gets instantiated.  *)
  | Metavar of meta_id * subst * vname
 (*   (\* For logical metavars, there's no substitution.  *\)
  *   | Metavar of (U.location * string) * metakind * metavar ref
  * and metavar =
  *   (\* An uninstantiated var, along with a venv (stipulating over which vars
  *    * it should be closed), and its type.
  *    * If its type is not given, it implies its type should be a sort.  *\)
  *   | MetaUnset of (lexp option * lexp) VMap.t * ltype option * scope_level
  *   | MetaSet of lexp
  * and metakind =
  *   | MetaGraft of subst
  *   (\* Forward reference or Free var: Not known yet, but not instantiable by
  *    * unification.  *\)
  *   | MetaFoF
  * and subst = lexp VMap.t *)
 (*
  * The PTS I'm imagining looks like:
  *
  *    S = { TypeLevel, TypeOmega, Type ℓ }
  *    A = { Level : TypeLevel, Z : Level, S : Level → Level,
  *          Type : (ℓ : Level) → Type (S ℓ) }
  *    R = { (TypeLevel, Type ℓ, TypeOmega),
  *          (TypeLevel, TypeOmega, TypeOmega),
  *          (Type ℓ, TypeOmega, TypeOmega),
  *          (Type ℓ₁, Type ℓ₂, Type (max l₁ l₂) }
  *)

and sort =
  | Stype of lexp
  | StypeOmega
  | StypeLevel
and sort_level =
  | SLz
  | SLsucc of lexp
  | SLlub of lexp * lexp

and ldecl = vname * lexp * ltype
and ldecls = ldecl list

and elexp =
  (* A constant, either string, integer, or float.  *)
  | Eimm of location * value

  (* A builtin constant, typically a function implemented in Ocaml.  *)
  | Ebuiltin of symbol

  (* A variable reference, using deBruijn indexing.  *)
  | Evar of vref

  | Eproj of location * elexp * int

  (* Recursive `let` binding.  *)
  | Elet of location * eldecls * elexp

  (* An anonymous function.  *)
  | Elambda of vname * elexp

  (* A (curried) function call.
   * In other words,         Call(f, [e1, e2])
   * is just a shorthand for Call (Call (f, [e1]), [e2]).  *)
  | Ecall of elexp * elexp list

  (* A data constructor, such as `cons` or `nil`. The int is its arity and the
   * symbol, its name. *)
  | Econs of int * symbol

  (* Case analysis on an agebraic datatype.
   * Case (l, e, branches, default)
   * tests the value of `e`, and either selects the corresponding branch
   * in `branches` or branches to the `default`.  *)
  | Ecase of location
            * elexp
            * (location * vname list * elexp) SMap.t
            * (vname * elexp) option

  (* A Type expression.  There's no useful operation we can apply to it,
   * but they can appear in the code.  *)
  | Etype of lexp

and eldecl = vname * elexp
and eldecls = eldecl list

and value =
  | Vint of int
  | Vinteger of Z.t
  | Vstring of string
  | Vcons of symbol * value list
  | Vbuiltin of string
  | Vfloat of float
  | Closure of vname * elexp * runtime_env
  | Vsexp of sexp             (* Values passed to macros.  *)
  (* Unable to eval during macro expansion, only throw if the value is used *)
  | Vundefined
  | Vtype of lexp           (* The lexp value can't be trusted.  *)
  | Vin of in_channel
  | Vout of out_channel
  | Vcommand of (unit -> value)
  | Vref of (value ref)
  | Velabctx of elab_context
  | Varray of (value array)


(*** Utility functions ***)

let pretoken_location : pretoken -> location = function
  | Preblock (location, _) | Pretoken (location, _) | Prestring (location, _)
    -> location

let sexp_location : sexp -> location = function
  | Block (l, _) -> l
  | Symbol (l, _) -> l
  | String (l, _) -> l
  | Integer (l, _) -> l
  | Float (l, _) -> l
  | Node (l, _, _) -> l
