(* Copyright (C) 2020-2023  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>. *)

(** A heap of Typer objects that can be partially initialized. *)

open Ir
open Eval

module IMap = Util.IMap
module OL = Opslexp

type location = Source.Location.t

type size = int
type addr = int

let error ~(loc : location) ?print_action fmt =
  Log.log_fatal ~section:"HEAP" ~loc ?print_action fmt

let dloc = Util.dummy_location
let type0 = Debruijn.type0

let next_free_address : addr ref = ref 1

type t = (symbol option ref * value option array) IMap.t ref

let heap : t = ref IMap.empty

let alloc (cells_count : size) : addr =
  let address = !next_free_address in
  next_free_address := address + 1;
  heap := IMap.add address (ref None, Array.make cells_count None) !heap;
  address

let free (address : addr) : unit =
  heap := IMap.remove address !heap

(** If address points to a fully initialized object, returns it. *)
let export (address : addr) : value =
  let rec export_cells cells n acc =
    if n == 0
    then acc
    else
      let n = n - 1 in
      export_cells cells n (Option.get cells.(n) :: acc)
  in
  let constructor, cells = IMap.find address !heap in
  Vcons (Option.get !constructor, export_cells cells (Array.length cells) [])

(** Initializes the header of the object so it is valid for the given data
   constructor. *)
let store_header
      (loc : location)
      (address : addr)
      (datacons : value)
    : unit =

  match datacons with
  | Vcons (symbol, [])
    -> let constructor, _ = IMap.find address !heap in
       constructor := Some symbol
  | _ -> error ~loc "not a data constructor: %s" (Fmt.string_of_value datacons)

(** If [address] points to an object of at least [cell_index + 1] cells, mutates
   the value at that index. *)
let store_cell (address : addr) (cell_index : int) (value : value) : unit =
  let _, cells = IMap.find address !heap in
  cells.(cell_index) <- Some value

(** If [address] points to an object of at least [cell_index + 1] cells and the
   cell at [cell_index] is initialized, returns the value at that index. *)
let load_cell (address : addr) (cell_index : int) : value =
  let _, cells = IMap.find address !heap in
  Option.get cells.(cell_index)

let datacons_label_of_string : builtin_function =
  fun loc _
  -> function
  | [Vstring _ as label] -> label
  | _ -> error ~loc "`datacons-label<-string` expects [##DataconsLabel]."

let heap_alloc : builtin_function =
  fun loc _
  -> function
  | [Vint size] -> Vcommand (fun () -> Vint (alloc size))
  | _ -> error ~loc "`Heap.alloc` expects [Int]."

let heap_free : builtin_function =
  fun loc _
  -> function
  | [Vint address] -> Vcommand (fun () -> free address; Vint 0)
  | _ -> error ~loc "`Heap.free` expects [Int]."

let heap_export : builtin_function =
  fun loc _
  -> function
  | [Vint address] -> Vcommand (fun () -> export address)
  | _ -> error ~loc "`Heap.export` expects [Int]."

let heap_store_header : builtin_function =
  fun loc _
  -> function
  | [Vint address; datacons]
    -> Vcommand (fun () -> store_header loc address datacons; Vint 0)
  | _ -> error ~loc "`Heap.store-header` expects [Int; 'a]"

let heap_store_cell : builtin_function =
  fun loc _
  -> function
  | [Vint address; Vint cell_index; value]
    -> Vcommand (fun () -> store_cell address cell_index value; Vint 0)
  | _ -> error ~loc "`Heap.store-header` expects [Int; Int; 'a]"

let heap_load_cell : builtin_function =
  fun loc _
  -> function
  | [Vint address; Vint cell_index; _value]
    -> Vcommand (fun () -> load_cell address cell_index)
  | _ -> error ~loc "`Heap.store-cell` expects [Int; Int]"

let register_builtins () =
  add_builtin_function "Heap" Eval.type_constructor max_int;
  add_builtin_function "datacons-label<-string" datacons_label_of_string 1;
  add_builtin_function "Heap.alloc" heap_alloc 1;
  add_builtin_function "Heap.free" heap_alloc 1;
  add_builtin_function "Heap.export" heap_alloc 1;
  add_builtin_function "Heap.store-header" heap_store_header 2;
  add_builtin_function "Heap.store-cell" heap_store_cell 3;
  add_builtin_function "Heap.load-cell" heap_load_cell 2;
