(* opslexp.ml --- Operations on Lexps

Copyright (C) 2011-2023  Free Software Foundation, Inc.

Author: Stefan Monnier <monnier@iro.umontreal.ca>
Keywords: languages, lisp, dependent types.

This file is part of Typer.

Typer is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

Typer is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.  *)

open Ir
open Lexp

module U = Util
module P = Pexp
module E = Elexp
module L = Lexp
module M = Myers
module S = Subst
module DB = Debruijn

module SMap = U.SMap
module IMap = U.IMap

type set_plexp = (lexp * lexp) list
type sort_compose_result
  = SortResult of ltype
  | SortInvalid
  | SortK1NotType
  | SortK2NotType
type mv_set = (scope_level * ltype * ctx_length * vname) IMap.t
              (* Metavars that appear in non-erasable positions.  *)
              * unit IMap.t

let error ~location fmt =
  Log.log_fatal ~section:"OPSLEXP" ~loc:location fmt

let dloc = DB.dloc
let dsinfo = DB.dsinfo

module LMap
  (* Memoization table.  FIXME: Ideally the keys should be "weak", but
   * I haven't found any such functionality in OCaml's libs.  *)
  = Hashtbl.Make
      (struct type t = lexp let hash = Hashtbl.hash let equal = (==) end)

let reducible_builtins
  = ref (SMap.empty : (lexp_context
                       -> (arg_kind * lexp) list (* The builtin's args *)
                       (* The reduction result and the remaining args *)
                       -> (lexp * (arg_kind * lexp) list) option) SMap.t)

let log_tc_error ?print_action ?loc fmt =
  Log.log_error ~section:"TC" ?print_action ?loc fmt

let log_tc_warning ?print_action ?loc fmt =
  Log.log_warning ~section:"TC" ?print_action ?loc fmt

(* `conv_erase` is supposed to be safe according to the ICC papers.
   It might be incompatible with the reduction of `Eq.cast`, though. See :
   https://mailman.iro.umontreal.ca/pipermail/typer/2020-October/000870.html *)
let conv_erase = true          (* Makes conv ignore erased terms. *)

(* `impredicative_erase` is inconsistent: as shown in samples/hurkens.typer
 * it allows the definition of an inf-looping expression of type ⊥.  *)
let impredicative_erase = false (* Allows erasable args to be impredicative. *)

(* The safety of `impredicative_universe_poly` is unknown.
 * But I also like the idea.
 * Furthermore it is sufficient to be able to encode System-F (tho
 * I haven't been able to generalize this result to Fω).  *)
let impredicative_universe_poly = true (* Assume arg is TypeLevel.z when erasable. *)

(* Lexp context *)

let lookup_type  = DB.lctx_lookup_type
let lookup_value = DB.lctx_lookup_value

(** Extend a substitution S with a (mutually recursive) set
 * of definitions DEFS.
 * This is rather tricky.  E.g. for
 *
 *     (x₁ = e1; x₂ = e₂)
 *
 * Where x₁ will be DeBruijn #1 and x₂ will be DeBruijn #0,
 * we want a substitution of the form
 *
 *     (let x₂ = e₂ in e₂) · (let x₁ = e₁; x₂ = e₂ in e₁) · Id
 *
 * Because we want #2 in both e₂ and e₁ to refer to the nearest variable in
 * the surrouding context, but the substitution for #0 (defined here as
 * `let x₂ = e₂ in e₂`) will be interpreted in the remaining context,
 * which already provides "x₁".
 *)
(* FXIME: This creates an O(N^2) tree from an O(N) `let`!  *)
let rec lexp_defs_subst l s defs = match defs with
  | [] -> s
  | (_, lexp, _) :: defs'
    -> lexp_defs_subst l (S.cons (mkLet (l, defs, lexp)) s) defs'

(** Convert a lexp_context into a substitution.  *)

let rec lctx_to_subst lctx =
  match DB.lctx_view lctx with
  | DB.CVempty -> Subst.identity
  | DB.CVlet (_, LetDef (_, e), _, lctx)
    -> let s = lctx_to_subst lctx in
       L.scompose (S.substitute e) s
  | DB.CVlet (v, _, _, lctx)
    -> let s = lctx_to_subst lctx in
       (* Here we decide to keep those vars in the target domain.
        * Another option would be to map them to `L.impossible`,
        * hence making the target domain be empty (i.e. making the substitution
        * generate closed results).  *)
       L.ssink v s
  | DB.CVfix (defs, lctx)
    -> let s1 = lctx_to_subst lctx in
       let s2 = lexp_defs_subst dsinfo S.identity
                                (List.rev defs) in
       L.scompose s2 s1

(* Map of lexp builtin elements accessible via (## <name>).
 * Values are pairs consisting of :
 * - the type's De Bruijn offset (i.e. the context's size when the builtin
 *   was added)
 * - the builtin's type  *)
let lmap = ref (SMap.empty : (S.db_offset * lexp) SMap.t)

let add_builtin_cst (name : string) (offset : S.db_offset) (e : lexp)
  = let map = !lmap in
    assert (not (SMap.mem name map));
    lmap := SMap.add name (offset, e) map

let get_builtin (ctx : lexp_context) (name) =
  let (offset, btl) = SMap.find name !lmap in
  mkSusp btl (S.shift ((M.length ctx) - offset))

let type_dummy = DB.type_integer

(** Reduce to weak head normal form.
 * WHNF implies:
 * - Not a Let.
 * - Not a let-bound variable.
 * - Not a Susp.
 * - Not an instantiated Metavar.
 * - Not a Call (Lambda _).
 * - Not a Call (Call _).
 * - Not a Call (_, []).
 * - Not a Case (Cons _).
 * - Not a Proj of a Cons.
 * FIXME: This should be memoized!
 *
 * BEWARE: As a general rule lexp_whnf should not be used on actual *code*
 * but only on *types*.  If you must use it on code, be sure to use its
 * return value as little as possible since WHNF will inherently introduce
 * call-by-name behavior.  *)

(* FIXME This large letrec closes the loop between lexp_whnf and
   get_type so that we can use get_type in the WHNF of a case to get
   the inductive type of the target (and it's typelevel). A better
   solution would be to add these values as annotations in the lexp
   datatype. *)
let rec lexp_whnf e (ctx : lexp_context) : lexp =
  match lexp_lexp' e with
  | Var v -> (match lookup_value ctx v with
             | None -> e
             (* We can do this blindly even for recursive definitions!
              * IOW the risk of inf-looping should only show up when doing
              * things like full normalization (e.g. lexp_conv_p).  *)
             | Some e' -> lexp_whnf e' ctx)
  | Susp (e, s) -> lexp_whnf (push_susp e s) ctx
  | Call (_, e, []) -> lexp_whnf e ctx
  | Call (l, f, (((_, arg)::args) as xs)) ->
     (match lexp'_whnf f ctx with
      | Lambda (_, _, _, body) ->
         (* Here we apply whnf to the arg eagerly to kind of stay closer
          * to the idea of call-by-value, although in this context
          * we can't really make sure we always reduce the arg to a value.  *)
         lexp_whnf (mkCall (l, push_susp body (S.substitute (lexp_whnf arg ctx)),
                            args))
                   ctx
      | Call (l, f', xs1) -> lexp_whnf (mkCall (l, f', List.append xs1 xs)) ctx
      | Builtin ((_, name), _)
        -> (match SMap.find_opt name (!reducible_builtins) with
            | Some f -> Option.value ~default:e
                                     (Option.map
                                       (fun (e, args) ->
                                          lexp_whnf (mkCall (l, e, args)) ctx)
                                       (f ctx xs))
            | None -> e)
      | _ -> e)                 (* Keep `e`, assuming it's more readable!  *)
  | Case (l, e, rt, branches, default) ->
     let e' = lexp_whnf e ctx in
     let reduce it name aargs =
       let targs = match lexp'_whnf it ctx with
         | Inductive (_,_,fargs,_) -> fargs
         | _ -> Log.log_error "Case on a non-inductive type in whnf!"; [] in
       try
         let (_, _, branch) = SMap.find name branches in
         let (subst, _)
           = List.fold_left
               (fun (s, targs) (_, arg) ->
                 match targs with
                 | [] -> (S.cons (lexp_whnf arg ctx) s, [])
                 | _targ::targs ->
                    (* Ignore the type arguments *)
                    (s, targs))
               (S.identity, targs)
               aargs in
         (* Substitute case Eq variable by the proof (Eq.refl l t e') *)
         let subst = S.cons (mk_eq_witness l e' ctx) subst in
         lexp_whnf (push_susp branch subst) ctx
       with
       | Not_found
         -> match default with
            | Some (_v,default)
              -> let subst = S.cons (mk_eq_witness l e' ctx) (S.substitute e') in
                 lexp_whnf (push_susp default subst) ctx
            | _ -> Log.log_error
                     ~section:"WHNF" ~loc:(sexp_location l)
                     {|Unhandled constructor "%s" in case expression|} name;
                   mkCase (l, e, rt, branches, default) in
     (match lexp_lexp' e' with
      | Cons (it, (_, name)) -> reduce it name []
      | Call (_, f, aargs) ->
         (match lexp'_whnf f ctx with
          | Cons (it, (_, name)) -> reduce it name aargs
          | _ -> mkCase (l, e, rt, branches, default))
      | _ -> mkCase (l, e, rt, branches, default))

  | Proj (loc, lxp, (_loc, label))
    -> let c, args = match lexp'_whnf lxp ctx with
         | Call (_loc, f, args) -> f, args
         | _ -> lxp, [] in
       (match lexp'_whnf c ctx with
        | Cons (it, _) ->
           (match lexp'_whnf it ctx with
            | Inductive (_l, _lbl, targs, constructors)
                 when SMap.cardinal constructors = 1
              -> let (_, fields) = List.hd (SMap.bindings constructors) in
                 let rec drop n l =
                   if n == 0 then l else (try drop (n - 1) (List.tl l) with
                                            _ -> []) in
                 let rec getfield label args fields =
                   match args, fields with
                   | _ , [] -> Log.log_error ~loc:(sexp_location loc)
                                 "Tuple does not have the field `%s`" label; e
                   | (_, arg)::_, (_, (_, Some fn), _)::_ when fn = label ->
                      (* Reduce to the argument corresponding to the field *)
                      lexp_whnf arg ctx
                   | _::args, _::fields -> getfield label args fields
                   | [], _ -> (* This case should be impossible through typing *)
                      Log.log_fatal ~loc:(sexp_location loc)
                        "Projected tuple has fewer arguments than fields"
                 in getfield label (drop (List.length targs) args) fields
            | _ -> Log.log_error ~loc:(sexp_location loc) "Proj on a non-tuple in WHNF!"; e)
        | _ -> e) (* Not a proj of a cons: don't reduce. *)

  | Metavar (idx, s, _)
    -> (match metavar_lookup idx with
       | MVal e -> lexp_whnf (push_susp e s) ctx
       | _ -> e)

  (* FIXME: I'd really prefer to use "native" recursive substitutions, using
   *   ideally a trick similar to the db_offsets in lexp_context!  *)
  | Let (l, defs, body)
    -> lexp_whnf (push_susp body (lexp_defs_subst l S.identity defs)) ctx

  | _elem -> e

and lexp'_whnf e (ctx : lexp_context) : lexp' =
  lexp_lexp' (lexp_whnf e ctx)

and conv_builtin_p ctx e name =
  (* FIXME: Maybe we could use `conv_p (get_builin name)` instead?  *)
  match lexp'_whnf e ctx with
  | Builtin ((_, name'), _) -> name = name'
  | _ -> false

and eq_cast_whnf ctx args =
  match args with
  | _l1 :: _l2 :: _t :: _x :: _y :: (_, p) :: _f :: (_, fx) :: rest
    -> (match lexp'_whnf p ctx with
        | Call (_, eq, _)
          when conv_builtin_p ctx eq "Eq.eq" ||
               conv_builtin_p ctx eq "Quotient.eq"
          -> Some (fx, rest)
        | _ -> None)
  | _ -> None

and eq_uneq_whnf ctx args =
  match args with
  | _l :: _t :: (_, x) :: (_, y) :: _p :: (_, i) :: rest
    -> if conv_p ctx i DB.interval_i0
          then Some (x, rest)
       else if conv_p ctx i DB.interval_i1
          then Some (y, rest)
       else None
  | _ -> None

and quotient_elim_whnf ctx args =
  match args with
  | _l1 :: _l2 :: _l3 :: _A :: _R :: _P :: (_, f) :: (_, _p) :: (_, q) :: rest
    -> (match lexp'_whnf q ctx with
        | Call (_, qin, [_; _; _; _; (_, e)])
          when conv_builtin_p ctx qin "Quotient.in"
          -> Some (mkCall (dsinfo, f, [Anormal, e]), rest)
        | _ -> None)
  | _ -> None

and register_reducible_builtins () =
  reducible_builtins :=
    List.fold_right
      (fun (n, f) m -> SMap.add n f m) [
        ("Eq.cast", eq_cast_whnf);
        ("Eq.uneq", eq_uneq_whnf);
        ("Quotient.elim", quotient_elim_whnf)
      ] !reducible_builtins

(** A very naive implementation of sets of pairs of lexps.  *)
and set_empty : set_plexp = []
and set_member_p (s : set_plexp) (e1 : lexp) (e2 : lexp) : bool
  = try let _ = List.find (fun (e1', e2')
                           -> L.eq e1 e1' && L.eq e2 e2')
                          s
        in true
     with Not_found -> false
and set_add (s : set_plexp) (e1 : lexp) (e2 : lexp) : set_plexp
  = (* assert (not (set_member_p s e1 e2)); *)
    ((e1, e2) :: s)
and set_shift_n (s : set_plexp) (n : db_offset)
  = List.map (let s = S.shift n in
              fun (e1, e2) -> (Lexp.push_susp e1 s, Lexp.push_susp e2 s))
             s
and set_shift s : set_plexp = set_shift_n s 1

(********* Testing if two types are "convertible" aka "equivalent"  *********)

(* Turn e (presumably of type TypeLevel) into a canonical representation,
 * which is basically the set of vars it references along with the number of
 * `succ` applied to them.
 * `c` is the maximum "constant" level that occurs in `e`
 * and `m` maps variable indices to the maxmimum depth at which they were
 * found.  *)
and level_canon e =
  let add_var_depth v d ((c,m) as acc) =
    let o = try IMap.find v m with Not_found -> -1 in
    if o < d then (c, IMap.add v d m) else acc in

  let rec canon e d ((c,m) as acc) =
    match lexp_lexp' e with
    | SortLevel SLz -> if c < d then (d, m) else acc
    | SortLevel (SLsucc e) -> canon e (d + 1) acc
    | SortLevel (SLlub (e1, e2)) -> canon e1 d (canon e2 d acc)
    (* FIXME: Apply substitutions from surrounding `Let` bindings.  *)
    | Var (_, i) -> add_var_depth i d acc
    | Metavar (i, s, _)
      -> (match metavar_lookup i with
         | MVal e -> canon (push_susp e s) d acc
         | _ -> add_var_depth (- i) d acc)
    | Susp (e, s) -> canon (push_susp e s) d acc
    | _ -> (max_int, m)
  in canon e 0 (0,IMap.empty)

and level_leq (c1, m1) (c2, m2) =
  c1 <= c2
  && c1 != max_int
  && IMap.for_all (fun i d -> try d <= IMap.find i m2 with Not_found -> false)
                 m1

(* Returns true if e₁ and e₂ are equal (upto alpha/beta/...).  *)
and conv_p' (ctx : lexp_context) (vs : set_plexp) e1 e2 : bool =
  let e1' = lexp_whnf e1 ctx in
  let e2' = lexp_whnf e2 ctx in
  e1' == e2' ||
    let changed = not (e1 == e1' && e2 == e2') in
    if changed && set_member_p vs e1' e2' then true else
    let vs' = if changed then set_add vs e1' e2' else vs in
    let conv_p = conv_p' ctx vs' in
    match (lexp_lexp' e1', lexp_lexp' e2') with
    | (Imm (_, v1), Imm (_, v2)) -> v1 = v2
    | (SortLevel sl1, SortLevel sl2)
      -> (match (sl1, sl2) with
         | (SLz, SLz) -> true
         | (SLsucc sl1, SLsucc sl2) -> conv_p sl1 sl2
         | _ -> let ce1 = level_canon e1' in
               let ce2 = level_canon e2' in
               level_leq ce1 ce2 && level_leq ce2 ce1)
    | (Sort (_, s1), Sort (_, s2))
      -> s1 == s2
        || (match (s1, s2) with
           | (Stype e1, Stype e2) -> conv_p e1 e2
           | _ -> false)
    | (Builtin ((_, s1), _), Builtin ((_, s2), _)) -> s1 = s2
    | (Var (_, v1), Var (_, v2)) -> v1 = v2
    | (Arrow (_, ak1, vd1, t11, t12), Arrow (_, ak2, _vd2, t21, t22))
      -> ak1 == ak2
        && conv_p t11 t21
        && conv_p' (DB.lexp_ctx_cons ctx vd1 Variable t11) (set_shift vs')
                  t12 (srename vd1 t22)
    | (Lambda (ak1, l1, t1, e1), Lambda (ak2, _l2, t2, e2))
      -> ak1 == ak2 && (conv_erase || conv_p t1 t2)
        && conv_p' (DB.lexp_ctx_cons ctx l1 Variable t1)
                  (set_shift vs')
                  e1 e2
    | (Call (_, f1, args1), Call (_, f2, args2))
      -> let conv_arglist_p args1 args2 : bool =
          List.fold_left2
            (fun eqp (ak1,t1) (ak2,t2)
             -> eqp && ak1 = ak2
               && (conv_erase && ak1 = Aerasable || conv_p t1 t2))
            true args1 args2 in
        List.length args1 == List.length args2
        && conv_p f1 f2 && conv_arglist_p args1 args2
    | (Inductive (_, l1, args1, cases1), Inductive (_, l2, args2, cases2))
      -> let rec conv_fields ctx vs fields1 fields2 =
          match fields1, fields2 with
          | ([], []) -> true
          | ((ak1,vd1,t1)::fields1, (ak2,_vd2,t2)::fields2)
            -> ak1 == ak2 && conv_p' ctx vs t1 t2
              && conv_fields (DB.lexp_ctx_cons ctx vd1 Variable t1)
                            (set_shift vs)
                            fields1 fields2
          | _,_ -> false in
        let rec conv_args ctx vs args1 args2 =
          match args1, args2 with
          | ([], []) ->
             (* Args checked alright, now go on with the fields,
              * using the new context.  *)
             SMap.equal (conv_fields ctx vs) cases1 cases2
          | ((ak1,l1,t1)::args1, (ak2,_l2,t2)::args2)
            -> ak1 == ak2 && conv_p' ctx vs t1 t2
              && conv_args (DB.lexp_ctx_cons ctx l1 Variable t1)
                          (set_shift vs)
                          args1 args2
          | _,_ -> false in
        l1 == l2 && conv_args ctx vs' args1 args2
    | (Cons (t1, (_, l1)), Cons (t2, (_, l2))) -> l1 = l2 && conv_p t1 t2
    | (Case (_, target1, r1, cases1, def1), Case (_, target2, r2, cases2, def2))
      -> (* FIXME The termination of this case conversion is very
            fragile. Indeed, we recurse in all branches, bypassing any
            termination condition. Checking syntactic equality as a
            base case seems to be sufficient in simple cases, but it
            is probably not enough in general. *)
       eq e1' e2' || (
        conv_p target1 target2 &&
          conv_p r1 r2 && (
          (* Compare the branches *)
          (* We can arbitrarily use target1 since target1 and target2
             are convertible *)
          let target = target1 in
          let etype = lexp_whnf (get_type ctx target) ctx in
          let ekind = get_type ctx etype in
          let elvl = match lexp'_whnf ekind ctx with
            | Sort (_, Stype l) -> l
            | _ -> Log.log_fatal ~loc:(Lexp.location ekind)
                     "Target lexp's kind is not a sort"; in
          (* 1. Get the inductive for the field types *)
          let it, aargs = match lexp_lexp' etype with
            | Call (_, f, args) -> (f, args)
            | _ -> (etype, []) in
          (* 2. Build the substitution for the inductive arguments *)
          let fargs, ctors =
            (match lexp'_whnf it ctx with
             | Inductive (_, _, fargs, constructors)
               -> fargs, constructors
             | _ -> Log.log_fatal ("Case of non-inductive in conv_p")) in
          let fargs_subst = List.fold_left2 (fun s _farg (_, aarg) -> S.cons aarg s)
                              S.identity fargs aargs in
          (* 3. Compare the branches *)
          let ctx_extend_with_eq ctx subst hlxp =
            let tlxp = mkSusp target subst in
            let tltp = mkSusp etype subst in
            let tlvl = mkSusp elvl subst in
            let eqty = mkCall (dsinfo, DB.type_eq,
                         [(Aerasable, tlvl);   (* Typelevel *)
                          (Aerasable, tltp);   (* Inductive type *)
                          (Anormal, hlxp);     (* Lexp of the branch head *)
                          (Anormal, tlxp)]) in (* Target lexp *)
            DB.lexp_ctx_cons ctx (dsinfo, None) Variable eqty in
          (* The map module doesn't have a function to compare two
             maps with the key (which is needed to get the field types
             from the inductive. Instead, we work with the lists of
             associations. *)
          (try
             List.for_all2 (fun (l1, (_, fields1, e1)) (l2, (_, fields2, e2)) ->
                 l1 = l2 &&
                   let fieldtypes = SMap.find l1 ctors in
                   let rec mkctx ctx args s i vdefs1 vdefs2 fieldtypes =
                     match vdefs1, vdefs2, fieldtypes with
                     | [], [], [] -> Some (ctx, List.rev args, s)
                     | (ak1, vdef1)::vdefs1, (ak2, _vdef2)::vdefs2,
                       (ak', _vdef', ftype)::fieldtypes
                       -> if ak1 = ak2 && ak2 = ak' then
                            mkctx
                              (DB.lexp_ctx_cons ctx vdef1 Variable (mkSusp ftype s))
                              ((ak1, (mkVar (vdef1, i)))::args)
                              (ssink vdef1 s)
                              (i - 1)
                              vdefs1 vdefs2 fieldtypes
                          else None
                     | _,_,_ -> None in
                   match mkctx ctx [] fargs_subst (List.length fields1)
                           fields1 fields2 fieldtypes with
                   | None -> false
                   | Some (nctx, args, _subst) ->
                      let offset = (List.length fields1) in
                      let subst = S.shift offset in
                      let eaargs =
                        List.map (fun (_, a) -> (Aerasable, a)) aargs in
                      let ctor = mkSusp (mkCall (dsinfo, mkCons (it, (DB.dloc, l1)),
                                                 eaargs)) subst in
                      let hlxp = mkCall (dsinfo, ctor, args) in
                      let nctx = ctx_extend_with_eq nctx subst hlxp in
                      conv_p' nctx (set_shift_n vs' (offset + 1)) e1 e2
               ) (SMap.bindings cases1) (SMap.bindings cases2)
           with
           | Invalid_argument _ -> false (* If the lists have different length *)
          )
          && (match (def1, def2) with
              | (Some (v1, e1), Some (_v2, e2)) ->
                 let nctx = DB.lctx_extend ctx v1 Variable etype in
                 let subst = S.shift 1 in
                 let hlxp = mkVar ((dsinfo, None), 0) in
                 let nctx = ctx_extend_with_eq nctx subst hlxp in
                 conv_p' nctx (set_shift_n vs' 2) e1 e2
              | None, None -> true
              | _, _ -> false)))
    | (Metavar (id1,s1,_), Metavar (id2,s2,_)) when id1 == id2 ->
       (* FIXME Should we use conversion on the terms of the
          substitution instead of syntactic equality? *)
       subst_eq s1 s2
    | (Lambda (a, l, t, _) as e', e'') | (e'', (Lambda (a, l, t, _) as e'))
      (* Eta expansion of functions *)
       -> let expansion = mkLambda (a, l, t,
                                    mkCall (dsinfo, push_susp (hc e'') (S.shift 1),
                                            [(a, mkVar ((dsinfo, None), 0))])) in
          conv_p' ctx vs (hc e') expansion
    | (_, _) -> false

and conv_p (ctx : lexp_context) e1 e2
  = if e1 == e2 then true
    else conv_p' ctx set_empty e1 e2

and mk_eq_witness sinfo e ctx =
  let etype = get_type ctx e in (* FIXME: we should not need get_type here.  *)
  let elevel = match lexp'_whnf (get_type ctx etype) ctx with
    | Sort (_, Stype l) -> l
    | _ -> Log.internal_error "" in
  (* FIXME: Doesn't `e` need a "shift" here?  *)
  let fn = mkLambda (Aerasable, (sinfo, None), etype, e) in
  mkCall (sinfo, get_builtin ctx "Eq.eq",
          [Aerasable, elevel;
           Aerasable, etype;
           Anormal, fn])

(********* Testing if a lexp is properly typed  *********)

and mkSLlub ctx e1 e2 =
  let lwhnf1 = lexp_whnf e1 ctx in
  let lwhnf2 = lexp_whnf e2 ctx in
  match (lexp_lexp' lwhnf1, lexp_lexp' lwhnf2) with
  | (SortLevel SLz, _) -> e2
  | (_, SortLevel SLz) -> e1
  | (SortLevel (SLsucc e1), SortLevel (SLsucc e2))
    -> mkSortLevel (SLsucc (mkSLlub ctx e1 e2))
  | (_e1', _e2')
    -> let ce1 = level_canon lwhnf1 in
      let ce2 = level_canon lwhnf2 in
      if level_leq ce1 ce2 then e2
      else if level_leq ce2 ce1 then e1
      else mkSortLevel (mkSLlub' (e1, e2)) (* FIXME: Could be more canonical *)

and sort_compose ctx1 ctx2 l ak k1 k2 =
  (* BEWARE!  Technically `k2` can refer to `v`, but this should only happen
   * if `v` is a TypeLevel.  *)
 let lwhnf1 = lexp'_whnf k1 ctx1 in
 let lwhnf2 = lexp'_whnf k2 ctx2 in
  match (lwhnf1, lwhnf2) with
  | (Sort (_, s1), Sort (_, s2))
    -> (match s1, s2 with
       | (Stype l1, Stype l2)
         -> if ak == Aerasable && impredicative_erase
           then SortResult (mkSusp k2 (S.substitute impossible))
           else let l2' = (mkSusp l2 (S.substitute impossible)) in
                SortResult (mkSort (l, Stype (mkSLlub ctx1 l1 l2')))
       | (StypeLevel, Stype _l2)
            when ak == Aerasable && impredicative_universe_poly
         (* The safety/soundness of this rule is completely unknown.
          * It's pretty powerful, e.g. allows tuples containing
          * level-polymorphic functions, and makes impredicative-encoding
          * of data types almost(just?) as flexible as inductive types.  *)
         -> SortResult (mkSusp k2 (S.substitute DB.level0))
       | (StypeLevel, Stype _)
         | (StypeLevel, StypeOmega)
         (* This might be safe, but I don't think it adds much power.
          * It would add some flexibility, especially w.r.t generalized
          * arguments, but let's not bother for now: it's easier to add it
          * later than to remove it later.
          * | (Stype _, StypeOmega) *)
         -> SortResult (mkSort (l, StypeOmega))
       | _ -> SortInvalid)
  | (Sort (_, _), _) -> SortK2NotType
  | (_, _) -> SortK1NotType

and dbset_push ak erased =
  let nerased = DB.set_sink 1 erased in
  if ak = Aerasable then DB.set_set 0 nerased else nerased

and nerased_let defs erased =
  (* Let bindings are not erasable, with the important exception of
   * let-bindings of the form `x = y` where `y` is an erasable var.
   * This exception is designed so that macros like `case` which need to
   * rebind variables to user-specified names can do so without having
   * to pay attention to whether the var is erasable or not.
   *)
  (* FIXME: Maybe allow more cases of erasable let-bindings.  *)
  let nerased = DB.set_sink (List.length defs) erased in
  let es = List.map
             (fun (_v, e, _t) ->
               (* Look for `x = y` where `y` is an erasable var.
                * FIXME: `nerased` assumes all the vars in `defs`
                * will be non-erasable, so in `let x = z; y = x ...`
                * where `z` is erasable, `x` will be found
                * to be erasable, but not `y`.  *)
               match lexp_lexp' e with Var (_, idx) -> DB.set_mem idx nerased
                          | _ -> false)
             defs in
  if not (List.mem true es) then nerased else
    List.fold_left
      (fun erased e
       -> dbset_push (if e then Aerasable else Anormal) erased)
      erased es

(* "check ctx e" should return τ when "Δ ⊢ e : τ"  *)
and check'' erased ctx e =
  let check = check'' in
  let assert_type ctx e t t' =
    if conv_p ctx t t' then ()
    else
      log_tc_error
        ~loc:(Lexp.location e)
        ("@[<v>Type mismatch for:"
         ^^ "@,  @[<hov 2>%a@]"
         ^^ "@,Type:"
         ^^ "@,  @[<hov 2>%a@]"
         ^^ "@,is not equal to:"
         ^^ "@,  @[<hov 2>%a@]"
         ^^ "@]")
        pp_print_clean_lexp e
        pp_print_clean_lexp t
        pp_print_clean_lexp t'
  in
  let check_type erased ctx t =
    let s = check erased ctx t in
    (match lexp'_whnf s ctx with
     | Sort _ -> ()
     | _
       -> let loc = Lexp.location t in
          log_tc_error
            ~loc
            "@[<v>Expression:@,  @[<hov 2>%a@]@,is not a proper type.@]"
            pp_print_clean_lexp t);
    s in
  match lexp_lexp' e with
  | Imm (_, Vfloat _) -> DB.type_float
  | Imm (_, Vinteger _) -> DB.type_integer
  | Imm (_, Vstring _) -> DB.type_string
  | Imm _
    -> (log_tc_error ~loc:(Lexp.location e) "Unsupported immediate value!";
        type_dummy)
  | SortLevel SLz -> DB.type_level
  | SortLevel (SLsucc e)
    -> let t = check erased ctx e in
      (* FIXME: Actually, we should probably have a special function to check
       * that `e` is a level, so as to avoid `case` and other funny things.  *)
      assert_type ctx e t DB.type_level;
      DB.type_level
  | SortLevel (SLlub (e1, e2))
    -> let t1 = check erased ctx e1 in
      assert_type ctx e1 t1 DB.type_level;
      let t2 = check erased ctx e2 in
      assert_type ctx e2 t2 DB.type_level;
      DB.type_level
  | Sort (l, Stype e)
    -> let t = check erased ctx e in
      assert_type ctx e t DB.type_level;
      mkSort (l, Stype (mkSortLevel (SLsucc e)))
  | Sort (_, StypeLevel) -> DB.sort_omega
  | Sort (_, StypeOmega)
    -> ((* error_tc ~loc:(Lexp.location e) "Reached unreachable sort!";
        * Log.internal_error "Reached unreachable sort!"; *)
       DB.sort_omega)
  | Builtin (_, t)
    -> let _ = check_type erased ctx t in
      t
  (* FIXME: Check recursive references.  *)
  | Var (((loc, name), idx) as v)
    -> if DB.set_mem idx erased then
        log_tc_error
          ~loc:(sexp_location loc)
          {|Var `%s` can't be used here, because it's erasable|}
          (U.maybename name) ;
       lookup_type ctx v
  | Susp (e, s) -> check erased ctx (push_susp e s)
  | Let (l, defs, e)
    -> let _ =
        List.fold_left (fun ctx (v, _e, t)
                        -> (let _ = check_type DB.set_empty ctx t in
                           DB.lctx_extend ctx v ForwardRef t))
                       ctx defs in
      let nerased = nerased_let defs erased in
      let nctx = DB.lctx_extend_rec ctx defs in
      (* FIXME: Termination checking!  Positivity-checker!  *)
      let _ = List.fold_left (fun n (_v, e, t)
                              -> assert_type
                                  nctx e
                                  (check (if DB.set_mem (n - 1) nerased
                                          then DB.set_empty
                                          else nerased)
                                         nctx e)
                                  (push_susp t (S.shift n));
                                n - 1)
                             (List.length defs) defs in
      mkSusp (check nerased nctx e)
             (lexp_defs_subst l S.identity defs)
  | Arrow (loc, ak, v, t1, t2)
    -> (let k1 = check_type erased ctx t1 in
        let nctx = DB.lexp_ctx_cons ctx v Variable t1 in
        let k2 = check_type (DB.set_sink 1 erased) nctx t2 in
        match sort_compose ctx nctx loc ak k1 k2 with
        | SortResult k -> k
        | SortInvalid
          -> log_tc_error ~loc:(sexp_location loc) "Invalid arrow: inner TypelLevel argument";
             mkSort (loc, StypeOmega)
        | SortK1NotType
          -> log_tc_error ~loc:(Lexp.location t1) "Not a proper type";
             mkSort (loc, StypeOmega)
        | SortK2NotType
          -> log_tc_error ~loc:(Lexp.location t2) "Not a proper type";
             mkSort (loc, StypeOmega))
  | Lambda (ak, ((l,_) as v), t, e)
    -> (let _k = check_type DB.set_empty ctx t in
       mkArrow (l, ak, v, t,
                check (dbset_push ak erased)
                      (DB.lctx_extend ctx v Variable t)
                      e))
  | Call (_, f, args)
    -> let ft = check erased ctx f in
      List.fold_left
        (fun ft (ak,arg)
         -> let at = check (if ak = Aerasable then DB.set_empty else erased)
                           ctx arg in
            match lexp'_whnf ft ctx with
            | Arrow (_l, ak', _v, t1, t2)
              -> if ak != ak'
                 then log_tc_error ~loc:(Lexp.location arg) "arg kind mismatch";
                 assert_type ctx arg at t1;
                 mkSusp t2 (S.substitute arg)
            | _ -> log_tc_error
                     ~loc:(Lexp.location arg)
                     "@[<v>Calling a non function, of type:@,  @[<hov 2>%a@]@]"
                     pp_print_clean_lexp ft;
                   ft)
        ft args
  | Inductive (l, _label, args, cases)
    -> let rec arg_loop ctx erased args =
        match args with
        | []
          -> let level
              = SMap.fold
                  (fun _ case level ->
                    let (level, _, _, _) =
                      List.fold_left
                        (fun (level, ictx, erased, subst) (ak, v, t) ->
                          let lwhnf = lexp_whnf
                                        (check_type erased ictx t) ictx in
                          ((match lexp_lexp' lwhnf with
                            | Sort (_, Stype _)
                                 when ak == Aerasable && impredicative_erase
                              -> level
                            | Sort (_, Stype level')
                              -> mkSLlub ctx level
                                  (mkSusp level' subst)
                            | Sort (_, StypeLevel)
                              -> (if not(ak == Aerasable
                                      && impredicative_universe_poly)
                                 then log_tc_error
                                        ~loc:(Lexp.location t)
                                        ~print_action:(fun _ -> ())
                                        "@[<v>Field of type:@,  @[<hov 2>%a@]@,is not allowed.@]"
                                        pp_print_clean_lexp t);
                                level
                            | _tt
                              -> log_tc_error
                                   ~loc:(Lexp.location t)
                                   ~print_action:(fun _ ->
                                     DB.print_lexp_ctx ictx; print_newline ())
                                   "@[<v>Field type:@,  @[<hov 2>%a@]@,is not a Type:@,  @[<hov 2>%a@]@]"
                                   pp_print_clean_lexp t
                                   pp_print_clean_lexp lwhnf;
                                 level),
                           DB.lctx_extend ictx v Variable t,
                           DB.set_sink 1 erased,
                           (* The final type(level) cannot refer
                            * to the fields!  *)
                           S.cons
                             (match lexp_lexp' lwhnf with
                              (* The non-erasable case is tested above!  *)
                              | Sort (_, StypeLevel) -> DB.level0
                              | _ -> impossible)
                             subst))
                        (level, ctx, erased, S.identity)
                        case in
                    level)
                  cases (mkSortLevel SLz) in
            mkSort (l, Stype level)
        | (ak, v, t)::args
          -> let _k = check_type DB.set_empty ctx t in
            mkArrow (lexp_sinfo t, ak, v, t,
                     arg_loop (DB.lctx_extend ctx v Variable t)
                              (dbset_push ak erased)
                              args) in
      let tct = arg_loop ctx erased args in
      tct
  | Proj (l, e, (loc, label))
    -> ( let call_split e =
        match lexp_lexp' e with
        | Call (_l, f, args) -> (f, args)
        | _ -> (e,[]) in
      let etype = lexp_whnf (check erased ctx e) ctx in
      let it, aargs = call_split etype in
      match lexp'_whnf it ctx, aargs with
        | Inductive (_, _, fargs, constructors), aargs
            when SMap.cardinal constructors = 1
          -> let rec mksubst s fargs aargs =
              match fargs, aargs with
              | [], [] -> s
              | _farg::fargs, (_ak, aarg)::aargs
                -> mksubst (S.cons aarg s) fargs aargs
              | _,_ -> (log_tc_error ~loc:(sexp_location l)
                        "Wrong arg number to inductive type!"; s) in
            let s = mksubst S.identity fargs aargs in
            let (_,fieldtypes) = List.hd (SMap.bindings constructors) in
            let rec getfieldtype s (fieldtypes
                                    : (arg_kind * vname * ltype) list) =
              match fieldtypes with
              | [] -> log_tc_error ~loc:(sexp_location l) "Tuple has no field named: %s" label;
                     etype
              | (ak, (_, Some fn), ftype)::_ when fn = label
                (* We found our field! *)
                -> if not (ak = Aerasable)
                  then mkSusp ftype s (* Yay!  We found our field! *)
                  else (log_tc_error ~loc:(sexp_location l) "Can't Proj an erasable field: %s"
                          label;
                        Lexp.impossible)
              | (_, vdef, _)::fieldtypes
                -> let fvdef vdef e loc lbl =
                   match vdef with
                   |(_, Some _) -> mkProj (loc, e, lbl)
                   |_ -> Lexp.impossible in
                   let fieldref = fvdef vdef e l (loc, label) in
                   getfieldtype (S.cons fieldref s) fieldtypes in
                   getfieldtype s fieldtypes
        | Inductive _, _
          -> Log.log_error ~loc:(sexp_location l)
              "Proj on an inductive type that's not a tuple!";
            etype
        | _,_ -> Log.log_error ~loc:(sexp_location l) "Proj on a non-inductive type!" ; etype)

  | Case (l, e, ret, branches, default)
    (* FIXME: Check that the return type isn't TypeLevel.  *)
    -> let call_split e =
        match lexp_lexp' e with
        | Call (_l, f, args) -> (f, args)
        | _ -> (e,[]) in
       let etype = lexp_whnf (check erased ctx e) ctx in
       (* FIXME save the type in the case lexp instead of recomputing
          it over and over again *)
       let ekind = get_type ctx etype in
       let elvl = match lexp'_whnf ekind ctx with
         | Sort (_, Stype l) -> l
         | _ -> Log.log_error ~loc:(Lexp.location ekind)
                  "Target lexp's kind is not a sort"; DB.level0 in
      let it, aargs = call_split etype in
      (match lexp'_whnf it ctx, aargs with
       | Inductive (_, _, fargs, constructors), aargs ->
          let rec mksubst s fargs aargs =
            match fargs, aargs with
            | [], [] -> s
            | _farg::fargs, (_ak, aarg)::aargs
              (* We don't check aarg's type, because we assume that `check`
               * returns a valid type.  *)
              -> mksubst (S.cons aarg s) fargs aargs
            | _
              -> log_tc_error ~loc:(sexp_location l) "Wrong arg number to inductive type!";
                 s in
          let s = mksubst S.identity fargs aargs in
          let ctx_extend_with_eq ctx subst hlxp nerased =
            let tlxp = mkSusp e subst in
            let tltp = mkSusp etype subst in
            let tlvl = mkSusp elvl subst in
            let eqty = mkCall (l, DB.type_eq,
                         [(Aerasable, tlvl);   (* Typelevel *)
                          (Aerasable, tltp);   (* Inductive type *)
                          (Anormal, hlxp);     (* Lexp of the branch head *)
                          (Anormal, tlxp)]) in (* Target lexp *)
            (* The eq proof is erasable. *)
            let nerased = dbset_push Aerasable nerased in
            let nctx = DB.lexp_ctx_cons ctx (l, None) Variable eqty in
            (nerased, nctx) in
          SMap.iter
            (fun name (l, vdefs, branch)
             -> let fieldtypes = SMap.find name constructors in
               let rec mkctx erased ctx s hlxp vdefs fieldtypes =
                 match vdefs, fieldtypes with
                 | [], [] -> (erased, ctx, hlxp)
                 (* FIXME: If ak is Aerasable, make sure the var only
                  * appears in type annotations.  *)
                 | (ak, vdef)::vdefs, (_ak', _vdef', ftype)::fieldtypes
                   -> mkctx (dbset_push ak erased)
                           (DB.lexp_ctx_cons ctx vdef Variable (mkSusp ftype s))
                           (ssink vdef s)
                           (mkCall (l, mkSusp hlxp (S.shift 1), [(ak, mkVar (vdef, 0))]))
                           vdefs fieldtypes
                 | _
                   -> log_tc_error ~loc:(sexp_location l) "Wrong number of args to constructor!";
                      (erased, ctx, hlxp) in
               let hctor =
                 mkCall (l, mkCons (it, (sexp_location l, name)),
                         List.map (fun (_, a) -> (Aerasable, a)) aargs) in
               let (nerased, nctx, hlxp) =
                 mkctx erased ctx s hctor vdefs fieldtypes in
               let subst = S.shift (List.length vdefs) in
               let (nerased, nctx) = ctx_extend_with_eq nctx subst hlxp nerased in
               assert_type nctx branch
                           (check nerased nctx branch)
                           (mkSusp ret (S.shift ((List.length fieldtypes) + 1))))
            branches;
          let diff = SMap.cardinal constructors - SMap.cardinal branches in
          (match default with
           | Some (v, d)
             -> if diff <= 0
                then log_tc_warning ~loc:(sexp_location l) "Redundant default clause";
               let nctx = (DB.lctx_extend ctx v (LetDef (0, e)) etype) in
               let nerased = DB.set_sink 1 erased in
               let subst = S.shift 1 in
               let hlxp = mkVar ((l, None), 0) in
               let (nerased, nctx) =
                 ctx_extend_with_eq nctx subst hlxp nerased in
               assert_type nctx d (check nerased nctx d)
                           (mkSusp ret (S.shift 2))
           | None
             -> if diff > 0
                then
                  log_tc_error
                    ~loc:(sexp_location l) "Non-exhaustive match: %d cases missing" diff)
       | _,_ -> log_tc_error ~loc:(sexp_location l) "Case on a non-inductive type!");
      ret
  | Cons (t, (_l, name))
    -> (match lexp'_whnf t ctx with
       | Inductive (l, _, fargs, constructors)
         -> (try
              let fieldtypes = SMap.find name constructors in
              let rec indtype fargs start_index =
                match fargs with
                | [] -> []
                | (ak, vd, _)::fargs -> (ak, mkVar (vd, start_index))
                                       :: indtype fargs (start_index - 1) in
              let rec fieldargs ftypes =
                match ftypes with
                | [] -> let nargs = List.length fieldtypes + List.length fargs in
                       mkCall (l, mkSusp t (S.shift nargs),
                               indtype fargs (nargs - 1))
                | (ak, vd, ftype) :: ftypes
                  -> mkArrow (lexp_sinfo ftype, ak, vd, ftype,
                             fieldargs ftypes) in
              let rec buildtype fargs =
                match fargs with
                | [] -> fieldargs fieldtypes
                | (_ak, ((l,_) as vd), atype) :: fargs
                  -> mkArrow (l, Aerasable, vd, atype,
                             buildtype fargs) in
              buildtype fargs
             with
             | Not_found
               -> log_tc_error ~loc:(sexp_location l) {|Constructor "%s" does not exist|} name;
                  type_dummy)
       | _ -> log_tc_error
                ~loc:(Lexp.location e)
                "@[<v>Cons of a non-inductive type:@,  @[<hov 2>%a@]@]"
                pp_print_clean_lexp t;
              type_dummy)
  | Metavar (idx, s, _)
    -> (match metavar_lookup idx with
       | MVal e -> let e = push_susp e s in
                  check erased ctx e
       | MVar (_, t, _) -> push_susp t s)

and check' ctx e =
  let res = check'' DB.set_empty ctx e in
    (Log.stop_on_error (); res)

and check ctx e = check' ctx e

(** Compute the set of free (meta)variables.  **)

and list_union l1 l2 = match l1 with
  | [] -> l2
  | (x::l1) -> list_union l1 (if List.mem x l2 then l2 else (x::l2))

and mv_set_empty : mv_set = (IMap.empty, IMap.empty)
and mv_set_add (ms, nes) id x : mv_set = (IMap.add id x ms, IMap.add id () nes)
and mv_set_union ((ms1, nes1) : mv_set) ((ms2, nes2) : mv_set) : mv_set
  = (IMap.merge (fun _m oss1 oss2
                -> match (oss1, oss2) with
                  | (None, _) -> oss2
                  | (_, None) -> oss1
                  | (Some ss1, Some ss2)
                    -> (let ((sl1, t1, len1, (_, name1)),
                            (sl2, t2, len2, (_, name2)))
                         = (ss1, ss2) in
                       assert (sl1 = sl2);
                       assert (t1 = t2);
                       assert (len1 = len2);
                       assert (name2 = name1));
                      Some ss1)
                ms1 ms2,
     IMap.merge (fun _m _o1 _o2 -> Some ()) nes1 nes2)
and mv_set_erase (ms, _nes) = (ms, IMap.empty)

and fv_memo = LMap.create 1000

and fv_empty = (DB.set_empty, mv_set_empty)
and fv_union (fv1, mv1) (fv2, mv2)
  = (DB.set_union fv1 fv2, mv_set_union mv1 mv2)
and fv_sink n (fvs, mvs) = (DB.set_sink n fvs, mvs)
and fv_hoist n (fvs, mvs) = (DB.set_hoist n fvs, mvs)
and fv_erase (fvs, mvs) = (fvs, mv_set_erase mvs)

and fv (e : lexp) : (db_set * mv_set) =
  let fv' e = match lexp_lexp' e with
    | Imm _ -> fv_empty
    | SortLevel SLz -> fv_empty
    | SortLevel (SLsucc e) -> fv e
    | SortLevel (SLlub (e1, e2)) -> fv_union (fv e1) (fv e2)
    | Sort (_, Stype e) -> fv e
    | Sort (_, (StypeOmega | StypeLevel)) -> fv_empty
    | Builtin (_, t) -> fv t
    | Var (_, i) -> (DB.set_singleton i, mv_set_empty)
    | Proj (_, lxp, _) -> fv lxp
    | Susp (e, s) -> fv (push_susp e s)
    | Let (_, defs, e)
      -> let len = List.length defs in
        let (fvs, _)
          = List.fold_left (fun (fvs, o) (_, e, t)
                            -> (fv_union fvs (fv_union (fv_erase
                                                         (fv_sink o (fv t)))
                                                      (fv e)),
                               o - 1))
                           (fv e, len) defs in
        fv_hoist len fvs
    | Arrow (_, _, _, t1, t2) -> fv_union (fv t1) (fv_hoist 1 (fv t2))
    | Lambda (_, _, t, e) -> fv_union (fv_erase (fv t)) (fv_hoist 1 (fv e))
    | Call (_, f, args)
      -> List.fold_left (fun fvs (ak, arg)
                        -> let afvs = fv arg in
                          fv_union fvs
                                   (if ak = Aerasable
                                    then fv_erase afvs
                                    else afvs))
                       (fv f) args
    | Inductive (_, _, args, cases)
      -> let alen = List.length args in
        let s
          = List.fold_left (fun s (_, _, t)
                            -> fv_sink 1 (fv_union s (fv t)))
                           fv_empty args in
        let s
          = SMap.fold
              (fun _ fields s
               -> fv_union
                   s
                   (fv_hoist (List.length fields)
                             (List.fold_left (fun s (_, _, t)
                                              -> fv_sink 1 (fv_union s (fv t)))
                                             fv_empty fields)))
              cases s in
        fv_hoist alen s
    | Cons (t, _) -> fv_erase (fv t)
    | Case (_, e, t, cases, def)
      -> let s = fv_union (fv e) (fv_erase (fv t)) in
        let s = match def with
          | None -> s
          | Some (_, e) -> fv_union s (fv_hoist 2 (fv e)) in
        SMap.fold (fun _ (_, fields, e) s
                   -> fv_union s (fv_hoist (List.length fields + 1) (fv e)))
                  cases s
    | Metavar (id, s, name)
      -> (match metavar_lookup id with
         | MVal e -> fv (push_susp e s)
         | MVar (sl, t, cl)
           -> let (fvs, mvs) = fv_erase (fv (push_susp t s)) in
             (fvs, mv_set_add mvs id (sl, t, cl, name)))
  in
  let ofvs = LMap.find_opt fv_memo e in
  (* The memoized value depends on the metavariable table. When it
     changes, we have to recompute the free variables. Instead of
     flushing the table after every instantiation, we check that no
     free metavars are actually instantiated. For this to work, we
     assume that the table changes monotonously: metavariables can be
     instantiated, but the instantiations cannot be undone. *)
  match ofvs with
  | Some ((_, (mvfs, _)) as fvs)
       when IMap.for_all
              (fun mv _ -> match metavar_lookup mv with
                           | MVar _ -> true
                           | MVal _ -> false)
              mvfs
    -> fvs
  | _ -> let r = fv' e in
         LMap.add fv_memo e r;
         r

(** Finding the type of a expression.  **)
(* This should never signal any warning/error.  *)

and get_type ctx e =
  match lexp_lexp' e with
  | Imm (_, Vfloat _) -> DB.type_float
  | Imm (_, Vinteger _) -> DB.type_integer
  | Imm (_, Vstring _) -> DB.type_string
  | Imm _ -> type_dummy
  | Builtin (_, t) -> t
  | SortLevel _ -> DB.type_level
  | Sort (l, Stype e) -> mkSort (l, Stype (mkSortLevel (mkSLsucc e)))
  | Sort (_, StypeLevel) -> DB.sort_omega
  | Sort (_, StypeOmega) -> DB.sort_omega
  | Var (((_, _name), _idx) as v) -> lookup_type ctx v
  | Proj (l, e, (loc, label))
            -> (

              let call_split e =
                match lexp_lexp' e with
                | Call (_l, f, args) -> (f, args)
                | _ -> (e,[]) in
              let etype = lexp_whnf (get_type ctx e) ctx in
              let it, aargs = call_split etype in
              match lexp'_whnf it ctx, aargs with
                | Inductive (_, _, fargs, constructors), aargs
                    when SMap.cardinal constructors = 1
                  -> let rec mksubst s fargs aargs =
                      match fargs, aargs with
                      | [], [] -> s
                      | _farg::fargs, (_ak, aarg)::aargs
                        -> mksubst (S.cons aarg s) fargs aargs
                      | _,_ -> (log_tc_error ~loc:(sexp_location l)
                                "Wrong arg number to inductive type!"; s) in
                    let s = mksubst S.identity fargs aargs in
                    let (_,fieldtypes) = List.hd (SMap.bindings constructors) in
                    let rec getfieldtype s (fieldtypes
                                            : (arg_kind * vname * ltype) list) =
                      match fieldtypes with
                      | [] -> log_tc_error ~loc:(sexp_location l) "Tuple has no field named: %s"
                               label;
                             etype
                      | (ak, (_, Some fn), ftype)::_ when fn = label
                        (* We found our field! *)
                        -> if not (ak = Aerasable)
                          then mkSusp ftype s (* Yay!  We found our field! *)
                          else (log_tc_error ~loc:(sexp_location l)
                                  "Can't Proj an erasable field: %s" label;
                                Lexp.impossible)
                      | (_, vdef, _)::fieldtypes
                        ->  let fvdef vdef e loc lbl =
                            match vdef with
                            |(_, Some _) -> mkProj (loc, e, lbl)
                            |_ -> Lexp.impossible in
                            let fieldref = fvdef vdef e l (loc, label) in
                            getfieldtype (S.cons fieldref s) fieldtypes in
                            getfieldtype s fieldtypes
                | Inductive _, _
                  -> Log.log_error ~loc:(sexp_location l)
                      "Proj on an inductive type that's not a tuple!";
                    etype
                | _,_ -> Log.log_error ~loc:(sexp_location l) "Proj on a non-inductive type!" ;
                        etype)
  | Susp (e, s) -> get_type ctx (push_susp e s)
  | Let (l, defs, e)
    -> let nctx = DB.lctx_extend_rec ctx defs in
      mkSusp (get_type nctx e) (lexp_defs_subst l S.identity defs)
  | Arrow (l, ak, v, t1, t2)
    (* FIXME: Use `check` here but silencing errors?  *)
    -> (let k1 = get_type ctx t1 in
       let nctx = DB.lexp_ctx_cons ctx v Variable t1 in
       let k2 = get_type nctx t2 in
       match sort_compose ctx nctx l ak k1 k2 with
       | SortResult k -> k
       | _ -> mkSort (l, StypeOmega))
  | Lambda (ak, ((l,_) as v), t, e)
    -> (mkArrow (l, ak, v, t,
                get_type (DB.lctx_extend ctx v Variable t)
                         e))
  | Call (_l, f, args)
    -> let ft = get_type ctx f in
      List.fold_left
        (fun ft (_ak,arg)
         -> match lexp'_whnf ft ctx with
           | Arrow (_l, _ak', _v, _t1, t2)
             -> mkSusp t2 (S.substitute arg)
           | _ -> ft)
        ft args
  | Inductive (l, _label, args, cases)
    (* FIXME: Use `check` here but silencing errors?  *)
    -> let rec arg_loop args ctx =
        match args with
        | []
          -> let level
              = SMap.fold
                  (fun _ case level ->
                    let (level, _, _) =
                      List.fold_left
                        (fun (level, ictx, subst) (ak, v, t) ->
                          let s = lexp'_whnf (get_type ictx t) ictx in
                          ((match s with
                            | Sort (_, Stype _)
                                 when ak == Aerasable && impredicative_erase
                              -> level
                            | Sort (_, Stype level')
                              -> mkSLlub ctx level
                                  (mkSusp level' subst)
                            | _tt -> level),
                           DB.lctx_extend ictx v Variable t,
                           (* The final type(level) cannot refer
                            * to the fields!  *)
                           S.cons
                             (match s with
                              | Sort (_, StypeLevel) -> DB.level0
                              | _ -> impossible)
                             subst))
                        (level, ctx, S.identity)
                        case in
                    level)
                  cases (mkSortLevel SLz) in
            mkSort (l, Stype level)
        | (ak, v, t)::args
          -> mkArrow (lexp_sinfo t, ak, v, t,
                     arg_loop args (DB.lctx_extend ctx v Variable t)) in
      let tct = arg_loop args ctx in
      tct
  | Case (_l, _e, ret, _branches, _default) -> ret
  | Cons (t, (_l, name))
    -> (match lexp'_whnf t ctx with
       | Inductive (l, _, fargs, constructors)
         -> (try
              let fieldtypes = SMap.find name constructors in
              let rec indtype fargs start_index =
                match fargs with
                | [] -> []
                | (ak, vd, _)::fargs -> (ak, mkVar (vd, start_index))
                                       :: indtype fargs (start_index - 1) in
              let rec fieldargs ftypes =
                match ftypes with
                | [] -> let nargs = List.length fieldtypes + List.length fargs in
                       mkCall (l, mkSusp t (S.shift nargs),
                               indtype fargs (nargs - 1))
                | (ak, vd, ftype) :: ftypes
                  -> mkArrow (lexp_sinfo ftype, ak, vd, ftype,
                             fieldargs ftypes) in
              let rec buildtype fargs =
                match fargs with
                | [] -> fieldargs fieldtypes
                | (_ak, ((l,_) as vd), atype) :: fargs
                  -> mkArrow (l, Aerasable, vd, atype,
                             buildtype fargs) in
              buildtype fargs
            with Not_found -> type_dummy)
       | _ -> type_dummy)
  | Metavar (idx, s, _)
    -> (match metavar_lookup idx with
       | MVal e -> get_type ctx (push_susp e s)
       | MVar (_, t, _) -> push_susp t s)

(* FIXME: Remove the mutual recursion between `lexp_whnf` and
   `get_type`, and move this closer to the `lexp_whnf`. *)
let _ = register_reducible_builtins ()

(*********** Type erasure, before evaluation.  *****************)

(* References to erasable bindings shouldn't appear in the bodies of
   lexps that are being erased, but we need to adjust the debruijn
   indices of other vars, hence we substitute erasable variables with
   a dummy.  *)
let erasure_dummy = DB.type0

let arity_of_cons
      (lctx : lexp_context)
      (ty : lexp)
      (location, name : symbol)
    : int =

  let count_unless_erasable i = function
    | (Aerasable, _, _) -> i
    | _ -> i + 1
  in

  match lexp'_whnf ty lctx with
  | Inductive (_, _, _, constructors)
    -> (match SMap.find_opt name constructors with
        | Some params -> List.fold_left count_unless_erasable 0 params
        | None -> error ~location "invalid constructor: %s" name)
  | _
    -> error
         ~location:(Lexp.location ty)
         ("@[<v>Can't deduce arity of constructor `%s`,"
          ^^ "@,because it is not an inductive type:"
          ^^ "@,  @[<hov 2>%a@]@]")
         name
         pp_print_clean_lexp ty


let pos_of_label lctx label e : int =
  let call_split e =
    match lexp_lexp' e with
    | Call (_, f, args) -> (f, args)
    | _ -> (e,[]) in

   let it, aargs = call_split (lexp_whnf e lctx) in
  match lexp'_whnf it lctx, aargs with
    | Inductive (_, _, _, constructors), _
        when SMap.cardinal constructors = 1
      -> let (_, st) = label in
         (match SMap.bindings constructors with
          | [(_,l)] -> let rec find_index
                                label
                                (l : (arg_kind * vname * ltype) list)
                                (c: int) : int =
                        match l with
                          | []
                            -> let (loc,st) = label in
                              Log.log_error ~loc:loc "Label Not Found: %s" st;
                              0
                          | (_, (_, Some vn), _ )::_ when (String.equal vn st)
                            -> c
                          | _::tl -> find_index label tl (c + 1)
                        in
                        find_index label l 0
             | _ -> let (loc, _) = label in
                   Log.log_fatal ~loc:loc "Wrong arg number to inductive type!")
    | Inductive _, _
      -> let (loc,_) = label in
        Log.log_error ~loc:loc "Proj on an inductive type that's not a tuple!";
        0
    | _,_ -> let (loc,_) = label in
            Log.log_error ~loc:loc "@[<v>Proj on a non-inductive type:@,  @[<hov 2>%a@]@]"
              pp_print_clean_lexp it;
            0


let rec erase_type (lctx : lexp_context) (lxp: lexp) : elexp =
  match lexp_lexp' lxp with
  | Imm (l, s)     -> Eimm (sexp_location l, s)
  | Builtin (v, _) -> Ebuiltin (v)
  | Var (v)        -> Evar (v)
  | Proj (l, exp, label)
    -> let t = get_type lctx exp in
      Eproj (sexp_location l, erase_type lctx exp, pos_of_label lctx label t)
  | Cons (ty, s)   -> Econs (arity_of_cons lctx ty s, s)

  | Lambda (Aerasable, _, _, body)
    -> erase_type lctx (L.push_susp body (S.substitute erasure_dummy))

  | Lambda (_, vdef, ty, body)
    -> let lctx' = DB.lctx_extend lctx vdef Variable ty in
       Elambda (vdef, erase_type lctx' body)

  | Let (l, decls, body)
    -> let lctx', edecls = clean_decls lctx decls in
       Elet (sexp_location l, edecls, erase_type lctx' body)

  | Call (_, fct, args)
    -> Ecall (erase_type lctx fct, List.filter_map (clean_arg lctx) args)

  | Case (location, target, _, branches, default)
    -> let etarget = erase_type lctx target in
       let ebranches = clean_branch_map lctx branches in
       Ecase (sexp_location location, etarget, ebranches, clean_default lctx default)

  | Susp (l, s) -> erase_type lctx (L.push_susp l s)

  (* To be thrown out *)
  | Arrow _     -> Etype lxp
  | SortLevel _ -> Etype lxp
  | Sort _      -> Etype lxp

  (* Still useful to some extent.  *)
  | Inductive (_, _, _, _) -> Etype lxp
  | Metavar (idx, s, _)
    -> (match metavar_lookup idx with
        | MVal e -> erase_type lctx (push_susp e s)
        | MVar _ -> Log.internal_error "Metavar in erase_type")

and clean_arg lctx = function
  | (Aerasable, _) -> None
  | (_, lexp) -> Some (erase_type lctx lexp)

and clean_decls
      (lctx : lexp_context)
      (decls : ldecl list)
    : lexp_context * (vname * elexp) list =
  let lctx' = DB.lctx_extend_rec lctx decls in
  (lctx', List.map (fun (v, lexp, _) -> (v, erase_type lctx' lexp)) decls)

and clean_default lctx lxp =
  match lxp with
  | Some (v, lxp)
    -> let lxp' = L.push_susp lxp (S.substitute erasure_dummy) in
       Some (v, erase_type lctx lxp')
  | None -> None

and clean_branch_map lctx cases =
  let clean_branch (l, args, expr) =
    let rec clean_arg_list args acc subst lctx =
      match args with
      | (Aerasable, _) :: tl
        -> (* Drop the variable and substitute it in the body. *)
           clean_arg_list tl acc (S.cons erasure_dummy subst) lctx
      | (_, var) :: tl
        -> (* Keep the variable and sink the substitution. *)
           let lctx' = DB.lctx_extend lctx var Variable erasure_dummy in
           clean_arg_list tl (var :: acc) (ssink var subst) lctx'
      | [] -> (List.rev acc, subst, lctx)
    in
    let eargs, subst, lctx' = clean_arg_list args [] S.identity lctx in
    let subst = S.cons erasure_dummy subst in (* Substitute the equality. *)
    (sexp_location l, eargs, erase_type lctx' (L.push_susp expr subst))
  in
  SMap.map clean_branch cases

(* The following entry points to erasure, `erase_type` and
   `clean_decls`, check that no metavariables are left before
   performing the erasure itself. *)
let erase_type lctx lxp =
  let _, (mvs, _) = fv lxp in
  if not (IMap.is_empty mvs) then
    Log.log_fatal
      ~print_action:(fun () ->
        Format.printf "@[<v>";
        IMap.iter
          (fun i (_, t, _, (l, n)) ->
            Format.printf "%s ?%s[%i] : @[<hov 2>%a@]@,"
              (Source.Location.to_string (sexp_location l))
              (Option.value ~default:"" n)
              i
              pp_print_clean_lexp t)
          mvs;
        Format.printf "@]")
      ("Metavariables in erase_type :");
  erase_type lctx lxp

(* Textually identical to the previous definition of `clean_decls`,
   but calls the new `erase_type`, which checks for metavars. *)
let clean_decls
      (lctx : lexp_context)
      (decls : ldecl list)
    : lexp_context * (vname * elexp) list =

  let lctx' = DB.lctx_extend_rec lctx decls in
  (lctx', List.map (fun (v, lexp, _) -> (v, erase_type lctx' lexp)) decls)

(** Turning a set of declarations into an object.  **)

let ctx2tup ctx nctx =
  (*Log.debug_msg ("Entering ctx2tup\n");*)
  assert (M.length nctx >= M.length ctx
          && ctx == M.nthcdr (M.length nctx - M.length ctx) nctx);
  let rec get_blocs nctx blocs =
    if nctx == ctx then blocs else
      match DB.lctx_view nctx with
      | DB.CVlet (_, _, _, nctx) as bloc -> get_blocs nctx (bloc :: blocs)
      | DB.CVfix (_, nctx) as bloc -> get_blocs nctx (bloc :: blocs)
      | _ -> assert false in
  let rec mk_lets_and_tup blocs types =
    let loc = dsinfo in
    match blocs with
    | []
      -> let cons_name = "cons" in
        let cons_label = (sexp_location loc, cons_name) in
        let type_label = (sexp_location loc, "record") in
        let offset = List.length types in
        let types = List.rev types in
        (*Log.debug_msg ("Building tuple of size " ^ string_of_int offset ^ "\n");*)

        mkCall (dsinfo, mkCons (mkInductive (loc, type_label, [],
                               SMap.add cons_name
                                 (List.map (fun (oname, t)
                                            -> (Aimplicit, oname,
                                               (* FIXME: We need to shift
                                                * for references outside
                                                * of the blocs, but for
                                                * references within the blocs,
                                                * this means we refer to the
                                                * let-binding i.s.o the tuple
                                                * field (which would be both
                                                * equivalent and preferable). *)
                                               mkSusp t (S.shift offset)))
                                    types)
                                 SMap.empty),
              cons_label),
              List.mapi (fun i (oname, _t)
                        -> (Aimplicit, mkVar (oname, offset - i - 1)))
                types)
    | (DB.CVlet (name, LetDef (_, e), t, _) :: blocs)
      -> mkLet (loc, [(name, mkSusp e (S.shift 1), t)],
             mk_lets_and_tup blocs ((name, t) :: types))
    | (DB.CVfix (defs, _) :: blocs)
      -> mkLet (loc, defs,
             mk_lets_and_tup blocs (List.append
                                      (List.rev
                                         (List.map (fun (oname, _, t)
                                                    -> (oname, t))
                                            defs))
                                      types))
    | _ -> assert false in
  mk_lets_and_tup (get_blocs nctx []) []

(* opslexp.ml ends here.  *)
