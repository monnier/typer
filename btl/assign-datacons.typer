get-symbol-name sexp =
  let error-msg = "<ERROR: not a symbol>"
  in Sexp_dispatch sexp
                   (lambda _ _ -> error-msg) % node
                   (lambda name -> name) % symbol
                   (lambda _ -> error-msg) % string
                   (lambda _ -> error-msg) % int
                   (lambda _ -> error-msg) % float
                   (lambda _ -> error-msg); % block

get-constructor-name pattern-sexp =
  let error-msg = "<ERROR: invalid constructor name>"
  in Sexp_dispatch pattern-sexp
                % Must be a node...
                (lambda head _ ->
                 Sexp_dispatch head
                               (lambda _ _ -> error-msg)
                               % ...whose head is a symbol.
                               (lambda name -> name)
                               (lambda _ -> error-msg)
                               (lambda _ -> error-msg)
                               (lambda _ -> error-msg)
                               (lambda _ -> error-msg))
                (lambda _ -> error-msg)
                (lambda _ -> error-msg)
                (lambda _ -> error-msg)
                (lambda _ -> error-msg)
                (lambda _ -> error-msg);

get-subpatterns pattern-sexp =
  Sexp_dispatch pattern-sexp
                (lambda _ args -> args)
                (lambda sym-name -> (cons (Sexp_symbol sym-name) nil))
                (lambda _ -> nil)
                (lambda _ -> nil)
                (lambda _ -> nil)
                (lambda _ -> nil);

% If the constructor is the "comma" tuple constructor (for tuples of the form
% `(a, b, c, ...)`, return the corresponding datacons name.
normalize-tuple-name cstr-name nb-params =
  if String_eq "_,_" cstr-name
  then tuple-lib.get-name-from-list (tuple-lib.tuple-cstr-names) nb-params
  else cstr-name;

% E.g. with argument `(a (b x y) z)`, returns `["x", "y", "z"]`
get-newvars-names : List Sexp -> List String;
get-newvars-names subpatterns =
  List_foldl
      (lambda acc pat ->
       Sexp_dispatch pat
                     % node
                     (lambda _head tail ->
                      List_concat acc (get-newvars-names tail))
                     % symbol
                     (lambda name -> List_concat acc (cons name nil))
                     % all other cases should not occur
                     (lambda _ -> acc)
                     (lambda _ -> acc)
                     (lambda _ -> acc)
                     (lambda _ -> acc))
      nil
      subpatterns;

pattern-has-var pat var-name =
  case List_find
           (lambda candidate -> String_eq var-name candidate)
           (get-newvars-names (get-subpatterns pat))
  | some _ => true
  | none => false;

% Generates a `case` matching over `subject`, which is supposed to have a single
% constructor `cstr-name` with `nb-params` operands, but only accesses the
% `var-idx`-th operand, under the bound variable `var-sym`. The branch returns
% `ret-val`.
make-single-case : Sexp -> String -> Int -> Int -> Sexp -> Sexp -> Sexp;
make-single-case subject cstr-name nb-params var-idx var-sym ret-val =
  let mk-pat-params : Int -> List Sexp;
      mk-pat-params i =
        let current-sym = if Int_eq i var-idx
                          then var-sym
                          else (Sexp_symbol "_")
        in if Int_< i nb-params
           then cons current-sym (mk-pat-params (Int_+ 1 i))
           else nil;
      pattern = (Sexp_node (Sexp_symbol (normalize-tuple-name cstr-name nb-params))
                           (mk-pat-params 0));
      branch-node = (Sexp_node (Sexp_symbol "_=>_")
                               (cons pattern (cons ret-val nil)))
  in Sexp_node (Sexp_symbol "##case_")
               (cons (Sexp_node (Sexp_symbol "_|_")
                                (cons subject (cons branch-node nil)))
                     nil);

% Generates the `case` cascade that accesses and returns the `var-name` value
% in `subpatterns`, matching over `subject`.
make-case-for-var : String -> String -> List Sexp -> Sexp -> Sexp -> Sexp;
make-case-for-var var-name cstr-name subpatterns subject temp-matching-var =
  let nb-params = List_length subpatterns;
  in case List_find (lambda pat-idx-pair ->
                     case pat-idx-pair
                     | pair pat _ => pattern-has-var pat var-name)
                    (List_mapi pair subpatterns)
     | some found-subpattern =>
         % There is a corresponding subpattern with a variable named `var-name`.
         (case found-subpattern
          | pair pat idx =>
              Sexp_dispatch
                  pat
                  % node case : has subpatterns
                  (lambda sub-cstr-name sub-subpatterns ->
                   let body = make-case-for-var var-name
                                                (get-symbol-name sub-cstr-name)
                                                sub-subpatterns
                                                temp-matching-var
                                                temp-matching-var
                   in make-single-case subject
                                       cstr-name
                                       nb-params
                                       idx
                                       temp-matching-var
                                       body)
                  % symbol case : is the variable we're looking for
                  (lambda _ ->
                   make-single-case subject
                                    cstr-name
                                    nb-params
                                    idx
                                    temp-matching-var
                                    temp-matching-var)
                  % other cases : error (cannot occur)
                  (lambda _ -> Sexp_error)
                  (lambda _ -> Sexp_error)
                  (lambda _ -> Sexp_error)
                  (lambda _ -> Sexp_error))
     | none =>
         % Logically should not occur
         Sexp_symbol "Internal error in assing-datacons";

assign-datacons = macro
    (lambda args ->
     let
         pattern-sexp = List_nth 0 args Sexp_error; % e.g. `(a (b x y) z)`
         value-sexp = List_nth 1 args Sexp_error; % e.g. `someFunction 1 2 3`

         cstr-name = get-constructor-name pattern-sexp;
         subpatterns = get-subpatterns pattern-sexp;

         new-bound-variables = get-newvars-names subpatterns; % `["x", "y", "z"]`
         
         make-bound-var-asgn-list datacons-var temp-matching-var =
           List_map
               (lambda var-name ->
                make-decl
                    (Sexp_symbol var-name)
                    (make-case-for-var var-name
                                       cstr-name
                                       subpatterns
                                       datacons-var
                                       temp-matching-var))
               % Only consider variables not named `_`
               (List_filter
                    (lambda var-name -> not (String_eq var-name "_"))
                    new-bound-variables);
         
         % Final list of assignments, which contains :
         % - an unnamed variable with the value being filtered over
         % - every bound variable from the pattern (`x`, `y` and `z` from the above
         %   example), whose value is a cascade of `case`s returning the appropriate
         %   element from `tuple-varname`
         % e.g.
         %   gensym0 = someFunction 1 2 3;
         %   x = case gensym0
         %       | a gensym1 _ =>
         %           case gensym1
         %           | b gensym1 _ => gensym1;
         %   y = case gensym0
         %       | a gensym1 _ =>
         %           case gensym1
         %           | b _ gensym1 => gensym1;
         %   z = case gensym0
         %       | a _ gensym1 =>
         %           gensym1;
         make-defs-list = lambda datacons-var temp-matching-var ->
           cons (make-decl datacons-var value-sexp)
                (make-bound-var-asgn-list datacons-var temp-matching-var)
     in do {
         datacons-var <- gensym ();
         temp-matching-var <- gensym ();
         IO_return (Sexp_node (Sexp_symbol "_;_")
                              (make-defs-list datacons-var temp-matching-var));
     });

% `(a (b x y) z) <- somevalue`
test1 = cons (quote (a (b x y) z))
             (cons (quote somevalue) nil);

% `a, (b x _ z), c <- sometuple`
test2 = cons (quote (a, (b x _ z), c))
             (cons (quote sometuple) nil);

% `(a (b (x, y, z) w) z) <- somevalue`
test3 = cons (quote (a (b (x, y, z) w) v))
             (cons (quote sometuple) nil);

t = cons (quote (exists (hp, hqr) (pf-p-hp,
                                   pf-qr-hqr,
                                   pf-hp-hqr-disjoint,
                                   pf-heap-eq-hp-hqr)))
         (cons (quote pf-hyp) nil);

runtest test =
  IO_run
      (do {
               res <- Macro_expand assign-datacons test;
               Sexp_debug_print res;
               IO_return unit
           })
      ();
