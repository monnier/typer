%%% depelim --- A macro for dependent elimination

%%      Copyright (C) 2020  Free Software Foundation, Inc.
%%
%% Author: Jean-Alexandre Barszcz <jean-alexandre.barszcz@umontreal.ca>
%% Keywords: languages, lisp, dependent types.
%%
%% This file is part of Typer.
%%
%% Typer is free software; you can redistribute it and/or modify it under the
%% terms of the GNU General Public License as published by the Free Software
%% Foundation, either version 3 of the License, or (at your option) any
%% later version.
%%
%% Typer is distributed in the hope that it will be useful, but WITHOUT ANY
%% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
%% FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
%% more details.
%%
%% You should have received a copy of the GNU General Public License along
%% with this program.  If not, see <http://www.gnu.org/licenses/>.

%%% Description
%%
%% In Typer, the branch bodies of a `case` expression must all have
%% the same type. In order to write proofs that depend the
%% constructor, Typer adds a proof in the environment of each branch:
%% the equality between the branch's head and the case target. This
%% equality can be eliminated in each branch using `Eq_cast` to go
%% from a branch-dependent type to a target-dependent type. The
%% following macro eases this process by eliminating the boilerplate.
%%
%% The macro extends the builtin `case` syntax to
%% `case_as_return_`. The `as` and `return` clauses are used to build
%% Eq_cast's `f` argument (a lambda). The `as` clause should contain a
%% symbol (the variable binding the case target or branch head), and
%% is optional if the target is itself a symbol. The `return` clause
%% is `f`'s body, i.e. the return type of the case, using the variable
%% from the `as` clause to refer to the target or branch head.

%%% Example usage
%%
%%     type Nat
%%       | Z
%%       | S Nat;
%%
%%     Nat_induction :
%%       (P : Nat -> Type_ ?l) ≡>
%%       P Z ->
%%       ((n : Nat) -> P n -> P (S n)) ->
%%       ((n : Nat) -> P n);
%%     Nat_induction base step n =
%%       case n return (P n) %% <-- `as` clause omitted: the target is a var.
%%       | Z => base
%%       | S n' => (step n' (Nat_induction (P := P) base step n'));

%%% Grammar
%%
%% This macro was written for the following operators (the precedence
%% level 42 is chosen to match that of the builtin case):
%%
%%     define-operator "as" 42 42;
%%     define-operator "return" 42 42;

case_as_return_impl args =
  let impl target_sexp var_sexp ret_and_branches_sexp =
        case Sexp_wrap ret_and_branches_sexp
        | node hd (cons ret_sexp branches) =>
            %% TODO Check that var_sexp is a variable.
            %% TODO Check that hd is the symbol `_|_`.
            let
                on_branch branch_sexp =
                  case Sexp_wrap branch_sexp
                  | node arrow_sexp (cons head_sexp (cons body_sexp nil)) =>
                      %% TODO Check that arrow_sexp is `=>`.
                      Sexp_node
                          arrow_sexp
                          (cons head_sexp
                                (cons (quote (##Eq\.cast
                                                (x := uquote head_sexp)
                                                (y := uquote target_sexp)
                                                (p := ##DeBruijn 0)
                                                (f := lambda (uquote var_sexp) ->
                                                  uquote ret_sexp)
                                                (uquote body_sexp)))
                                      nil))
                  | _ => Sexp_error; %% FIXME Improve error reporting.
                new_branches = List_map on_branch branches;
            in %% We extend the builtin case, so nested patterns don't work.
                quote (##case_
                        (uquote (Sexp_node (Sexp_symbol "_|_")
                                           (cons target_sexp new_branches))))
        | _ => Sexp_error;
  in IO_return
         case args
         | cons target_sexp
                (cons var_sexp
                      (cons ret_and_branches_sexp nil))
           => impl target_sexp var_sexp ret_and_branches_sexp
         | cons target_sexp
                (cons ret_and_branches_sexp nil)
           => %% If the `as` clause is omitted, and the target is a
              %% symbol, use it for `var_sexp` as well.
             (case Sexp_wrap target_sexp
              | symbol _ => impl target_sexp target_sexp ret_and_branches_sexp
              | _ => Sexp_error)
         | _ => Sexp_error;

case_as_return_ = macro case_as_return_impl;
case_return_    = macro case_as_return_impl;
